package com.nanda.experiment;
import com.datatorrent.contrib.kafka.KafkaSinglePortStringInputOperator;
import static org.apache.apex.malhar.stream.api.Option.Options.name;

import com.datatorrent.api.StreamingApplication;
import com.datatorrent.api.annotation.ApplicationAnnotation;
import org.apache.hadoop.conf.Configuration;
import com.datatorrent.api.DAG;
import org.apache.apex.malhar.stream.api.ApexStream;
import org.apache.apex.malhar.stream.api.impl.StreamFactory;

@ApplicationAnnotation(name="Kafka2Hdfs")
public class Kafka2Hdfs implements StreamingApplication {

@Override
public void populateDAG(DAG dag, Configuration conf) {

        System.out.println("Inside Dataflow");

		KafkaSinglePortStringInputOperator k = new KafkaSinglePortStringInputOperator();
		ApexStream<String> d = StreamFactory.fromInput(k, k.outputPort, name("kafkaIn1"));


        LineOutputOperator<String> fOut = new LineOutputOperator<>();
		d.endWith(fOut, fOut.input, name("fOut"));


    }
}
