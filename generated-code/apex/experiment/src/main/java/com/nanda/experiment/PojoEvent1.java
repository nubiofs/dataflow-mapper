public class PojoEvent1 {

	private String line_destination;
	private String line_way;
	private String line_origin;
	private String line_id;

	public String getLine_destination() { return line_destination; }
	public void setLine_destination(String line_destination) { this.line_destination = line_destination; }

	public String getLine_way() { return line_way; }
	public void setLine_way(String line_way) { this.line_way = line_way; }

	public String getLine_origin() { return line_origin; }
	public void setLine_origin(String line_origin) { this.line_origin = line_origin; }

	public String getLine_id() { return line_id; }
	public void setLine_id(String line_id) { this.line_id = line_id; }


}