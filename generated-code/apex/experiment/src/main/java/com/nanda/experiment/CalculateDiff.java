package com.nanda.experiment;

import com.datatorrent.api.DAG;
import com.datatorrent.api.StreamingApplication;
import org.apache.apex.malhar.lib.fs.LineByLineFileInputOperator;
import org.apache.apex.malhar.lib.function.Function;
import org.apache.apex.malhar.stream.api.ApexStream;
import org.apache.apex.malhar.stream.api.impl.StreamFactory;
import org.apache.hadoop.conf.Configuration;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.apache.apex.malhar.stream.api.Option.Options.name;

public class CalculateDiff implements StreamingApplication {

    @Override
    public void populateDAG(DAG dag, Configuration conf) {

        conf.addResource(this.getClass().getResourceAsStream("/META-INF/properties-CalculateDiff.xml"));
        LineByLineFileInputOperator timesFile = new LineByLineFileInputOperator();
        //FileInputOperator2 timesFile = new FileInputOperator2();
        LineOutputOperator2<String> out = new LineOutputOperator2<>();

        timesFile.setDirectory("/tmp/201904/");
        //out.setFilePath("/tmp/output/");
        //out.setBaseName("example.txt");

        ApexStream<String> prevTime = StreamFactory
                .fromInput(timesFile, timesFile.output, name("timesFile"));

        prevTime
                .map(new SetPrevisionPojo()).print();
                //.endWith(out, out.input, name("fileOut"));

        prevTime.populateDag(dag);

    }

    private static Date tryParseDate(String item) {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        try {
            return format.parse(item);
        } catch (ParseException e) {
            return null;
        }
    }

    public static class SetPrevisionPojo implements Function.MapFunction<String, String> {

        @Override
        public String f(String values) {

            String[] items = values.split(",");
            PrevisionPojo prev = new PrevisionPojo();

            if (items.length == 6) {
                prev.setLineId(items[0].trim());
                prev.setStop1Id(items[1].trim());
                Date time1 = tryParseDate(items[2].trim());
                if (time1 != null) prev.setStop1PrevTime(time1.getTime());
                prev.setStop2Id(items[3].trim());
                Date time2 = tryParseDate(items[4].trim());
                if (time2 != null) prev.setStop2PrevTime(time2.getTime());

                //Division by 60000 converts from millis to minutes
                if (time1 != null && time2 != null) {
                    if (time2.getTime() > time1.getTime())
                        prev.setPrevDiff((time2.getTime() - time1.getTime()) / 60000);
                    else
                        prev.setPrevDiff((time1.getTime() - time2.getTime()) / 60000);
                }
                prev.setTimestamp(items[5].trim());
            }
            else
                return "";


            return prev.getLineId() + "-" + prev.getStop1Id() + "-" +
                    prev.getStop2Id() + ", " + prev.getPrevDiff() + ", " +
                    prev.getTimestamp();
        }
    }

}
