package com.nanda.experiment;

public class PrevisionPojo {

    private String lineId;
    private String stop1Id;
    private Long stop1PrevTime;
    private String stop2Id;
    private Long stop2PrevTime;
    private Long prevDiff;
    private String timestamp;

    public String getLineId() {
        return lineId;
    }

    public void setLineId(String lineId) {
        this.lineId = lineId;
    }

    public String getStop1Id() {
        return stop1Id;
    }

    public void setStop1Id(String stop1Id) {
        this.stop1Id = stop1Id;
    }

    public Long getStop1PrevTime() {
        return stop1PrevTime;
    }

    public void setStop1PrevTime(Long stop1PrevTime) {
        this.stop1PrevTime = stop1PrevTime;
    }

    public String getStop2Id() {
        return stop2Id;
    }

    public void setStop2Id(String stop2Id) {
        this.stop2Id = stop2Id;
    }

    public Long getStop2PrevTime() {
        return stop2PrevTime;
    }

    public void setStop2PrevTime(Long stop2PrevTime) {
        this.stop2PrevTime = stop2PrevTime;
    }

    public Long getPrevDiff() {
        return prevDiff;
    }

    public void setPrevDiff(Long prevDiff) {
        this.prevDiff = prevDiff;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String toString() {
        return lineId + ", " + stop1Id + ", " + stop1PrevTime + ", " +
                stop2Id + ", " + stop2PrevTime + ", " + prevDiff + ", " + timestamp;
    }
}
