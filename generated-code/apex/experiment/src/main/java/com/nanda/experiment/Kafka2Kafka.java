package com.nanda.experiment;
import com.datatorrent.contrib.kafka.KafkaSinglePortStringInputOperator;
import static org.apache.apex.malhar.stream.api.Option.Options.name;
import com.datatorrent.contrib.kafka.KafkaSinglePortOutputOperator;

import org.apache.apex.malhar.lib.function.Function;
import com.datatorrent.api.StreamingApplication;
import com.datatorrent.api.annotation.ApplicationAnnotation;
import org.apache.hadoop.conf.Configuration;
import com.datatorrent.api.DAG;
import org.apache.apex.malhar.stream.api.ApexStream;
import org.apache.apex.malhar.stream.api.impl.StreamFactory;

@ApplicationAnnotation(name="Kafka2Kafka")
public class Kafka2Kafka implements StreamingApplication {

@Override
public void populateDAG(DAG dag, Configuration conf) {

		/*$placeholderDeclarations*/

		KafkaSinglePortStringInputOperator k = new KafkaSinglePortStringInputOperator();
		ApexStream<String> d = StreamFactory.fromInput(k, k.outputPort, name("kafkaIn1"));


        KafkaSinglePortOutputOperator<String, String> kOut = new KafkaSinglePortOutputOperator<>();
		d.endWith(kOut, kOut.inputPort, name("kafkaOut1"));


        d = d.filter(new BiggerThanThirty());

        KafkaSinglePortOutputOperator<null, String> k2Out = new KafkaSinglePortOutputOperator<>();
		d.endWith(k2Out, k2Out.inputPort, name("kafkaOut2"));


        d.print();

    }

    public static class BiggerThanThirty implements Function.FilterFunction<String> {

        @Override
        public boolean f(String content) {
            if (!content.contains(","))
                return false;

            String max = content.split(",")[1].trim();

            if (Integer.parseInt(max) < 30)
                return false;

            return true;
        }
    }
}
