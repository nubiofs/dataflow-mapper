package com.nanda.experiment;
import static org.apache.apex.malhar.stream.api.Option.Options.name;

import com.datatorrent.api.Context;
import org.apache.apex.malhar.lib.window.WindowOption;
import org.apache.apex.malhar.stream.api.ApexStream;
import org.apache.apex.malhar.stream.api.impl.StreamFactory;
import com.datatorrent.lib.math.MinKeyVal;
import com.datatorrent.lib.math.MaxKeyVal;

import org.apache.apex.malhar.lib.function.Function;
import com.datatorrent.lib.util.KeyValPair;
import com.datatorrent.api.StreamingApplication;
import com.datatorrent.api.annotation.ApplicationAnnotation;
import org.apache.hadoop.conf.Configuration;
import com.datatorrent.api.DAG;
import org.apache.apex.malhar.stream.api.ApexStream;
import org.apache.apex.malhar.stream.api.impl.StreamFactory;

@ApplicationAnnotation(name="FileMinMaxFile")
public class FileMinMaxFile implements StreamingApplication {

@Override
public void populateDAG(DAG dag, Configuration conf) {

		FileStringInput f = new FileStringInput();
		ApexStream<String> d = StreamFactory.fromInput(f, f.output, name("f1"));

		ApexStream<KeyValPair<String, Double>> d2 = d.map(new Example());

        /*MinKeyVal<String, Double> minOperator1 = new MinKeyVal<>();
		d2 = d2.addOperator(minOperator1, minOperator1.data, minOperator1.min)
        .window(new WindowOption.GlobalWindow());


        LineOutputOperator<KeyValPair<String, Double>> f2Out1 = new LineOutputOperator<>();
		d2.endWith(f2Out1, f2Out1.input, name("f2Out1"));*/

        MaxKeyVal<String, Double> maxOperator1 = new MaxKeyVal<>();
		d2 = d2.addOperator(maxOperator1, maxOperator1.data, maxOperator1.max)
                .window(new WindowOption.GlobalWindow());

        LineOutputOperator<KeyValPair<String, Double>> f2Out2 = new LineOutputOperator<>();
		d2.endWith(f2Out2, f2Out2.input, name("f2Out2"));

		d2.populateDag(dag);

        /*dag.getMeta(minOperator1).getAttributes()
            .put(Context.OperatorContext.APPLICATION_WINDOW_COUNT, 5);*/


}

    public static class Example implements Function.MapFunction<String, KeyValPair<String, Double>> {
        @Override
        public KeyValPair<String, Double> f(String value) {
            String[] items = value.split("=");
            return new KeyValPair<>(items[0], tryParseTime(items[1].trim()));
        }
    }

    private static Double tryParseTime(String inputItem) {
        try {
            return Double.parseDouble(inputItem);
        } catch (NumberFormatException e) {
            return null;
        }
    }

}
