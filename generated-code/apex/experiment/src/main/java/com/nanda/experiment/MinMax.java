package com.nanda.experiment;
import static org.apache.apex.malhar.stream.api.Option.Options.name;
import org.apache.apex.malhar.lib.fs.LineByLineFileInputOperator;
import org.apache.apex.malhar.stream.api.ApexStream;
import org.apache.apex.malhar.stream.api.impl.StreamFactory;
import com.datatorrent.lib.math.MinKeyVal;
import com.datatorrent.lib.math.MaxKeyVal;

import org.apache.apex.malhar.lib.function.Function;
import com.datatorrent.lib.util.KeyValPair;
import com.datatorrent.api.StreamingApplication;
import com.datatorrent.api.annotation.ApplicationAnnotation;
import org.apache.hadoop.conf.Configuration;
import com.datatorrent.api.DAG;

@ApplicationAnnotation(name="MinMax")
public class MinMax implements StreamingApplication {

@Override
public void populateDAG(DAG dag, Configuration conf) {

		LineByLineFileInputOperator f = new LineByLineFileInputOperator();
		ApexStream<String> d = StreamFactory.fromInput(f, f.output, name("f"));

        //ApexStream<String> d = f.read("/tmp/","teste.txt");

		ApexStream<KeyValPair<String, Double>> d2 = d.map(new Example());


        MinKeyVal<String, Double> minOperator1 = new MinKeyVal<>();
		d2.addOperator(minOperator1, minOperator1.data, minOperator1.min).print();

        LineOutputOperator<KeyValPair<String, Double>> fOut = new LineOutputOperator<>();
        d2.endWith(fOut, fOut.input, name("fOut"));

        MaxKeyVal<String, Double> maxOperator1 = new MaxKeyVal<>();
		d2.addOperator(maxOperator1, maxOperator1.data, maxOperator1.max).print();

        LineOutputOperator<KeyValPair<String, Double>> fOut2 = new LineOutputOperator<>();
		d2.endWith(fOut2, fOut2.input, name("fOut2"));


    }

    public static class Example implements Function.MapFunction<String, KeyValPair<String, Double>> {
        @Override
        public KeyValPair<String, Double> f(String value) {
            String[] items = value.split(",");
            return new KeyValPair<>(items[0], tryParseTime(items[1].trim()));
        }
    }

    private static Double tryParseTime(String inputItem) {
        try {
            return Double.parseDouble(inputItem);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    private static class PrettyPrint implements Function.MapFunction<String, String> {
        @Override
        public String f(String input) {
            return input.split(",")[0] + "-----sum: "
                    + input.split(",")[1];
        }
    }

}
