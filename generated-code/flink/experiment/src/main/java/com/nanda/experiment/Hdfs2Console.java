package com.nanda.experiment;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.DataSet;

import java.io.InputStream;

import org.apache.flink.api.common.functions.MapFunction;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Hdfs2Console {
    public static void main(String[] args) throws Exception {

		final ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

        InputStream inputStream = Hdfs2Console.class.getResourceAsStream("/Hdfs2Console.properties");

        //Properties prop = ParameterTool.fromPropertiesFile(inputStream).getProperties();


		DataSet<String> d = env.readTextFile("hdfs://localhost:8020/tmp/201906/calcdiff/");


        d.map(new CalcDiff()).print();

    }

    private static Date tryParseDate(String item) {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        try {
            return format.parse(item);
        } catch (ParseException e) {
            return null;
        }
    }

    public static class CalcDiff implements MapFunction<String, String> {

        @Override
        public String map(String values) {

            String[] items = values.split(",");

            String line;
            Long diff = 0L;
            String timestamp;

            if (items.length == 6) {
                line = items[0].trim();
                Date time1 = tryParseDate(items[2].trim());
                Date time2 = tryParseDate(items[4].trim());

                //Division by 60000 converts from millis to minutes
                if (time1 != null && time2 != null) {
                    if (time2.getTime() > time1.getTime())
                        diff = (time2.getTime() - time1.getTime()) / 60000;
                    else
                        diff = (time1.getTime() - time2.getTime()) / 60000;
                }
                timestamp = items[5].trim();
            }
            else
                return "";

            return line + ", " + diff + ", " + timestamp;
        }
    }
}
