package com.nanda.experiment;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;

import java.io.InputStream;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.core.fs.FileSystem;

public class HdfsMinMaxHdfs {

    public static void main(String[] args) throws Exception {

		ExecutionEnvironment env  = ExecutionEnvironment.getExecutionEnvironment();

        InputStream inputStream = HdfsMinMaxHdfs.class.getResourceAsStream("/HdfsMinMaxHdfs.properties");

        //Properties prop = ParameterTool.fromPropertiesFile(inputStream).getProperties();

		DataSet<String> d = env.readTextFile("hdfs://localhost:8020/tmp/201906/prev/simplified_201906_flink.txt");


		DataSet<Tuple2<String, Double>> d2 = d.map(new Example());

        //d2.groupBy(0).minBy(1).print();

        //d2.writeAsText("hdfs://localhost:8020/tmp/output/201906/prev/min_flink.txt", FileSystem.WriteMode.OVERWRITE).setParallelism(1);


        d2.groupBy(0).maxBy(1).print();


        d2.writeAsText("hdfs://localhost:8020/tmp/output/201906/prev/max_flink.txt", FileSystem.WriteMode.OVERWRITE).setParallelism(1);

        //env.execute();
    }

    public static class Example implements MapFunction<String, Tuple2<String, Double>> {
        @Override
        public Tuple2<String, Double> map(String value) {
            String[] items = value.split(",");
            return new Tuple2<>(items[0], tryParseTime(items[1].trim()));
        }
    }

    private static Double tryParseTime(String inputItem) {
        try {
            return Double.parseDouble(inputItem);
        } catch (NumberFormatException e) {
            return null;
        }
    }

}
