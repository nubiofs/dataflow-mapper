package com.nanda.experiment;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.api.java.utils.ParameterTool;
import java.io.InputStream;
import java.util.Properties;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer011;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.core.fs.FileSystem;

public class Kafka2Hdfs {

    public static void main(String[] args) throws Exception {

        System.out.println("Inside Dataflow");

		StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        InputStream inputStream = Kafka2Hdfs.class.getResourceAsStream("/Kafka2Hdfs.properties");

        Properties prop = ParameterTool.fromPropertiesFile(inputStream).getProperties();


		DataStream<String> d = env.addSource(new FlinkKafkaConsumer011<>("example", new SimpleStringSchema(), prop).setStartFromEarliest());


        d.writeAsText("hdfs://localhost:8020/tmp/sample.txt", FileSystem.WriteMode.OVERWRITE).setParallelism(1);


    }
}
