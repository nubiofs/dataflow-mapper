package com.nanda.experiment;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.api.java.utils.ParameterTool;
import java.io.InputStream;
import java.util.Properties;
import org.apache.flink.core.fs.FileSystem;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple2;

public class MinMax {

    public static void main(String[] args) throws Exception {

		StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        InputStream inputStream = MinMax.class.getResourceAsStream("/MinMax.properties");

        Properties prop = ParameterTool.fromPropertiesFile(inputStream).getProperties();


		DataStream<String> d = env.readTextFile("hdfs://localhost:8020/tmp/201904/prev/201904_flink.txt");

        //DataStream<String> d = f.read("/tmp/","teste.txt");

		DataStream<Tuple2<String, Double>> d2 = d.map(new Example());


        d2.keyBy(0).minBy(1).print();


        d2.keyBy(0).maxBy(1).print();


        //f.write(TP.build("localhost:8020/tmp/output/201904/prev/"),TP.build("min_flink.txt"), TP.build(52428800), d2);

        d2.writeAsText("hdfs://localhost:8020/tmp/output/201904/prev/min_flink.txt", FileSystem.WriteMode.OVERWRITE).setParallelism(1);


    }

    public static class Example implements MapFunction<String, Tuple2<String, Double>> {
        @Override
        public Tuple2<String, Double> map(String value) {
            String[] items = value.split(",");
            return new Tuple2<>(items[0], tryParseTime(items[1].trim()));
        }
    }

    private static Double tryParseTime(String inputItem) {
        try {
            return Double.parseDouble(inputItem);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    private static class PrettyPrint implements MapFunction<String, String> {
        @Override
        public String map(String input) {
            return input.split(",")[0] + "-----sum: "
                    + input.split(",")[1];
        }
    }

}
