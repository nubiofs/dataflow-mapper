package com.nanda.csvExample.util;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.Utils;
import org.apache.flink.api.java.io.TupleCsvInputFormat;
import org.apache.flink.api.java.operators.DataSource;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.api.java.typeutils.TupleTypeInfo;

import org.apache.flink.core.fs.FileSystem;
import org.apache.flink.core.fs.Path;

import java.util.stream.IntStream;


public class GenericRead {

    /*This can be called by the user to read a csv file with columns varying from 1 to 25*/
    private static DataSource<Tuple> readCsv(ExecutionEnvironment env, Path path, Class[] fieldTypes) {
        TupleTypeInfo<Tuple> typeInfo = TupleTypeInfo
                .getBasicAndBasicValueTupleTypeInfo(fieldTypes);

        TupleCsvInputFormat<Tuple> inputFormat =
                new TupleCsvInputFormat<>(path, typeInfo);

        return new DataSource<>(env, inputFormat, typeInfo, Utils.getCallLocationName());
    }

    public static void main(String[] args) throws Exception {

        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
        int n = 3; // number of columns here

        Path p = new Path(System.getProperty("user.dir") + "/products.csv");
        Class[] types = IntStream.range(0, n).mapToObj(i -> String.class).toArray(Class[]::new);
        DataSet<Tuple> d = readCsv(env, p, types);
        d.writeAsCsv("output1.txt", FileSystem.WriteMode.OVERWRITE).setParallelism(1);

        p = new Path(System.getProperty("user.dir") + "/products2.csv");
        types = IntStream.range(0, 4).mapToObj(i -> String.class).toArray(Class[]::new);
        d = readCsv(env, p, types);
        d.writeAsCsv("output2.txt", FileSystem.WriteMode.OVERWRITE).setParallelism(1);

        env.execute();

    }

}
