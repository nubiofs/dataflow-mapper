package com.nanda.bdOperations;

import org.apache.flink.api.common.operators.Order;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple2;

public class SortBatch {

    public static void main(String[] args) throws Exception {
        final ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

        DataSet<Tuple2<String, Integer>> mybatch = env.fromElements(
                new Tuple2<>("hello", 1),
                new Tuple2<>("hello", 1),
                new Tuple2<>("world", 2),
                new Tuple2<>("world", 3),
                new Tuple2<>("world", 4)
        );

        System.out.println("==>All elements:");
        mybatch.print();

        System.out.println("==>Distinct elements:");
        mybatch = mybatch.distinct();
        mybatch.print();

        System.out.println("==>First n (top) elements");
        mybatch.first(2).print();


        System.out.println("==>Sort: ascending order");
        mybatch.sortPartition(1, Order.ASCENDING).setParallelism(1).print();

        System.out.println("==>Sort: descending order");
        mybatch.sortPartition(1, Order.DESCENDING).setParallelism(1).print();

    }
}