package com.nanda.bdOperations;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple2;

public class AggregationBatch {

    public static void main(String[] args) throws Exception {
        // set up the batch execution environment
        final ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

        DataSet<Tuple2<String, Integer>> mybatch = env.fromElements(
                new Tuple2<>("hello", 1),
                new Tuple2<>("world", 2),
                new Tuple2<>("world", 3),
                new Tuple2<>("world", 4)
        );


        System.out.println("==>MAX:");
        mybatch.groupBy(0).maxBy(1).print();

        System.out.println("==>MIN:");
        mybatch.groupBy(0).minBy(1).print();

        System.out.println("==>SUM:");
        mybatch.groupBy(0).sum(1).print();

    }
}