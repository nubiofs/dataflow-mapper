/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.nanda.bdOperations;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple2;

public class JoinUnionBatch {

	public static void main(String[] args) throws Exception {
		final ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

		DataSet<Tuple2<String, Integer>> mybatch = env.fromElements(
				new Tuple2<>("hello", 1),
				new Tuple2<>("world", 2),
				new Tuple2<>("world", 3),
				new Tuple2<>("world", 4)
		);

		System.out.println("==>Union of two datasets");
		DataSet<Tuple2<String, Integer>> second = env.fromElements(
				new Tuple2<>("uno", 1),
				new Tuple2<>("dos", 2)
		);

		mybatch.union(second).print();

		System.out.println("==>Equi-join");
		DataSet<Tuple2<String, String>> third = env.fromElements(
				new Tuple2<>("hello", "oi"),
				new Tuple2<>("bye", "tchau")
		);

		mybatch.join(third).where(0).equalTo(0).print();

		env.execute("Flink Streaming Union example");
	}
}