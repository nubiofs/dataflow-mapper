package com.nanda.rabbit;

import com.nanda.rabbit.RabbitTutorial.Conn;
import com.rabbitmq.client.*;
import org.apache.flink.api.common.functions.RuntimeContext;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.operators.StreamingRuntimeContext;
import org.apache.flink.streaming.connectors.rabbitmq.RMQSource;
import org.apache.flink.streaming.connectors.rabbitmq.common.RMQConnectionConfig;

import java.io.IOException;

public class RabbitSource {

    private static final String EXCHANGE_NAME = "topic_test";
    private static final String QUEUE_NAME = "flink-test";

    public static void main(String[] args) throws Exception {
        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        //env.enableCheckpointing(3000, CheckpointingMode.EXACTLY_ONCE);

        Conn.createConnection(EXCHANGE_NAME);
        Channel channel = Conn.getChannel();

        boolean durable = true;
        //b:a durable, b1:non-exclusive, b2:non-autodelete queue with a well-known name
        channel.queueDeclare(QUEUE_NAME, durable, false, false, null);

        /*s2 is the binding key. '#' means that the queue will receive all the messages*/
        channel.queueBind(QUEUE_NAME, EXCHANGE_NAME, "#");

        final RMQConnectionConfig connectionConfig = Conn.conConfigFactory();

        RMQSource<String> rmqsource = new RMQSource<>(
                connectionConfig,            // config for the RabbitMQ connection
                "flink-test",        // name of the RabbitMQ queue to consume
                false,         // use correlation ids; can be false if only at-least-once is required
                new SimpleStringSchema());

        final DataStream<String> stream = env
                .addSource(rmqsource)  // deserialization schema to turn messages into Java objects
                .setParallelism(1);   // non-parallel source is only required for exactly-once

        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                String message = new String(body, "UTF-8");
                System.out.println(" [x] Received '" + message + "'");
                channel.basicAck(envelope.getDeliveryTag(), false);
            }
        };


        boolean autoAck = false;
        /*RuntimeContext runtimeContext = getRuntimeContext();
        if (runtimeContext instanceof StreamingRuntimeContext
                && ((StreamingRuntimeContext) runtimeContext).isCheckpointingEnabled()) {
            autoAck = false;
            // enables transaction mode
            channel.txSelect();
        } else {
            autoAck = true;
        }*/

        channel.basicConsume(QUEUE_NAME, autoAck, consumer);
        stream.print();
        env.execute();

    }

}

