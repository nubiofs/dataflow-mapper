package com.nanda.rabbit.RabbitTutorial;

import com.rabbitmq.client.*;
import org.apache.flink.streaming.connectors.rabbitmq.common.RMQConnectionConfig;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Conn {

    private static Connection connection;
    private static Channel channel;

    public static String exchangeName          = "simpl_exchange";
    public static String queueName             = "second";
    public static String rabbitmqHostname      = "localhost";
    public static String rabbitmqVirtualHost   = "/";
    public static String rabbitmqUsername      = "guest";
    public static String rabbitmqPassword      = "guest";
    public static Integer rabbitmqPort         = 5672;
    public static boolean durableQueue         = false;


    public static ConnectionFactory createConnection(String EXCHANGE_NAME) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        connection = factory.newConnection();

        channel = connection.createChannel();
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.TOPIC);

        return factory;
    }

    public static void closeConnection() throws TimeoutException, IOException {
        channel.close();
        connection.close();
    }

    public static void publishMessage(String EXCHANGE_NAME, String severity, String message)
    throws Exception {
        channel.basicPublish(EXCHANGE_NAME, severity,
                            MessageProperties.PERSISTENT_TEXT_PLAIN,
                            message.getBytes("UTF-8"));
        System.out.println(" [x] Sent '" + severity + "':'" + message + "'");
    }


    public static Channel getChannel() {
        return channel;
    }

    public static Connection getConnection() {
        return connection;
    }

    public static RMQConnectionConfig conConfigFactory() {
        RMQConnectionConfig connectionConfig = new RMQConnectionConfig.Builder()
                .setHost(rabbitmqHostname)
                .setPort(rabbitmqPort)
                .setUserName(rabbitmqUsername)
                .setPassword(rabbitmqPassword)
                .setVirtualHost(rabbitmqVirtualHost)
                .build();
        return connectionConfig;
    }

}
