package com.nanda.rabbit.RabbitTutorial;

import com.rabbitmq.client.*;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import java.io.IOException;

public class ReceiveLogsTopic {

    private static final String EXCHANGE_NAME = "topic_logs";
    private static final String QUEUE_NAME = "resource_cataloguer_data_stream";

    public static void main(String[] argv) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        Conn.createConnection(EXCHANGE_NAME);

        /*Function queueDeclare() with no parameters creates
         a non-durable, exclusive, autodelete queue with a generated name */
        Channel channel = Conn.getChannel();
        //String queueName = channel.queueDeclare().getQueue();
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);

        if (argv.length < 1) {
            System.err.println("Usage: ReceiveLogsTopic [binding_key]...");
            System.exit(1);
        }

        for (String bindingKey : argv) {
            channel.queueBind(QUEUE_NAME, EXCHANGE_NAME, bindingKey);
        }

        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

        Consumer consumer = new DefaultConsumer(channel) {
            DataStream<String> mystream;
            String message;

            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                                       AMQP.BasicProperties properties, byte[] body) throws IOException {
                message = new String(body, "UTF-8");
                System.out.println(" [x] Received '" + envelope.getRoutingKey() + "':'" + message + "'");
                mystream = env.fromElements(message);
            }
        };

        //After the worker dies all unacknowledged messages will be redelivered.
        boolean autoAck = false;
        channel.basicConsume(QUEUE_NAME, autoAck, consumer);
        //channel.basicConsume(queueName, true, consumer);
    }
}