package com.nanda.rabbit;

import com.nanda.rabbit.RabbitTutorial.Conn;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import java.util.Scanner;

public class RabbitSink {

    private static final String EXCHANGE_NAME = "topic_test";
    private static final String QUEUE_NAME = "flink-test";

    private static void readFromScanner (Channel channel) throws Exception {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter your message. Type exit to finish.");
        String message = scanner.next();
        while (!message.equals("exit")) {
            channel.basicPublish(EXCHANGE_NAME, QUEUE_NAME,
                    new AMQP.BasicProperties.Builder()
                    .correlationId(java.util.UUID.randomUUID().toString()).build(),
                    message.getBytes("UTF-8"));
            System.out.println(" [x] Sent '" + message + "'");
            message = scanner.next();
        }

        scanner.close();
    }

    private static void readFromEnv(StreamExecutionEnvironment env, Channel channel) throws Exception {
        DataStream<String> nova = env.fromElements("oi", "baby","hello");

        channel.basicPublish(EXCHANGE_NAME, QUEUE_NAME,
                new AMQP.BasicProperties.Builder()
                        .correlationId(java.util.UUID.randomUUID().toString()).build(),
                nova.toString().getBytes("UTF-8"));

        nova.map(value -> "[x] Sent: " + value).print();
        env.execute();
    }

    public static void main(String args[]) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        Conn.createConnection(EXCHANGE_NAME);
        Channel channel = Conn.getChannel();

        //b:a durable, b1:non-exclusive, b2:non-autodelete queue with a well-known name
        channel.queueDeclare(QUEUE_NAME, true, false, false, null);

        /*Two different options for getting the data*/
        RabbitSink.readFromEnv(env, channel);
        //RabbitSink.readFromScanner(channel);

		Conn.closeConnection();
    }
}