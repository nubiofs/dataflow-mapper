package com.nanda.hdfs;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.core.fs.FileSystem;

public class FromElementsToHdfs {

    public static void main(String[] args) throws Exception {
        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
        DataSet<String> hdfslines = env.fromElements("uno", "another line", "hey!");
        /*If the file already exists, it will fail when using CLI*/
        hdfslines.writeAsText("hdfs://localhost:8020/tmp/test/output-dir/file1.txt",
                FileSystem.WriteMode.OVERWRITE);
        env.execute();
    }
}
