package com.nanda.hdfs;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;

public class HdfsToConsole {

    public static void main(String[] args) throws Exception {
        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
        // read text file from a HDFS running at nnHost:nnPort
        DataSet<String> hdfsLines = env
                .readTextFile("hdfs://localhost:8020/tmp/test/input-dir/file.txt");
        hdfsLines.print();
    }
}