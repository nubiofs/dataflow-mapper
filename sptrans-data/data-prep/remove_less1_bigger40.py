#!/usr/bin/python3

import os
import sys
import statistics

dirpath = os.path.abspath(os.path.dirname(sys.argv[0])) + "/"

infile_list = ["results/april/consec_stops/flink_exp1_april.txt",
"results/may/consec_stops/flink_exp1_may.txt",
"results/june/consec_stops/flink_exp1_june.txt"]

outfile_list = ["results/april/consec_stops/flink_exp1_april_clean.txt",
"results/may/consec_stops/flink_exp1_may_clean.txt",
"results/june/consec_stops/flink_exp1_june_clean.txt"]


#Only writes those lines whose values are between 1 and 50
for fname, fname2 in zip(infile_list, outfile_list):
    print("Infile: ", fname, " outfile: ", fname2 )

    with open(fname, "r") as infile, open(fname2, "w") as outfile:
        for line in infile:
            content = line.split(",")
            if (len(content) > 2):
                key = content[0].strip()
                value = content[1].strip()
                if ((float(value) <= 40) and (float(value) >= 1)):
                    if (key not in ['32772-260016855-340015333', '1465-720015730-810009963']):
                        outfile.write(line)
