#!/bin/bash


#USEFUL COMMAND: awk '!seen[$0]++' positions_output.txt > pos_output_uniq.txt 
#The comand above removes repeated lines inside the file
i=0
previous=1
curr=100
while [ $i -lt 200 ]; do
    #awk "NR>=$previous&&NR<=$curr" positions_output.txt > tmp.txt
    awk "NR>=$previous&&NR<=$curr" pos_output_uniq_sample_september.txt > tmp.txt
    #IMPORTANT: replace <kafka-path> by respective path
    timeout 5 sh <kafka-path>/kafka_2.11-0.8.2.2/bin/kafka-console-producer.sh --broker-list localhost:9092 --topic example < tmp.txt &
    pid=$!
    let i=i+1;
    #awk "NR>=$previous&&NR<=$curr" pos_output_uniq_sample_september.txt
    let previous=$curr+1
    let curr=curr+100
    sleep 0.5
    echo "pid is: $pid"
done
