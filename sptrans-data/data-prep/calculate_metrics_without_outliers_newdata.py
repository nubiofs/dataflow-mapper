#!/usr/bin/python3

import os
import sys
import json
import math
from datetime import datetime

dirpath = os.path.abspath(os.path.dirname(sys.argv[0])) + "/"

FMT = '%H:%M'

file_list = ['results/april/consec_stops/flink_exp1_april_clean.txt',
'results/may/consec_stops/flink_exp1_may_clean.txt', 
'results/june/consec_stops/flink_exp1_june_clean.txt']

#file_list = ["input/april/prev_diff_cleaned_201904.txt"]


#The metric_per_stop structure is:  { line-stop = [max, min, sum, cont] }
metrics_per_stop = { }


cont = 1
for fname in file_list:
    with open(fname, "r") as infile:
        for line in infile:
            content = line.split(",")
            if (len(content) >= 3):
                key = content[0].strip()
                minutes_diff = int(content[1].strip())
                
                #[max, min,sum, cont]
                if (metrics_per_stop.get(key) != None):
                    values = metrics_per_stop.get(key)
                    if (minutes_diff > values[0]):
                        values[0] = minutes_diff
                    if (minutes_diff < values[1]):
                        values[1] = minutes_diff
                    values[2] += minutes_diff
                    values[3] += 1

                    metrics_per_stop[key] = values
                else:
                    metrics_per_stop[key] = [minutes_diff, minutes_diff, minutes_diff, 1]
            
            cont+=1

for key in metrics_per_stop:
    first_el = key.split("-")
    line_id = first_el[0].strip()
    #It is the second stop instead of the first, since the prevision time refers
    #to the time spent to arrive to the second stop coming from the first one
    stop_id = first_el[2].strip()
    values = metrics_per_stop[key]
    print("%s, %s, %d, %d, %d, %d" % (stop_id, line_id, values[0], values[1], values[2], values[3]))
    
