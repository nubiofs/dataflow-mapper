
#!/usr/bin/python3

import os
import sys
import json


curr_dir = os.path.dirname(os.path.abspath(__file__))
input_dir = curr_dir + "/input/june/arrival_time/"
output_dir = curr_dir + "/output/june/arrival_time/"

lines = ["1651", "1465", "1450", "32772", "34694", "198"]


stops_line_1651 = ["1410074", "1410075", "1410076", "3305795", "3305796", "3305797"]
stops_line_1465 = ["670016557", "706325", "70016561", "440015161", "440015158", "440015164", "340015323", "340015329", "530015342", "530015348", "340015750", "340015746", "150015738", "720015734", "720015730", "810009963", "810009941", "810009943", "810009945", "810009948", "810009950", "810009951", "810009954", "810009956", "230009957", "230009960", "230009962", "230009829", "230009870", "230009868", "230009867", "230009866", "230009864", "230009861", "230009860", "230009858"]
stops_line_1450 = ["670016557", "706325", "70016561", "440015161", "440015158", "440015164", "340015323", "340015329", "530015342", "530015348", "340015750", "340015746", "150015738", "720015734", "720015730"]
stops_line_32772 = ["260016860", "260016858", "260016856", "260016855",  "340015333", "340015337", "340015346", "340015751", "340015748", "340015740", "340015736", "720015731"]
stops_line_34694 = ["440015007", "440015005",  "440015003",  "630015010", "120011357", "120011361", "120011363", "550011365", "120011367", "960011368", "960011373"]
stops_line_198 = ["920015225", "920015205", "920015204", "920015203", "530015173", "530015320", "530015319", "530015318", "530015317", "1506163"]

stops = [stops_line_1651, stops_line_1465, stops_line_1450, stops_line_32772, stops_line_34694, stops_line_198]

i = j = 0
fname_suffix = ".txt"

while i < len(lines):
    curr_line = lines[i]
    print("Current line is " + curr_line)
    curr_stops = stops[i]
    fname_prefix = "line_" + curr_line + "_stop_" 
    j = 0
    while j < (len(curr_stops) - 1):
        first = input_dir + fname_prefix + curr_stops[j] + fname_suffix
        second = input_dir + fname_prefix + curr_stops[j+1] + fname_suffix
        output = output_dir + fname_prefix + curr_stops[j] + "_stop2_" + curr_stops[j+1] + fname_suffix
        with open(first, "r") as f, open(second, "r") as s, open(output, "w") as out:
            former = json.load(f)
            later = json.load(s)
            print("Read files %s and %s" % (first, second))
            for item, item2 in zip(former["info"], later["info"]):
                if (item["p"] != None and item2["p"] != None):
                    for vehicle_stop1 in item["p"]["l"][0]["vs"]:
                        for vehicle_stop2 in item2["p"]["l"][0]["vs"]:
                            if (vehicle_stop2["p"] == vehicle_stop1["p"]):
                                if (vehicle_stop2["ta"] == vehicle_stop1["ta"]):
                                    str_to_write = "%s, %s, %s, %s, %s, %s\n" %(curr_line, curr_stops[j], vehicle_stop1["t"], curr_stops[j+1], vehicle_stop2["t"], vehicle_stop2["ta"])
                                out.write(str_to_write)
        j+=1
    i+=1
    print("i is " + str(i))

