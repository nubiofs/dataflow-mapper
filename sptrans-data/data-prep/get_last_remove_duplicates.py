#!/usr/bin/python3

import os
import sys
import collections


dirpath = os.path.abspath(os.path.dirname(sys.argv[0])) + "/"

#fname = "/tmp/20190831_output.txt.0.1567346175506.tmp"
#fname2 = "/tmp/20190831_output_clean.txt"
fname = "/tmp/flink_201909.txt"
fname2 = "/tmp/flink_201909_clean.txt"

line_stop1_stop2 = collections.OrderedDict()


#with open(dirpath + fname, "r") as infile:
with open(fname, "r") as infile:
    for line in infile:
        if ("," in line):
            key = line.split(",")[0]
            value = line.split(",")[1]
            line_stop1_stop2[key] = value

with open(fname2, "w") as outfile:
    for key in line_stop1_stop2:
        content = key + "=" + line_stop1_stop2[key]
        outfile.write(content)
    