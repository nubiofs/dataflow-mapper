#!/usr/bin/python3

import os
import time
import sys
from json import dumps
from kafka import KafkaProducer
from datetime import datetime

curr_dir = os.path.dirname(os.path.abspath(__file__))
#pre_name = curr_dir + "/pos_output_uniq_sample_september23.txt"
pre_name = curr_dir + "/pos_uniq_only_september.txt"

producer = KafkaProducer(bootstrap_servers=['localhost:9092'],
                         value_serializer=lambda x: dumps(x).encode('utf-8'))

# Define a function for the thread
def read_send_data(fname):
    count = 0
    try:
        now = datetime.now()
        current_time = now.strftime("%H:%M:%S")
        print("Current Time =", current_time)

        with open(fname, "r") as infile:
            line = infile.readline().strip()
            while line and count<10:
                i = 0
                while i < 2000:
                    producer.send('example', value=line)
                    line = infile.readline().strip()
                    i+=1

                #0.10 seconds
                time.sleep(0.10)
                count+=1
                now = datetime.now()
                current_time = now.strftime("%H:%M:%S")
                print("Current Time =", current_time)


    except Exception as e:
        print(e)
        sys.exit(1)


#Call method
read_send_data(pre_name)

#now = datetime.now()
#current_time = now.strftime("%H:%M:%S")
#print("Current Time =", current_time)



#Time to read all from Apex before breaking the connection with kafka
time.sleep(5)

