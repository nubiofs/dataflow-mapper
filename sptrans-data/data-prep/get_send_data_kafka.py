#!/usr/bin/python3

import json
import os
import time
import sys
import subprocess

with open("positions_output.txt", "r") as infile:
    i = 0
    #Replace kafka-path by respective path
    kafka_call = "sh <kafka-path>/kafka_2.11-0.8.2.2/bin/kafka-console-producer.sh --broker-list localhost:9092 --topic example "
    for line in infile:
        os.system(kafka_call + "\n" + line)
        i+=1

        if (i >= 2000):
            print("Dormindo por 10 segundos")
            time.sleep(10)
            i=0

