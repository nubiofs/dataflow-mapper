#Starts a container
docker run --name <name_of_your_choice> --rm -i -t <container_name>

#List running containers
docker ps -a

#Enters into container bash 
docker exec -it <name_chosen_previously> bash
