#Save figure to file since it is being executed inside a docker container

import matplotlib.pyplot as plt; plt.rcdefaults()
import numpy as np
import matplotlib.pyplot as plt
import collections

N = 3
datasets = np.arange(N)
colors = ['b', 'r', 'g']

#April, May and June, respectively
#Line 1450
avg_1450 = np.array([2.162856932735186, 2.1854621427975345, 2.2455300610858466])
yerr_1450 = [0.003488037243564551, 0.0022554025025364847, 0.0025263605264556578]
lower_1450 = [2.153872, 2.179653, 2.239023]
upper_1450 = [2.171842, 2.191272, 2.252038]

#Line 1465
avg_1465 = np.array([1.9178806226645084, 1.927496294873502, 1.9277098423784882])
yerr_1465 = [0.0018654482654882514, 0.0011930203554939847, 0.0013520989481487305]
lower_1465 = [1.913076, 1.924423,1.924227]
upper_1465 = [1.922686, 1.930569, 1.931193]

#Line 1651
avg_1651 = np.array([1.8241051862673485, 1.7825938099150918, 1.7616919393455706])
yerr_1651 = [0.005581629709438286, 0.0036223298770755147, 0.0037724127805065094]
lower_1651 = [1.809728, 1.773263, 1.751975]
upper_1651 = [1.838483, 1.791924, 1.771409]

#Line 198
avg_198 = np.array([2.421301634524826, 2.350705820299872, 2.321750497784634])
yerr_198 = [0.0034631422860409056, 0.0020651195468005218, 0.0021956310586993383]
lower_198 = [2.412381, 2.345386, 2.316095]
upper_198 = [2.430222, 2.356025, 2.327406]

#Line 32772
avg_32772 = np.array([1.7300084424691082, 1.7671309510943847, 1.8000531124620498])
yerr_32772 = [0.002466565721417647, 0.001724291858369888, 0.002077293823754328]
lower_32772 = [1.723655, 1.762689, 1.794702]
upper_32772 = [1.736362, 1.771572, 1.805404]

#Line 34694
avg_34694 = np.array([2.089993445780554, 2.0800204686889585, 2.1834700675246577])
yerr_34694 = [0.006074589152717183, 0.0038262402552365366, 0.0048398083940195576]
lower_34694 = [2.074346, 2.070165, 2.171004]
upper_34694 = [2.105641, 2.089876, 2.195937]



lines = ["1450", "1465", "1651", "198", "32772", "34694"]
lines_avg = {"1450": avg_1450, "1465": avg_1465, "1651": avg_1651, 
             "198": avg_198, "32772": avg_32772, "34694": avg_34694}

lines_error = {"1450": yerr_1450, "1465": yerr_1465, "1651": yerr_1651, 
               "198": yerr_198, "32772": yerr_32772, "34694": yerr_34694} 

lines_lower = {"1450": lower_1450, "1465": lower_1465, "1651": lower_1651, 
             "198": lower_198, "32772": lower_32772, "34694": lower_34694}

lines_upper = {"1450": upper_1450, "1465": upper_1465, "1651": upper_1651, 
             "198": upper_198, "32772": upper_32772, "34694": upper_34694}


def autolabel(bars,lower, upper):
    for i,bar in enumerate(bars):
        height = bars[i]
        plt.text(datasets[i], height - 0.14, '%s'% (lower[i]), ha='center', va='bottom', fontsize=12)
        plt.text(datasets[i], height + 0.04, '%s'% (upper[i]), ha='center', va='bottom', fontsize=12)
      
for line in lines:

    plt.ylabel('Tempo (minutos)', fontsize=12)
    plt.xlabel('Meses de 2019', fontsize=12)
    plt.grid(True)
    title = "Tempo médio e IC entre paradas da linha " + line
    plt.title(title, fontsize=12)

    fig = plt.figure(1, figsize=(7, 2.5))

    plt.ylim(0, 2.65)                                                                                                                          
    plt.bar(datasets, lines_avg[line], yerr=lines_error[line], capsize=1.2, alpha=0.7, color=colors)
  
    plt.xticks(datasets, ('Abril', 'Maio', 'Junho'), fontsize=12)

    autolabel(lines_avg[line], lines_lower[line], lines_upper[line])

    #Increase scale of both axis                       
    plt.rc('xtick', labelsize=12)
    plt.rc('ytick', labelsize=12)

    fig_title = 'confidence_interval_chart_' + line + '.pdf'                       
    plt.savefig(fig_title, format='pdf')
    plt.close()


