

calcInterval <- function(avg, std_error){
  #95% of confidence
  #error <- qt(0.975, df = n - 1) * s/sqrt(n)
  #This is 99% confidence interval
  error <- qnorm(0.995) * std_error
  avg - error
  avg + error
  error
  cat("Interval is from ",  avg - error, " to ", avg + error, "\n")
  #cat("Error is ", error, "\n")
}



lines <- c('1450', '1465', '1651', '198', '32772', '34694')
months <- c('April', 'May', 'June')

#Line 1450
avg_1450 <- c(2.162856932735186, 2.1854621427975345, 2.2455300610858466)
std_error_1450 <- c(0.003488037243564551, 0.0022554025025364847, 0.0025263605264556578)

#Line 1465
avg_1465 <- c(1.9178806226645084, 1.927496294873502, 1.9277098423784882)
std_error_1465 <- c(0.0018654482654882514, 0.0011930203554939847, 0.0013520989481487305)

#Line 1651
avg_1651 <- c(1.8241051862673485, 1.7825938099150918, 1.7616919393455706)
std_error_1651 <- c(0.005581629709438286, 0.0036223298770755147, 0.0037724127805065094)

#Line 198
avg_198 <- c(2.421301634524826, 2.350705820299872, 2.321750497784634)
std_error_198 <- c(0.0034631422860409056, 0.0020651195468005218, 0.0021956310586993383)

#Line 32772
avg_32772 <- c(1.7300084424691082, 1.7671309510943847, 1.8000531124620498)
std_error_32772 <- c(0.002466565721417647, 0.001724291858369888, 0.002077293823754328) 

#Line 34694
avg_34694 <- c(2.089993445780554, 2.0800204686889585, 2.1834700675246577)
std_error_34694 <- c(0.006074589152717183, 0.0038262402552365366, 0.0048398083940195576)


lines_avg <- c(avg_1450, avg_1465, avg_1651, avg_198, avg_32772, avg_34694)
lines_std_error <- c(std_error_1450, std_error_1465, std_error_1651, std_error_198, std_error_32772, std_error_34694)



i <- 0
for (line in lines) {
    m <- 1
    i <- i + 1
    cat("=================\n")
    cat("Linha ", line, "\n")
    cat("=================\n")

    for (month in months) {
        cat(month, "data: ")
        #The elements were saved sequentially
        ind <- 3*(i-1) + m
        avg <- lines_avg[ind]
        std_error <- lines_std_error[ind]
        calcInterval(avg, std_error)
        m <- m + 1
    }
}




