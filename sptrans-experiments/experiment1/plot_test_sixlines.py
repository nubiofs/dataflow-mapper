import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from matplotlib.colors import colorConverter as cc
import numpy as np
 
def plot_mean_and_CI(mean, lb, ub, color_mean=None, color_shading=None):
    # plot the shaded range of the confidence intervals
    plt.fill_between(range(mean.shape[0]), ub, lb,
                     color=color_shading, alpha=.5)
    # plot the mean on top
    plt.plot(mean, color_mean)



lines = ['1450', '1465', '1651', '198', '32772', '34694']

#Lines 1450, 1465, 1651, 198, 32772, 34694

#Line 1450
avg_1450 = np.array([8.32068665762172, 8.356397430363355, 8.652139186799692])
lower_1450 = np.array([8.25, 8.31, 8.60])
upper_1450 = np.array([8.39, 8.40, 8.70])

#Line 1465
avg_1465 = np.array([8.25274367202988, 8.252341743646747, 8.439851987901275])
lower_1465 = np.array([8.21, 8.22, 8.41])
upper_1465 = np.array([8.30, 8.28, 8.47])

#Line 1651
avg_1651 = np.array([1.824019285557747, 1.7825938099150918, 1.7616919393455706])
lower_1651 = np.array([1.81, 1.77, 1.75])
upper_1651 = np.array([1.84, 1.79, 1.77])

#Line 198
avg_198 = np.array([4.020659473399131, 3.848878812641185, 3.779591727611006])
lower_198 = np.array([3.98, 3.83, 3.75])
upper_198 = np.array([4.05, 3.87, 3.80])

#Line 32772
avg_32772 = np.array([3.855326341934594, 3.8848632505501413, 3.918607045724065])
lower_32772 = np.array([3.81, 3.86, 3.89])
upper_32772 = np.array([3.89, 3.91, 3.95])

#Line 34694
avg_34694 = np.array([4.987437285189428, 5.048703921579259, 5.114771235164387])
lower_34694 = np.array([4.94, 5.02, 5.08])
upper_34694 = np.array([5.04, 5.08, 5.15])



# plot the data
fig = plt.figure(1, figsize=(7, 2.5))
#plot_mean_and_CI(avg_1450, upper_1450, lower_1450, color_mean='k', color_shading='k')
#plot_mean_and_CI(avg_1465, upper_1465, lower_1465, color_mean='b', color_shading='b')
#plot_mean_and_CI(avg_1651, upper_1651, lower_1651, color_mean='g--', color_shading='g')
plot_mean_and_CI(avg_198, upper_198, lower_198, color_mean='y', color_shading='y')
plot_mean_and_CI(avg_32772, upper_32772, lower_32772, color_mean='r', color_shading='r')
#plot_mean_and_CI(avg_34694, upper_34694, lower_34694, color_mean='c--', color_shading='c')
 
class LegendObject(object):
    def __init__(self, facecolor='red', edgecolor='white', dashed=False):
        self.facecolor = facecolor
        self.edgecolor = edgecolor
        self.dashed = dashed
 
    def legend_artist(self, legend, orig_handle, fontsize, handlebox):
        x0, y0 = handlebox.xdescent, handlebox.ydescent
        width, height = handlebox.width, handlebox.height
        patch = mpatches.Rectangle(
            # create a rectangle that is filled with color
            [x0, y0], width, height, facecolor=self.facecolor,
            # and whose edges are the faded color
            edgecolor=self.edgecolor, lw=3)
        handlebox.add_artist(patch)
 
        # if we're creating the legend for a dashed line,
        # manually add the dash in to our rectangle
        if self.dashed:
            patch1 = mpatches.Rectangle(
                [x0 + 2*width/5, y0], width/5, height, facecolor=self.edgecolor,
                transform=handlebox.get_transform())
            handlebox.add_artist(patch1)
 
        return patch
 
bg = np.array([1, 1, 1, 1, 1, 1])  # background of the legend is white
colors = ['black', 'blue', 'green', 'yellow', 'red', 'cyan']
# with alpha = .5, the faded color is the average of the background and color

#colors_faded = [(np.array(cc.to_rgb(color)) + bg) / 2.0 for color in colors]

#plt.legend([0, 1, 2, 3, 4, 5], ['Data 0', 'Data 1', 'Data 2', 'Data 3', 'Data 4', 'Data 5'],
#           handler_map={
#               0: LegendObject(colors[0], colors_faded[0]),
#               1: LegendObject(colors[1], colors_faded[1]),
#               2: LegendObject(colors[2], colors_faded[2], dashed=True),
#               3: LegendObject(colors[3], colors_faded[3]),
#               4: LegendObject(colors[4], colors_faded[4]),
#               5: LegendObject(colors[5], colors_faded[5], dashed=True),
#            })
 
plt.title('Example mean and confidence interval plot')
plt.tight_layout()
plt.grid()
plt.savefig('example20200223.pdf', format='pdf')
#plt.show()
