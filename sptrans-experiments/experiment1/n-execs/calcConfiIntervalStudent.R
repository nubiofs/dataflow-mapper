

calcInterval <- function(a, s){
  n <- 30
  #95% of confidence
  #error <- qt(0.975, df = n - 1) * s/sqrt(n)
  error <- qt(0.995, df = n - 1) * s/sqrt(n)
  a - error
  a + error
  paste("Interval is from ",  a-error, " to ", a+error, sep="")
}


paste("Apex - April data")
avg <- 43.27
stdDev <- 2.64
calcInterval(avg, stdDev)

paste("Apex - May data")
avg <- 59.00
stdDev <- 2.42
calcInterval(avg, stdDev)

paste("Apex - June data")
avg <- 49.57
stdDev <- 2.51
calcInterval(avg, stdDev) 


paste("Flink - April data")
avg <- 22.90
stdDev <- 01.03
calcInterval(avg, stdDev)

paste("Flink - May data")
avg <- 41.37
stdDev <- 01.38
calcInterval(avg, stdDev)

paste("Flink - June data")
avg <- 36.40
stdDev <- 01.10

calcInterval(avg, stdDev)


