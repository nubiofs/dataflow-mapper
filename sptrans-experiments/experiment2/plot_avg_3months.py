#!/usr/bin/env python
import matplotlib.pyplot as plt; plt.rcdefaults()
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D

markers={'Abril':'o', 'Maio':'^', 'Junho':'s'}
colors={'Abril':'b', 'Maio':'g', 'Junho':'r'}

def plot_in_minutes(std_error, month):
    #plt.errorbar(lines, std_error, label=month)
    plt.scatter(lines, std_error, alpha=0.9, marker=markers[month], 
    color=colors[month], facecolors='none', s=45, label=month)

plt.grid()
plt.xlabel('Linhas de ônibus')
plt.ylabel('Tempo (minutos)')
plt.title('Média dos valores máximos para os meses de abril, maio e junho')

lines = ['1450', '1465', '1651', '198', '32772', '34694']

#########
#Max#
########
std_error = [20.642857142857142, 15.0, 3.0, 16.09090909090909, 10.454545454545455, 10.545454545454545]
plot_in_minutes(std_error, 'Abril')

std_error = [20.642857142857142, 14.147058823529411, 3.25, 16.272727272727273, 10.454545454545455, 11.0]
plot_in_minutes(std_error, 'Maio')

std_error = [19.142857142857142, 14.235294117647058, 3.25, 16.818181818181817, 10.545454545454545, 11.0]
plot_in_minutes(std_error, 'Junho')

plt.legend()
plt.savefig('avg_max_3months.pdf', format='pdf')

plt.close()

plt.grid()
plt.xlabel('Linhas de ônibus')
plt.ylabel('Tempo (minutos)')
plt.title('Média dos valores mínimos para os meses de abril, maio e junho')

#########
#Min#
########
std_error = [4.928571428571429, 3.0588235294117645, 1.5, 5.2727272727272725, 2.8181818181818183, 3.5454545454545454]
plot_in_minutes(std_error, 'Abril')

std_error = [2.7142857142857144, 2.735294117647059, 1.25, 5.0, 2.3636363636363638, 2.0]
plot_in_minutes(std_error, 'Maio')

std_error = [5.071428571428571, 3.411764705882353, 1.25, 5.090909090909091, 2.727272727272727, 3.272727272727273]
plot_in_minutes(std_error, 'Junho')

plt.legend()
plt.savefig('avg_min_3months.pdf', format='pdf')


