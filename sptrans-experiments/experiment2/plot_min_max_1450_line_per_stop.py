#Save figure to file since it is being executed inside a docker container

import matplotlib.pyplot as plt; plt.rcdefaults()
import numpy as np
import matplotlib.pyplot as plt
import collections

N = 14
datasets = np.arange(N)

months = ["Abril", "Maio", "Junho"]


#Line 34694
max_time =  {
    "Abril" : [6.0, 4.0, 8.0, 5.0, 9.0, 14.0, 13.0, 14.0, 19.0, 12.0, 4.0, 13.0, 3.0, 4.0],
    "Maio" : [9.0, 6.0, 9.0, 8.0, 12.0, 16.0, 22.0, 14.0, 7.0, 5.0, 4.0, 6.0, 4.0, 4.0],
    "Junho" : [8.0, 4.0, 10.0, 6.0, 11.0, 15.0, 14.0, 13.0, 13.0, 5.0, 8.0, 7.0, 11.0, 12.0]
}


min_time =  {
    "Abril": [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 2.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
    "Maio": [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
    "Junho" : [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 2.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0]
}

avg_time = {
    "Abril": [1.4344919015505218, 1.1323001436093825, 2.1878054894097003, 1.7818350816624617, 3.0345348586229224, 2.3285483870967743, 5.105947223316266, 2.0940525587828493, 2.9402337631039885, 1.802929427430093, 1.6385140037730372, 2.128614660390047, 1.4030110017371165, 1.2877937201081306], 
    "Maio": [1.4713171995225882, 1.1526085351057658, 2.2480364479973165, 1.798817434066095, 3.1384540435206234, 2.55740359140418, 4.995093314641198, 1.946090749608838, 2.8980850373255436, 1.8146536306188148, 1.6886533176285827, 2.094803738317757, 1.4212290502793297, 1.3067428150331615],
    "Junho": [1.5313062148204026, 1.1585226245707057, 2.214983277591973, 1.7392037668224463, 2.8300479488545554, 2.358517949374653, 4.946314972847168, 2.0053126048540433, 2.9757564003103183, 1.7907092907092907, 1.645540268975112, 2.102659189580318, 1.465643274853801, 1.3113070655207055]
}


for month in months:

    plt.ylabel('Tempo (minutos)')
    plt.xlabel('Conjunto de paradas consecutivas')
    plt.grid(True)
    title = "Tempos máximo, médio e mínimo entre paradas - " + month.lower() + " de 2019"
    plt.title(title)

    fig = plt.figure(1, figsize=(7, 2.5))

    plt.ylim(0, 23)
    
    plt.bar(datasets, max_time[month], alpha=0.9, color='b', label='Máximos')
    plt.bar(datasets, avg_time[month], alpha=0.9, color='m', label='Tempo médio')
    plt.bar(datasets, min_time[month], alpha=0.9, color='r', label='Mínimos')


    plt.xticks(datasets, ('1-2', '', '3-4', '', '5-6', '', '7-8', '', '9-10', '', '11-12', '', '13-14', ''))

    fig_title = 'min_avg_max_chart_' + month + '.pdf'   

    plt.legend()                    
    plt.savefig(fig_title, format='pdf')
    plt.close()
