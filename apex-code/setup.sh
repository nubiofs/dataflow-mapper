#!/bin/bash

# Install softwares
apt-get update -y
apt-get -y -o APT::Immediate-Configure=false install wget sudo lsb-release
apt-get install -y -q --no-install-recommends apt-utils openjdk-8-jre-headless vim screen curl unzip man openssh-server maven git

#Set maven home
echo "export M2_HOME=/usr/share/maven" >> /etc/bash.bashrc
mvn -v

#Apex Core
git clone https://github.com/apache/apex-core.git
cd apex-core
mvn install
cd ..

#Apex Malhar
git clone https://github.com/apache/apex-malhar.git
cd apex-malhar
mvn install
cd ..

#DataTorrent Examples
git clone https://github.com/dtpublic/examples.git


## clean up
apt-get autoclean

