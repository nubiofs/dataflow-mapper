/**
 * Put your copyright and license info here.
 */
package com.nanda.twoOutputs;

import org.apache.hadoop.conf.Configuration;

import com.datatorrent.api.annotation.ApplicationAnnotation;
import com.datatorrent.api.StreamingApplication;
import com.datatorrent.api.DAG;
import com.datatorrent.api.DAG.Locality;
import com.datatorrent.lib.io.ConsoleOutputOperator;

@ApplicationAnnotation(name="OneInputTwoOut")
public class Application implements StreamingApplication
{

  @Override
  public void populateDAG(DAG dag, Configuration conf) {
    RandomNumberGenerator randomGenerator = dag.addOperator("randomGenerator", RandomNumberGenerator.class);

    ConsoleOutputOperator cons = dag.addOperator("console", new ConsoleOutputOperator());

    FileWriter w = dag.addOperator("writer", new FileWriter());

    dag.addStream("multipleConnections", randomGenerator.out, cons.input).setLocality(Locality.CONTAINER_LOCAL);

    dag.addStream("data", randomGenerator.out2, w.input).setLocality(Locality.CONTAINER_LOCAL);
  }
}
