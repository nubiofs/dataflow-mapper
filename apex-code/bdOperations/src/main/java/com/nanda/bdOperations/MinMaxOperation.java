package com.nanda.bdOperations;

import com.datatorrent.api.DAG;
import com.datatorrent.api.StreamingApplication;
import com.datatorrent.lib.io.ConsoleOutputOperator;
import com.datatorrent.api.Context.OperatorContext;
import com.datatorrent.lib.math.Max;
import com.datatorrent.lib.math.Min;
import org.apache.hadoop.conf.Configuration;


public class MinMaxOperation implements StreamingApplication {
    @SuppressWarnings("unchecked")
    @Override
    public void populateDAG(DAG dag, Configuration conf) {
        dag.setAttribute(DAG.APPLICATION_NAME, "AverageSampleApplication");

        FileInputOperator fileIn = dag.addOperator("fileInput", new FileInputOperator());
        fileIn.setDirectory("/tmp/teste.txt");

        /*Max*/
        Max<Double> max = dag.addOperator("max", Max.class);
        dag.addStream("max", fileIn.output, max.data);
        dag.getMeta(max).getAttributes()
                .put(OperatorContext.APPLICATION_WINDOW_COUNT, 20);

        ConsoleOutputOperator console = dag.addOperator("console", new ConsoleOutputOperator());
        dag.addStream("max_out", max.max, console.input);

        /******************************************************/
        FileInputOperator fileIn2 = dag.addOperator("fileInput2", new FileInputOperator());
        fileIn2.setDirectory("/tmp/teste.txt");

        /*Min*/
        Min<Double> min = dag.addOperator("min", Min.class);
        dag.addStream("min", fileIn2.output, min.data);
        dag.getMeta(min).getAttributes()
                .put(OperatorContext.APPLICATION_WINDOW_COUNT, 1);

        ConsoleOutputOperator console2 = dag.addOperator("console2", new ConsoleOutputOperator());
        dag.addStream("min_out", min.min, console2.input);
    }

}
