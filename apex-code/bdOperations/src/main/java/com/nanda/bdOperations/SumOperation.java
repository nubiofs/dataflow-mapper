package com.nanda.bdOperations;

import com.datatorrent.api.Context;
import com.datatorrent.api.DAG;
import com.datatorrent.api.StreamingApplication;
import com.datatorrent.lib.io.ConsoleOutputOperator;
import com.datatorrent.lib.math.Sum;
import org.apache.hadoop.conf.Configuration;

public class SumOperation implements StreamingApplication {
    @SuppressWarnings("unchecked")
    @Override
    public void populateDAG(DAG dag, Configuration conf) {
        dag.setAttribute(DAG.APPLICATION_NAME, "AverageSampleApplication");

        FileInputOperator fileIn = dag.addOperator("fileInput", new FileInputOperator());
        fileIn.setDirectory("/tmp/teste.txt");

        /*Sum*/
        Sum<Double> sum = dag.addOperator("sum", Sum.class);
        dag.addStream("sum", fileIn.output, sum.data);
        dag.getMeta(sum).getAttributes()
                .put(Context.OperatorContext.APPLICATION_WINDOW_COUNT, 5);

        ConsoleOutputOperator console = dag.addOperator("console", new ConsoleOutputOperator());
        dag.addStream("sum_out", sum.sum, console.input);

    }
}