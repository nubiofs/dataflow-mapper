package com.nanda.kafka08;

import com.datatorrent.contrib.kafka.KafkaSinglePortByteArrayInputOperator;
import com.datatorrent.lib.io.ConsoleOutputOperator;
import org.apache.hadoop.conf.Configuration;

import com.datatorrent.api.DAG;
import com.datatorrent.api.StreamingApplication;
import com.datatorrent.api.annotation.ApplicationAnnotation;

@ApplicationAnnotation(name="Kafka2Hdfs")
public class Kafka2Hdfs implements StreamingApplication {

    @Override
    public void populateDAG(DAG dag, Configuration conf) {

        KafkaSinglePortByteArrayInputOperator in
                = dag.addOperator("kafkaIn", new KafkaSinglePortByteArrayInputOperator());

        LineOutputOperator out = dag.addOperator("fileOut", new LineOutputOperator());



        dag.addStream("data", in.outputPort, out.input);
    }
}