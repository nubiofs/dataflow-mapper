#!/usr/bin/python3

import os
import sys

dirpath = os.path.abspath(os.path.dirname(sys.argv[0])) + "/"

def usage():
    print("Usage: python3 item_receiver.py [mapper] [processingChoice] [dataType] [dataName] [constructor]")
    print("[mapper]: chosen mapper")
    print("[processingChoice]: batch or real time processing")
    print("[caller]: the variable that receives the content")
    print("[key]: HashMap key")
    print("[value]: HashMap value")

def write_to_file(fname, varStructure):
    with open(fname, "a") as f:
        f.write("\n//placeholder\n")
        f.write(varStructure)


if (len(sys.argv) < 4):
    usage()
    sys.exit(1)

mapper = sys.argv[1].lower()
processing_choice =  sys.argv[2].lower()
caller = sys.argv[3]
print(mapper)

if (len(sys.argv) == 6): 
    key = sys.argv[4]
    value = sys.argv[5]
    varStructure = "%s.put(%s, %s);\n" % (caller, key, value)
else:
    varStructure = "HashMap<String, Object> %s = new HashMap<>();\n" % (caller)

print("varStructure")
print(varStructure)

if (mapper == "flink" or mapper == "both"):
    fname = dirpath + "../flink/out/content.java"
    write_to_file(fname, varStructure)


if (mapper == "apex" or mapper == "both"):
    fname = dirpath + "../apex/out/content.java"
    write_to_file(fname, varStructure)

