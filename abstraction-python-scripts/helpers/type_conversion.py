""" This class is a helper for the translation process, i.e.
the abstraction user will provide a code for his/her UDF 
using our API and then this code needs to be converted to the
appropriated ones that are intelligible for Flink and Apex"""

import re


def convert_to_flink_syntax(to_analyse):

    possible_words = {
        "batch": ["batch", "Batch", "File"],
        "stream": ["stream", "Stream", "Kafka"],
        "KV" : "Tuple2"
    }

    for word in possible_words["batch"]:
        if (word in to_analyse):
            to_convert.replace("Data", "DataSet")
    for word in possible_words["stream"]:
        if (word in to_analyse):
            to_convert.replace("Data", "DataStream")
    
    word = "KV"
    if (word in to_convert):
        to_convert.replace(word, possible_words[word])

    return to_convert   

def convert_to_apex_syntax(to_convert):

    possible_words = {
        "Data": "ApexStream",
        "KV" : "KeyValPair"
    }

    for key in possible_words:
        if (key in to_convert):
            to_convert = to_convert.replace(key, possible_words[key])

    return to_convert
