#!/usr/bin/python3

import os
import sys

dirpath = os.path.abspath(os.path.dirname(sys.argv[0])) + "/"

def usage():
    print("Usage: python3 item_receiver.py [mapper] [processingChoice] [dataType] [dataName] [constructor]")
    print("[mapper]: chosen mapper")
    print("[processingChoice]: batch or real time processing")
    print("[dataTypeF]: type of flink data structure (generics)")
    print("[dataTypeA]: type of apex data structure (generics)")
    print("[dataName]: name given to the data structure which receives the transformed data")
    print("[contructor]: optional parameter, it works as a flag to indicate it is a constructor declaration")


def write_to_file(fname, varStructure, newInstantiation, constructor):
    with open(fname, "a") as f:
        f.write("\n\n//placeholder\n")
        if (constructor):
            f.write(varStructure + newInstantiation)
        else:
            f.write(varStructure)



if (len(sys.argv) < 5):
    usage()
    sys.exit(1)

mapper = sys.argv[1].lower()
processingChoice =  sys.argv[2].lower()
dataTypeF = sys.argv[3]
dataTypeA = sys.argv[4]
dataName = sys.argv[5]
constructor = False
print(mapper)

if (len(sys.argv) == 7):
    if (sys.argv[6] == "constructor"):
        constructor = True

print("CONSTRUCTOR ", constructor)

varStructureF = "<" + dataTypeF + "> " + dataName + " = "
varStructureA = "<" + dataTypeA + "> " + dataName + " = "
newInstantiation  = "new "

print("varStructure")
print(varStructureF)
print(varStructureA)

if (mapper == "flink" or mapper == "both"):
    fname = dirpath + "../flink/out/content.java"
    if (processingChoice == "batch"):
        varStructureF = "DataSet" + varStructureF
        newInstantiation += "DataSet<>();\n"
    else:
        varStructureF = "DataStream" + varStructureF
        newInstantiation += "DataStream<>();\n"

    write_to_file(fname, varStructureF, newInstantiation, constructor)


if (mapper == "apex" or mapper == "both"):
    #Clean possible dummy content inside variable
    newInstantiation  = "new "

    fname = dirpath + "../apex/out/content.java"
    varStructureA = "ApexStream" + varStructureA
    newInstantiation += "ApexStream<>();\n"

    write_to_file(fname, varStructureA, newInstantiation, constructor)

