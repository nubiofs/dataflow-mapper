#!/usr/bin/python3

import os
import sys

dirpath = os.path.abspath(os.path.dirname(sys.argv[0])) + "/"

def usage():
    print("Usage: python3 redirector.py [content] [body]")
    print("[content]: string with content to be copied")
    print("[body]: 'import' if it is an import, 'body' if it is all the rest")

def write_file(fname, fcontent):
    print(fname)
    print(fcontent)
    with open(dirpath + fname, "a") as f:
        f.write(fcontent)

if (len(sys.argv) < 3):
    usage()
    sys.exit(1)

content_to_copy = sys.argv[1]
role = sys.argv[2].lower()

if (role == "import"):
    file_name = "imports.java"
else:
    file_name = "content.java"

path_to_folder = "../flink/out/"
write_file(path_to_folder + file_name, content_to_copy)

path_to_folder = "../apex/out/"
write_file(path_to_folder + file_name, content_to_copy)

