#!/usr/bin/python3

import sys
import os

dirpath = os.path.abspath(os.path.dirname(sys.argv[0])) + "/"

#Separate imports from code
def read_java_file(fname):
    print(fname)
    to_import = ""
    content = ""
    udf = ""

    with open(dirpath + fname, "r") as f:
        temp = f.readline()

        while (temp and temp.startswith("import")):
            to_import += temp
            temp = f.readline()

        while (temp and not(temp.startswith("//UDF"))):
            content += temp
            temp = f.readline()

        while temp:
            udf += f.read()
            temp = f.readline()

    f.close()

    return to_import, content, udf


def write_java_file(fname, fcontent):

    with open(dirpath + fname, "a") as f:
        f.write(fcontent)

    f.close()