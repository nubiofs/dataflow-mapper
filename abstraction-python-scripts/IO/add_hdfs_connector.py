#!/usr/bin/python3

import sys
import os
from shutil import copyfile

dirpath = os.path.abspath(os.path.dirname(sys.argv[0])) + "/"

def usage():
    print("Usage: python3 add_hdfs_connector.py package_name action")
    print("[package_name]: name of the package created by the abstraction user")
    print("[action]: read, readA, write or writeA")

def copy_connector_output(fname):
    source = dirpath + "io-apex/connectors/" + fname
    destination = rel_path + fname
    with open(source, "r") as src, open(destination, "w") as out:
        out.write(package)
        content = ("").join(src.readlines())
        
        out.write(content)
        

if (len(sys.argv) < 3):
    usage()
    sys.exit(1)

package_name = sys.argv[1]
action = sys.argv[2]

global package
package = "package com.nanda." + package_name + ";\n\n"

global rel_path
rel_path = dirpath + "../../generated-code/apex/" + package_name + "/src/main/java/com/nanda/" + package_name + "/"

print("Inside add_hdfs_connector")

# if (action in {"read", "readA"}):
#     if (defined_type2 == ""):
#         copy_connector_input("FileGenericInput.java", defined_type)
#     else:
#         copy_connector_KV_input("FileKVGenericInput.java", defined_type, defined_type2)

if (action in {"write", "writeA"}):
    copy_connector_output("LineOutputOperator.java")
else:
    print("Invalid action given.")
