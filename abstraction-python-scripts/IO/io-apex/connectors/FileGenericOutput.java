/*The code below is based on Apache Apex fileIO-simple
* and FileIO examples: https://github.com/dtpublic/examples
* */

import javax.validation.constraints.NotNull;

import com.datatorrent.api.Context;
import com.datatorrent.api.DefaultOutputPort;
import com.datatorrent.api.annotation.OutputPortFieldAnnotation;
import com.datatorrent.lib.io.fs.AbstractFileOutputOperator;

import java.nio.charset.StandardCharsets;

/*** Write incoming lines to output file */
public class File$TypeOutput extends AbstractFileOutputOperator<$Type> {
    private static final String NL = System.lineSeparator();

    @NotNull
    private String fileName;
    private String fName;    // per partition file name

    @Override
    public void setup(Context.OperatorContext context) {
        // create file name for this partition by appending the operator id to
        // the base name
        long id = context.getId();
        fName = fileName + "_p" + id;
        super.setup(context);
    }

    @Override
    protected String getFileName($Type tuple) { return fName; }

    @Override
    protected byte[] getBytesForTuple($Type line) {
        byte[] result = null;
        try {
            result = (line + NL).getBytes(StandardCharsets.UTF_8);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    public String getFileName() { return fileName; }
    public void setFileName(String v) { fileName = v; }
}
