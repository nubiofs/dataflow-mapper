#!/usr/bin/python3

#Join the pieces together
import sys
import os
import os.path as p
import re

def remove_file(filename):
    try:
        if (p.exists(filename)):
            os.remove(filename)
    except OSError as e:
        #print("Could not delete " + filename)
        pass


dirpath = os.path.abspath(os.path.dirname(sys.argv[0])) + "/"


def main():
    if (len(sys.argv) <= 3):
        print("Usage: python3 Manager.py [args]")
        print("arg1: package_name")
        print("arg2: class_name")
        print("arg3: framework name")
        sys.exit(1)

    package_name = sys.argv[1]
    class_name = sys.argv[2]
    tool = sys.argv[3]

    #Paths
    main_path = "../generated-code/" + tool + "/" + package_name + "/src/main/java/com/nanda/" + package_name + "/"
    #test_path = "../../generated-code/apex/" + package_name + "/src/test/java/com/nanda/" + package_name + "/"
    if (tool == "apex"):
        properties_path = "../generated-code/apex/" + package_name + "/src/main/resources/META-INF/"
        properties_file = dirpath + "properties-files/apex/out/properties.xml"
        end_properties_file = dirpath + properties_path + "properties-" + class_name + ".xml"
        parsed_file =  dirpath + tool + "/out/" + class_name + "ParsedA.java"
        try:
            print("Before ManagerA")
            #ManagerA.main(package_name, class_name)
            script_name = dirpath + "apex/ManagerA.py %s %s" % (package_name, class_name)
            status = os.system(script_name)
            print(status)
        except Exception as e:
            print("It was not possible to run ManagerA. " + e)
    elif (tool == "flink"):
        print("Inside Manager, tool is flink")
        properties_path = "../generated-code/flink/" + package_name + "/target/classes/"
        properties_file = dirpath + "properties-files/flink/out/general.properties"
        end_properties_file = dirpath + properties_path + class_name + ".properties"
        parsed_file =  dirpath + tool + "/out/" + class_name + "ParsedF.java"
    else:
        properties_path = properties_file = ""
        end_properties_file = parsed_file = ""

    #Names of files to be opened
    import_file = dirpath + tool + "/out/imports.java"
    content_file = dirpath + tool + "/out/content.java"
    udf_file =  dirpath + tool + "/out/udf.java"
    #test_file = dirpath + "testClass.java"

    #Name of generated files
    end_file = dirpath + main_path + class_name + ".java"
    #end_test_file = dirpath + test_path + class_name + "Test.java"

    content = udf = ''
    lines_seen = []

    #Creates properties file
    if (p.exists(properties_file)):
        if not p.exists(p.dirname(end_properties_file)):
            try:
                os.makedirs(os.path.dirname(end_properties_file))
            except OSError as exc: # Guard against race condition
                if exc.errno != errno.EEXIST:
                    raise
        with open(properties_file, "r") as in_file, open(end_properties_file, "w") as out_file:
            #if (tool == "apex"):
            #    out_file.write("<?xml version=\"1.0\"?>\n")
            for line in in_file:
                out_file.write(line)
            #print("It is supposed to write into ", end_properties_file)


    #Avoid repeated imports
    if (p.exists(import_file)):
        with open(import_file , "r") as infile:
            for line in infile:
                if line not in lines_seen:
                    lines_seen.append(line)

    if (p.exists(content_file)):
        with open(content_file , "r") as resp:
            content = resp.read()
        placeholder_pattern = re.compile("\\/\\/[\\s\\t]*placeholder")
        content = re.split(placeholder_pattern, content)[1:] #ignores whitespaces that comes before the first placeholder

    #print(content)
    if (p.exists(parsed_file)):
        with open(parsed_file, "r") as resp:
            parsed_content = resp.readlines()

        parsed_content[0] = parsed_content[0].replace("$packageName", package_name)
        pattern = re.compile("\\/\\*\\$placeholder(VariableDeclarationExpr|MethodCall)\\*\\/")

        i = 0
        line = 0
        while line < len(parsed_content) and i < len(content):
            while re.search(pattern, parsed_content[line]):
                resultsPattern = re.split(pattern, parsed_content[line], 1)
                beforePattern = resultsPattern[0]
                afterPattern = ""
                if (beforePattern.endswith(")")):
                    beforePattern += ";\n\t\t"

                if (len(resultsPattern) > 2):
                    afterPattern = resultsPattern[2]
                    #afterPattern = re.split(pattern, parsed_content[line])[-1]

                #Remove ";" char if afterPattern has some content different from whitespace
                if (content[i].strip().endswith(";") and not(re.match("^[ \t\n]*$", afterPattern))):
                    content[i] = content[i].strip()[:-1]
                parsed_content[line] = beforePattern + content[i].strip() + afterPattern + "\n"
                #parsed_content[line] = re.sub(pattern, content[i].strip() + " ", parsed_content[line])
                i+=1
            line+=1

        line = 0
        #Delete blank line if there are two consecutive blank lines
        while line < len(parsed_content):
            if re.match("^[ \t\n]*$", parsed_content[line]):
                if (line + 1 < len(parsed_content) and re.match("^[ \t\n]*$", parsed_content[line + 1])):
                    print("line is ", line)
                    del parsed_content[line]
                    line-=1
            line+=1

        if (p.exists(udf_file)):
            with open(udf_file, "r") as resp:
                udf = resp.readlines()

                parsed_line = parsed_content[-1]
                #While it is only blanks and enters but not a key
                while (re.match("^[ \t\n]*$", parsed_line) and not re.match("^[ \t\n]*}[ \t\n]*$", parsed_line)):
                    del parsed_content[-1]
                    if (len(parsed_content) > 0):
                        parsed_line = parsed_content[-1]
                    else:
                        parsed_line = ""
                #This is to remove the last "}"
                if (re.match("^[ \t\n]*}[ \t\n]*$", parsed_line)):
                    print("Deleting ", parsed_content[-1])
                    del parsed_content[-1]
                parsed_content += ["\n"] + udf + ["\n}"]

        #Generate the class with populateDAG function or main function in Flink case
        with open(end_file, "w") as resp:
            #parsed_content = ''.join(lines_seen) + parsed_content
            #package
            resp.write(parsed_content[0])
            resp.writelines(lines_seen + parsed_content[1:])
        print("Parsed file is ", parsed_file)

    #Clean temporary files
    #remove_file(import_file)
    #remove_file(content_file)
    remove_file(udf_file)
    #remove_file(properties_file)


if __name__ == '__main__':
    main()
