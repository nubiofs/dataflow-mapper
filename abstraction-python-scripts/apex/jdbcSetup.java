    @BeforeClass
    public static void setup() {
        try {
            Class.forName($databaseDriver).newInstance();

            Connection con = DriverManager.getConnection($databaseUrl);
            System.out.println("Connecting to the database...");
            System.out.println(con.toString());
            Statement stmt = con.createStatement();

            String createMetaTable = "CREATE TABLE IF NOT EXISTS " + JdbcTransactionalStore.DEFAULT_META_TABLE + " ( "
                    + JdbcTransactionalStore.DEFAULT_APP_ID_COL + " VARCHAR(100) NOT NULL, "
                    + JdbcTransactionalStore.DEFAULT_OPERATOR_ID_COL + " INT NOT NULL, "
                    + JdbcTransactionalStore.DEFAULT_WINDOW_COL + " BIGINT NOT NULL, " + "UNIQUE ("
                    + JdbcTransactionalStore.DEFAULT_APP_ID_COL + ", " + JdbcTransactionalStore.DEFAULT_OPERATOR_ID_COL + ", "
                    + JdbcTransactionalStore.DEFAULT_WINDOW_COL + ") " + ")";

            stmt.executeUpdate(createMetaTable);

            stmt.executeUpdate($query);
            
            //placeholder
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }