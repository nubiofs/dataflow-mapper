#!/usr/bin/python3

""" Generates properties file according to operators in use """
import sys
import os

dirpath = os.path.abspath(os.path.dirname(sys.argv[0])) + "/"

def usage():
    print("Usage: python3 add_properties_flink.py ARG1 ARG[2..n]")
    print("ARG1: connector type + (R)ead or (W)rite (kafkaR, kafkaW, fileR ...)")
    print("ARG[2..n]: properties to be put in the properties file")


def read_info(name):
    print("Inside add_properties_flink.read_info()")
    name = dirpath + name
    with open(name) as f:
        info = f.read()
    return info

def write_to_file(content):
    print("Inside add_properties_flink.write_to_file()")
    file_path = dirpath + "flink/out/general.properties"
    
    with open(file_path, "a") as f:
        f.write(content)

            
def main(args):
    
    if (len(sys.argv) < 3):
        usage()
        return
    
    args = sys.argv

    connector_type = args[1]
    content = ""

    print(connector_type)
    print("INSIDE add_properties_flink.py")
    if (connector_type == "kafkaR"):
        content = read_info("flink/kafka-input.properties")
        content = content.replace("$bootstrap", args[2])
        content = content.replace("$groupid", args[3])
    elif (connector_type == "kafkaW"):
        content = read_info("flink/kafka-output.properties")
        content = content.replace("$bootstrap", args[2])

    elif (connector_type == "rabbitmqR"):
        content = read_info("flink/rabbitmq-input.properties")
    elif (connector_type == "rabbitmqW"):
        content = read_info("flink/rabbitmq-output.properties")

    else:
        print("Wrong command given (should be kafka(R|W) format)")

    if (content != ""):
        write_to_file(content)


if __name__ == "__main__":
    main(sys.argv)
