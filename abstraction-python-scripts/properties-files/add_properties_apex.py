#!/usr/bin/python3

""" Generates properties file according to operators in use """
import sys
import os
from lxml import etree

dirpath = os.path.abspath(os.path.dirname(sys.argv[0])) + "/"
#sys.path.insert(0, os.path.abspath(os.path.dirname(sys.argv[0]) + "/"))

def usage():
    print("Usage: python3 AddProperties.py ARG1 ARG2 ARG[3..n]")
    print("ARG1: connector type + (R)ead or (W)rite (kafkaR, kafkaW, fileR ...)")
    print("ARG2: operator (name given when the operator was created)")
    print("ARG[3..n]: properties to be put in the properties file")


def read_info(name):
    name = dirpath + name
    with open(name) as f:
        info = f.read()
    return info


def replace_args(content, operator_name, args):
    root = etree.fromstring(content)
    i = 0

    for el in root.findall('property/name'):
        content = content.replace("$operator-name", operator_name)

    for el in root.findall('property/value'):
        if (len(args) <= i):
            return content
        value = el.text
        content = content.replace(value, args[i])
        i+=1

    return content


def write_to_file(content):
    print("Inside add_properties_apex.write_to_file()")
    file_path = dirpath + "apex/out/properties.xml"
    if (os.path.exists(file_path)):
        file_size = os.stat(file_path).st_size
    else:
        file_size = 0
    
    if file_size > 0:
        #Create element with content to be added
        prop = etree.fromstring(content)
        prop.tail = "\n"
        #Create the tree and get the root
        tree = etree.parse(file_path)
        root = tree.getroot()

        #Append the content        
        root.append(prop[0])
        for item in prop.findall('property'):
            item.tail = "\n\t"
            root.append(item)

        #Add the content to the file
        tree.write(file_path)
    else:
        with open(file_path, "w") as f:
            items = "\n".join(content.split("\n")[1:-1])
            f.write('<?xml version="1.0"?>\n<configuration>' + items + '\n</configuration>')

            
def main(args):
    
    if (len(sys.argv) < 4):
        usage()
        return
    
    args = sys.argv
    #Pop remove the item from the arguments
    args.pop(0)
    connector_type = args.pop(0)
    operator_name = args.pop(0)
    content = ""
    print("Arguments: ", args)

    if (connector_type == "kafkaR"):
        content = read_info("apex/kafka-input.xml")
    elif (connector_type == "kafkaW"):
        content = read_info("apex/kafka-output.xml")
            
    elif (connector_type == "fileTextR"):
        content = read_info("apex/file-input.xml")
    elif (connector_type == "fileTextW"):
        content = read_info("apex/file-output.xml")

    elif (connector_type == "fileHdfsR"):
        content = read_info("apex/hdfs-input.xml")
    elif (connector_type == "fileHdfsW"):
        content = read_info("apex/hdfs-output.xml")

    elif (connector_type == "jdbcR"):
        content = read_info("apex/jdbc-input.xml")
    elif (connector_type == "jdbcW"):
        content = read_info("apex/jdbc-output.xml")
    
    """ elif (connector_type == "rabbitmqR"):
        content = read_info("apex/rabbitmq-input.xml")
    elif (connector_type == "rabbitmqW"):
        content = read_info("apex/rabbitmq-output.xml") """

    if (content != ""):
        content = replace_args(content, operator_name, args)
        print(content)
        write_to_file(content)


if __name__ == "__main__":
    main(sys.argv)
