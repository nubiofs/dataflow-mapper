import org.apache.flink.api.java.utils.ParameterTool;

public class $className {

    public static void main(String[] args) throws Exception {

        InputStream input = $className.class.getResourceAsStream("/general.properties");
        ParameterTool parameters = ParameterTool.fromPropertiesFile(input);
        Properties prop = parameters.getProperties();