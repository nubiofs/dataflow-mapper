#!/usr/bin/python3

import sys
import os
import re

""" Important: ' and " are not interchangeable in Java. The simple quotes are for 
chars and the double one is for Strings. So, it is necessary to put double quotes and
find a way to scape the quotes from println tha the user will put in the code. """

dirpath = os.path.abspath(os.path.dirname(sys.argv[0])) + "/"

def usage():
    print("Usage: python3 parser.py [fileName]")
    print("[fileName]: name of the file containing user code to be parsed")

def join_line(line):
    global pure_java_content, leading_spaces
    if (pure_java_content != 'r.copy(String.join("+", '):
        pure_java_content += ' + '
    else:
        cont_leading_spaces = len(line) - len(line.lstrip())
        leading_spaces = line[:cont_leading_spaces]
    pure_java_content += '"' + line.replace('"', '\\"').rstrip() + '\\n"\n'


if (len(sys.argv) < 2):
    usage()
    sys.exit(1)

path_to_file = "../abstraction/src/main/java/com/nanda/abstraction/general/"
file_name = sys.argv[1]
input_file_name = path_to_file + file_name

""" As the code is developed, this list has to be updated """
api_list = [
"package", "main", "Data", "Dataflow", "File", "Kafka", 
"IO", "DataTransformation", "UDF", "ProjectSettings", "Auxiliar", ".setPackageName", 
".start", ".finish", ".read", ".write", ".map", ".flatMap", ".reduce", ".fold", 
".filter", ".sum", ".max", ".min"]


with open(input_file_name, "r") as f:
    content = f.readlines()

cont = 0
key_cont = 0
global pure_java_content, copy_to_file, leading_spaces
pure_java_content = 'r.copy(String.join("+", '
copy_to_file = ""
leading_spaces = ""
tabs = "\t\t"
key_diff_line = 0

#The split is to remove the .java from the name
output_file_name = path_to_file + file_name.split(".")[0] + "Parsed.java"
print(output_file_name)

searched_comment = "^[ \t]*/\*"
searched_simple_comment = "^[ \t]*//"
#searched_udf = "[ \t]*[a-zA-Z0-9]+\([ \ta-zA-Z0-9,<>\"]*[\) \t]*"
searched_udf = "[ \t]*[a-zA-Z0-9\(]+[a-zA-Z0-9,<> \t\)\{]*"
public_class = "[ \t]*public[ \t]*class"
public_main = "[ \t]*public[ \t]*static[ \t]*void[ \t]*main"

with open(output_file_name, "w") as out:
    idx = 0
    while idx < len(content):
        line = content[idx]

        if ("package" in line):
            out.write(line)
            out.write("\n\npublic class " + file_name.split(".")[0] + "Parsed {\n\n")
            out.write("\tstatic Redirector r = new Redirector();\n")
            out.write("\tpublic static void main(String[] args) throws Exception {\n\n")
        elif ("import" in line):
            out.write('\t\tr.copyImport("' + line.rstrip() + '\\n");\n')
        #Check for comments
        elif re.match(searched_comment, line):
            #The second checking is to avoid index out of bounds
            while ("*/" not in line and (idx + 1) < len(content)):
                join_line(line)
                line = content[idx + 1]
                idx = idx + 1
            if ("*/" in line):
                join_line(line)
        #Checking for simple comments, enters and spaces
        elif (re.match(searched_simple_comment, line) or line == "\n" or line.isspace() or "System.out.println" in line):
            join_line(line) 
            
        #Class name
        elif (re.match(public_class, line)):
            key_cont+=1
        #Main function
        elif (re.match(public_main, line)):
            tabs = ""
            join_line(line)
            key_cont+=1
            #This case happens if the key is in a different line from method
            while ("{" not in line and (idx + 1) < len(content)):
                idx += 1
                line = content[idx]
                key_diff_line = 1
            if (key_diff_line):
                content[idx] = line.replace("{", "")
                idx -= 1
                key_diff_line = 0

        #UDFs
        elif((("public" or "private") in line) and re.match(searched_udf, line.rstrip())):
            pure_java_content = tabs + leading_spaces + pure_java_content.rstrip() + '));\n'
            #Case when key is at another line
            if ("{" not in line):
                out.write(pure_java_content + "\t}\n" + line.rstrip() + " {" + '\n')
            else:
                out.write(pure_java_content + "\t}\n" + line.rstrip() + '\n')
            pure_java_content = 'r.copy(String.join("+", '
            join_line(line)
            key_cont+=1

            while ("{" not in line and (idx + 1) < len(content)):
                idx += 1
                line = content[idx]
                key_diff_line = 1
            if (key_diff_line):
                content[idx] = line.replace("{", "")
                idx -= 1
                key_diff_line = 0


        else:
            word = api_list[0]
            cont = 1
        
            #Checks if current line uses the provided API
            while (word not in line and cont < len(api_list)):
                word = api_list[cont]
                cont+= 1

            copy_to_file = ""
            if (word not in line):
                join_line(line)
                #copy_to_file = pure_java_content
            #copy previous lines to file
            if (word in line):
                #Current line is copied as it is
                if (pure_java_content != 'r.copy(String.join("+", '):
                    #The parentheses closes the String.join and r.copy methods
                    pure_java_content = tabs + leading_spaces + pure_java_content.rstrip() + '));\n'
                    copy_to_file = pure_java_content + line.rstrip() + '\n'
                    pure_java_content = 'r.copy(String.join("+", '
                else:
                    copy_to_file = line.rstrip() + '\n'

                out.write(copy_to_file)
                copy_to_file = ""

        idx += 1
        print(idx)

    if (pure_java_content != 'r.copy(String.join("+", '):
        print("The end")
        print(pure_java_content)
        #The parentheses closes the String.join and r.copy methods
        copy_to_file = tabs + leading_spaces + pure_java_content.rstrip() + '));\n'
        out.write(copy_to_file)

    #Keys closing main (or udf) and the class
    out.write("\n\t}\n}")
