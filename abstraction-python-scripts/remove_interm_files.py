#!/usr/bin/python3

import sys
import os

def remove_file(filename):
    try:
        os.remove(filename)
    except OSError as e:
        #print("Could not delete " + filename)
        pass

dirpath = os.path.abspath(os.path.dirname(sys.argv[0])) + "/"

###########################
#Clean temporary Apex files
import_file = dirpath + "apex/out/imports.java"
content_file = dirpath + "apex/out/content.java"
udf_file = dirpath + "apex/out/udf.java"
properties_file = dirpath + "properties-files/apex/out/properties.xml"

remove_file(import_file)
remove_file(content_file)
remove_file(udf_file)
remove_file(properties_file)

###########################
#Clean temporary Flink files
import_file = dirpath + "flink/out/imports.java"
content_file = dirpath + "flink/out/content.java"
udf_file = dirpath + "flink/out/udf.java"

remove_file(import_file)
remove_file(content_file)
remove_file(udf_file)
