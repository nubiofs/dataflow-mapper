import sys
import argparse

#PREVIOUS APPROACH FOR GENERATING POM FILE

#This file is intended to generate pom.xml for flink project,
#according to necessary dependencies for each application

def parseArgs():
    parser = argparse.ArgumentParser(description="Generate pom file for Flink project");

    # Mandatory arguments
    parser.add_argument("projectName", help="Project name", type=str)
    
    # Optional arguments
    parser.add_argument("-k", "--kafka", help="kafka", type=bool, default=False)
    parser.add_argument("-r", "--rabbitMQ", help="rabbitMQ", type=bool, default=False)
    
    return parser.parse_args()

def main(args):

    with open("flink/beggining.txt", "r") as f:
        beggining = f.read()

    with open("flink/dependencies.txt", "r") as f:
        dependencies = f.read()

    with open("flink/plugins.txt", "r") as f:
        plugins = f.read()

    with open("flink/profile.txt", "r") as f:
        profile = f.read()

    #Remove last line of string dependencies 
    dependencies = dependencies.rsplit("\n", 2)[0]

    if (args.kafka):
        with open("flink/kafka11-dep.txt") as f:
            dependencies += "\n" + f.read()
    # if(args.rabbitMQ):
    #     with open("flink/rabbit-dep.txt") as f:
    #         dependencies += "\n" + f.read()
    dependencies +="\n\t</dependencies>\n"


    arq = beggining + dependencies + plugins + profile
    arq = arq.replace("$name", args.projectName)
    print(arq)



if __name__ == "__main__":
    args = parseArgs()
    main(args)