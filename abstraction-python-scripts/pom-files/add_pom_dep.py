#!/usr/bin/python3

import sys
import os
from lxml import etree
#cElementTree is a more efficient implementation
from xml.etree import cElementTree as ET

dirpath = os.path.abspath(os.path.dirname(sys.argv[0])) + "/"
pom_ns = dict(pom='http://maven.apache.org/POM/4.0.0')
#sys.path.insert(0, os.path.abspath(os.path.dirname(sys.argv[0]) + "/"))

def add_dependency(path_to_pom, fname, dep_type):
    print("Inside add_dependency")
    dependency = ""
    print(dirpath + fname)
    try:
        fname = dirpath + fname
        path_to_pom = dirpath + path_to_pom

        with open(fname) as f:
            dependency += f.read()
        dependency = "<data>\n" + dependency + "\n</data>"
        dep = etree.fromstring(dependency)
        dep.tail = "\n\t"

        #parser = etree.XMLParser(remove_blank_text=True)
        tree = etree.parse(path_to_pom + "pom.xml")

        ET.register_namespace('',pom_ns.get('pom'))

        if (dep_type == "hdfs"):
            dep_type = "hadoop"
        #Checks if the dependency already exists
        repeat = find_repeated('pom:dependencies/pom:dependency', tree, dep, 'dependency')
        if (not repeat):
            for item in dep.findall('dependency'):
                item.tail = "\n\t"
                tree.find('pom:dependencies', pom_ns).append(item)

        if (dep.find('plugin') != None or dep.find('profile') != None):
            repeat = find_repeated('pom:build/pom:plugins/pom:plugin', tree, dep, 'plugin')

            if (not repeat):
                for item in dep.findall('plugin'):
                    item.tail = "\n\t"
                    #print(etree.tostring(tree.find('pom:build/plugins', pom_ns)))
                    if (tree.find('pom:build/pom:plugins', pom_ns) != None):
                        tree.find('pom:build/pom:plugins', pom_ns).append(item)

            repeat = find_repeated('pom:profiles/pom:profile/pom:dependencies/pom:dependency', tree, dep, 'profile/dependencies/dependency')
            if (not repeat):
                for item in dep.findall('profile/dependencies/dependency'):
                    item.tail = "\n\t"
                    if (tree.find('pom:profiles/pom:profile/pom:dependencies', pom_ns) != None):
                        tree.find('pom:profiles/pom:profile/pom:dependencies', pom_ns).append(item)

        print(path_to_pom + "pom.xml")
        tree.write(path_to_pom + "pom.xml", pretty_print=True)
    except Exception as e:
        print('Failed to open file: '+ str(e))


def find_repeated(structure_path, tree, dep, path_name):
    elements = [item.text for item in dep.findall("./" + path_name + "/artifactId")]

    for dependency_element in tree.findall(structure_path,pom_ns):  
        checkartifactid = dependency_element.find('pom:artifactId',pom_ns).text  
        #if (dep_type in checkartifactid):
        if (checkartifactid in elements):
            return True
    return False

def usage():
    print("Usage: python3 PomAddDep.py ARG1 ARG2 ARG3")
    print("ARG1: package name")
    print("ARG2: tool")
    print("ARG3: dependency type (kafka, rabbitmq, etc)")


def main():

    if (len(sys.argv) < 4):
        usage()
        return
    package_name = sys.argv[1]
    tool = sys.argv[2].lower()
    dep_type = sys.argv[3]
    fname = ""

    print("Inside PomAddDep")
    print("Tool is ", tool)

    apex = {"kafka": "pom-apex/kafka08-dep.txt", "rabbitmq" : "pom-apex/rabbitmq-dep.txt",
            "apexStream" : "pom-apex/apexstream-dep.txt", "jdbc" : "pom-apex/jdbc-dep.txt",
            "hdfs": None}
    flink = {"kafka": "pom-flink/kafka11-dep.txt", "rabbitmq" : None,
            "apexStream": None, "jdbc" : "pom-flink/jdbc-dep.txt",
            "hdfs": "pom-flink/hdfs-dep.txt"}


    if (tool == "flink"):
        fname = flink[dep_type]
    elif (tool == "apex"):
        fname = apex[dep_type]

    if (tool == "flink" or tool == "apex"):
        path = "../../generated-code/" + tool + "/" + package_name + "/"
        if (fname != None):
            add_dependency(path, fname, dep_type)
    elif (tool == "both"):
        #Run for Flink
        path = "../../generated-code/flink/" + package_name + "/"
        if (flink[dep_type] != None):
            add_dependency(path, flink[dep_type], dep_type)
        #Run for Apex
        path = "../../generated-code/apex/" + package_name + "/"
        if (apex[dep_type] != None):
            add_dependency(path, apex[dep_type], dep_type)
    else:
        print("Invalid tool chosen")
        sys.exit(1)


if __name__ == "__main__":
    main()

