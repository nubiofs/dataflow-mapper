import sys
import argparse

#PREVIOUS APPROACH FOR GENERATING POM FILE

#This file is intended to generate pom.xml for apex project,
#according to necessary dependencies for each application
#HOWEVER, this generates the pom from the zero and it makes sense
#in the previous approach. 
# Currently, since mvn already generates pom.xml, it is only necessary
# to add the dependency instead of creating the whole file.
#That is why I'm using PomAddDep.py right now.

def parseArgs():
    parser = argparse.ArgumentParser(description="Generate pom file for Apex project");

    # Mandatory arguments
    parser.add_argument("projectName", help="Project name", type=str)
    
    # Optional arguments
    parser.add_argument("-k", "--kafka", help="Uses Kafka?", type=bool, default=False)
    parser.add_argument("-r", "--rabbitMQ", help="Uses rabbitMQ?", type=bool, default=False)
    
    return parser.parse_args()


def main(args):

    with open("apex/beggining.txt", "r") as f:
        beggining = f.read()

    with open("apex/properties.txt", "r") as f:
        properties = f.read()

    with open("apex/plugins.txt", "r") as f:
        plugins = f.read()

    with open("apex/dependencies.txt", "r") as f:
        dependencies = f.read()


    #Remove last line of string dependencies 
    dependencies = dependencies.rsplit("\n", 2)[0]

    if (args.kafka):
        with open("apex/kafka08-dep.txt") as f:
            dependencies += "\n" + f.read()
    # if(args.rabbitMQ):
    #     with open("flink/rabbit-dep.txt") as f:
    #         dependencies += "\n" + f.read()
    dependencies +="\n\t</dependencies>\n</project>"

    arq = beggining + properties + plugins + dependencies
    arq = arq.replace("$name", args.projectName)
    print(arq)


if __name__ == "__main__":
    args = parseArgs()
    main(args)