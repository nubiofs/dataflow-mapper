#If appears a warning about malformed project, just add a version to
#javadoc, for example <version>2.9</version> 
if [ "$#" -ne 1 ]; then
   echo "USAGE: ./generate_flink_project.sh ARG1"
   echo "ARG1: name of the package"
   exit 1;
fi

name=$1

#Change to previous directory (i.e. tools_abstraction_layer)
cd ../generated-code/flink/

mvn archetype:generate \
-DarchetypeGroupId=org.apache.flink \
-DarchetypeArtifactId=flink-quickstart-java -DarchetypeVersion=1.5.3 \
-DgroupId=com.nanda -Dpackage=com.nanda.$name -DartifactId=$name \
-Dversion=1.0-SNAPSHOT \
-DinteractiveMode=false

if [[ "$?" -ne 0 ]] ; then
  echo 'Could not generate the project. Give another name.';
  exit 1;
fi
