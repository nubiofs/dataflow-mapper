#!/bin/bash

wget https://repo.maven.apache.org/maven2/org/apache/maven/apache-maven/3.2.5/apache-maven-3.2.5-bin.tar.gz

sudo tar -xvzf ~/Downloads/apache-maven-3.2.5-bin.tar.gz
sudo mv ~/Downloads/apache-maven-3.2.5-bin.tar.gz /opt/

echo 'export M2_HOME="/opt/apache-maven-3.2.5"' >> ~/.bashrc
echo "export PATH=$PATH:$M2_HOME/bin" >> ~/.bashrc

#To check maven version
mvn -v

#It is possible to use different maven alternatives:
#$alternatives --install /usr/bin/mvn mvn ${MAVEN_HOME_3}/bin/mvn 120
#$alternatives --config mvn
#$ <choose the desire alternative>
