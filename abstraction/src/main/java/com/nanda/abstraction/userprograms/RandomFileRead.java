package com.nanda.abstraction.userprograms;

import com.nanda.abstraction.general.*;
import com.nanda.abstraction.general.interfaces.MapFunction;

import java.util.Random;

public class RandomFileRead {

    static int result = 0;
    public static void main(String[] args) {
        //Código que utiliza a API
        ProjectSettings project = new ProjectSettings("Both");
        project.setPackageName("experiment");
        project.start("batch");

        //Código abaixo é composto apenas por Java Nativo
        Random r = new Random();
        int low = 10;
        int high = 100;
        result = r.nextInt(high - low) + low;

        if (result < 50)
            System.out.println("Valor menor que 50");
        else
            System.out.println("Valor maior ou igual a 50");

        //Código que mistura a interface com Java nativo
        FileIO<Integer> f = new FileIO<>(FileIO.FileFormat.Text);

        Data<Integer> d = f.read(TP.build("localhost:8020/input/"),TP.build("values.txt"));
        d.map(new CompareValue()).print();

        System.out.println("Encerrando a aplicação!\n");

        project.finish();

    }

    public static class CompareValue implements MapFunction<Integer, String> {
        @Override
        public String map(Integer value) {
            if (value < result)
                return "Valor menor que resultado";
            return "Valor maior ou igual ao resultado";
        }
    }
}