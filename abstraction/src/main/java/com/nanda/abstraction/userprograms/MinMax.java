package com.nanda.abstraction.userprograms;

import com.nanda.abstraction.general.*;
import com.nanda.abstraction.general.interfaces.FlatMapFunction;
import com.nanda.abstraction.general.interfaces.MapFunction;

import java.util.ArrayList;

public class MinMax {

    public static void main(String[] args) {
        ProjectSettings project = new ProjectSettings("Both");
        project.setPackageName("novinho2");
        project.start("stream");

        FileIO<String> f = new FileIO<>(FileIO.FileFormat.Hdfs);
        Data<String> d = f.read("localhost:8020/tmp/201904/prev/","201904_flink.txt");
        //Data<String> d = f.read("/tmp/","teste.txt");

        Data<KV<String, Double>> d2 = d.map(new Example());

        d2.minByKey().print();
        d2.maxByKey().print();

        //f.write(TP.build("localhost:8020/tmp/output/201904/prev/"),TP.build("min_flink.txt"), TP.build(52428800), d2);
        f.write("localhost:8020/tmp/output/201904/prev/", "min_flink.txt", 52428800, d2);


        project.finish();

    }

    public static class Example implements MapFunction<String, KV<String, Double>> {
        @Override
        public KV<String, Double> map(String value) {
            String[] items = value.split(",");
            return new KV<>(items[0], tryParseTime(items[1].trim()));
        }
    }

    private static Double tryParseTime(String inputItem) {
        try {
            return Double.parseDouble(inputItem);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    private static class PrettyPrint implements MapFunction<String, String> {
        @Override
        public String map(String input) {
            return input.split(",")[0] + "-----sum: "
                    + input.split(",")[1];
        }
    }

}
