package com.nanda.abstraction.userprograms;

import com.nanda.abstraction.general.Data;
import com.nanda.abstraction.general.FileIO;
import com.nanda.abstraction.general.KV;
import com.nanda.abstraction.general.ProjectSettings;
import com.nanda.abstraction.general.interfaces.MapFunction;

public class FileMinMaxFile {

    public static void main(String[] args) throws Exception{
        ProjectSettings project = new ProjectSettings("Both");
        //project.create("experiment", args);
        project.setPackageName("experiment");
        project.start("batch");

        FileIO<String> f = new FileIO<>(FileIO.FileFormat.Text);
        Data<String> d = f.read("/home/nanda/Documents/results/201904/prev/","201904_apex.txt");

        Data<KV<String, Double>> d2 = d.map(new Example());
        /*Data<KV<String, Double>> d3 = new Data<>();
        d3 = d2;*/
        FileIO<KV<String, Double>> f2 = new FileIO<>(FileIO.FileFormat.Hdfs);

        d2.minByKey();
        f2.write("/home/nanda/Documents/results/output/201904/prev/", "min_apex.txt", 52428800, d2);

        d2.maxByKey();
        f2.write("/home/nanda/Documents/results/output/201904/prev/", "max_apex.txt", 52428800, d2);


        project.finish();

    }

    public static class Example implements MapFunction<String, KV<String, Double>> {
        @Override
        public KV<String, Double> map(String value) {
            String[] items = value.split(",");
            return new KV<>(items[0], tryParseTime(items[1].trim()));
        }
    }

    private static Double tryParseTime(String inputItem) {
        try {
            return Double.parseDouble(inputItem);
        } catch (NumberFormatException e) {
            return null;
        }
    }

}
