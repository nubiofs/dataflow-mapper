package com.nanda.abstraction.userprograms;

import com.nanda.abstraction.general.*;

public class KafkaMinConsole {

    public static void main(String[] args) {
        ProjectSettings project = new ProjectSettings("Both");
        project.setPackageName("experiment");
        project.start("stream");

        Kafka<String> k = new Kafka<>();

        Data<String> d = k.read(TP.build("group"), TP.build("localhost:9092"),
                TP.build("example"), TP.build("localhost:2181"), TP.build(false));

        Window w = new Window(true);

        /*d= d.minByKey()
            .timeWindow(w, Time.seconds(10))
            .print();*/


        project.finish();

    }
}
