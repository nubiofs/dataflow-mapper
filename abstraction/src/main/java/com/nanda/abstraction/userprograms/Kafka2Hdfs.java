package com.nanda.abstraction.userprograms;

import com.nanda.abstraction.general.*;

public class Kafka2Hdfs {

    public static void main(String[] args) {
        ProjectSettings project = new ProjectSettings("Both");
        project.setPackageName("experiment");
        project.start("stream");

        Kafka<String> k = new Kafka<>();

        Data<String> d = k.read(TP.build("group"), TP.build("localhost:9092"),
                TP.build("example"), TP.build("localhost:2181"), TP.build(false));

        FileIO<String> f = new FileIO<>(FileIO.FileFormat.Hdfs);
        f.write(TP.build("localhost:8020/tmp/"), TP.build("sample.txt"), TP.build(1024), d);

        project.finish();

    }
}
