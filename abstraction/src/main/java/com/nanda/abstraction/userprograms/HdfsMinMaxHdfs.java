package com.nanda.abstraction.userprograms;

import com.nanda.abstraction.general.Data;
import com.nanda.abstraction.general.FileIO;
import com.nanda.abstraction.general.KV;
import com.nanda.abstraction.general.ProjectSettings;
import com.nanda.abstraction.general.interfaces.MapFunction;

public class HdfsMinMaxHdfs {

    public static void main(String[] args) {
        ProjectSettings project = new ProjectSettings("Both");
        project.setPackageName("experiment");
        project.start("batch");

        FileIO<String> f = new FileIO<>(FileIO.FileFormat.Hdfs);
        Data<String> d = f.read("localhost:8020/tmp/201904/prev/","simplified_201904_apex.txt");

        Data<KV<String, Double>> d2 = d.map(new Example());
        FileIO<KV<String, Double>> f2 = new FileIO<>(FileIO.FileFormat.Hdfs);

        d2.minByKey();
        f2.write("localhost:8020/tmp/output/201904/prev/", "simplified_min_apex.txt", 52428800, d2);

        d2.maxByKey();
        f2.write("localhost:8020/tmp/output/201904/prev/", "simplified_max_apex.txt", 52428800, d2);

        project.finish();

    }

    public static class Example implements MapFunction<String, KV<String, Double>> {
        @Override
        public KV<String, Double> map(String value) {
            String[] items = value.split(",");
            return new KV<>(items[0], tryParseTime(items[1].trim()));
        }
    }

    private static Double tryParseTime(String inputItem) {
        try {
            return Double.parseDouble(inputItem);
        } catch (NumberFormatException e) {
            return null;
        }
    }

}
