package com.nanda.abstraction.userprograms;

import com.nanda.abstraction.general.*;
import com.nanda.abstraction.general.interfaces.FilterFunction;


public class Hdfs2Kafka {

    public static void main(String[] args) {
        ProjectSettings project = new ProjectSettings("Both");
        System.out.println("Inside Dataflow");
        project.setPackageName("experiment");
        project.start("stream");

        FileIO<String> f = new FileIO<>(FileIO.FileFormat.Hdfs);
        Data<String> d = f.read("localhost:8020/tmp/","sample.txt");

        d.filter(new Splitter());

        Kafka<String> k = new Kafka<>();
        k.write(TP.build("localhost:9092"), TP.build("example"), d);


        project.finish();

    }

    public static class Splitter implements FilterFunction<String> {

        @Override
        public boolean filter(String values) {
            String[] items = values.split(",");
            for (String i : items)
                System.out.println("===>" + i);
            return true;
        }
    }
}
