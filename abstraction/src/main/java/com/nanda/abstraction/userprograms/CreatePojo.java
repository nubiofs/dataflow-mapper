package com.nanda.abstraction.userprograms;

import com.nanda.abstraction.general.TypeHelper;
import java.util.LinkedHashMap;

public class CreatePojo {

    public static void main(String[] args) {

        TypeHelper th = new TypeHelper();
        String tool = "Both";
        String pack = "experiment2";

        /*Create BusPosPojo*/
        LinkedHashMap<String, Object> busPos = new LinkedHashMap<String, Object>()
        {{
            put("busLineId", String.class);
            put("busId", String.class);
            put("lineId", String.class);
            put("lastLat", String.class);
            put("lastLon", String.class);
            put("lastPosTimestamp", String.class);
            put("lastStop", String.class);
            put("lastStopTimestamp", String.class);
        }};

        th.createUserPojo(busPos, tool, pack, "BusPosPojo");
        /*Create StopPojo*/
        LinkedHashMap<String, Object> stop = new LinkedHashMap<String, Object>()
        {{
            put("stopId", String.class);
            put("stopName", String.class);
            put("lat", Double.class);
            put("lon", Double.class);
            put("lineId", String.class);
            put("nextStop", String.class);
        }};

        th.createUserPojo(stop, tool, pack, "StopPojo");

        /*Create BusPosLastStopPojo*/
        LinkedHashMap<String, Object> busPosLast = new LinkedHashMap<String, Object>()
        {{
            put("busId", String.class);
            put("lineId", String.class);
            put("lastStopId", String.class);
            put("lastStopTimestamp", String.class);
        }};
        th.createUserPojo(busPosLast, tool, pack, "BusPosLastStopPojo");


        /*Create TimeTablePojo*/
        LinkedHashMap<String, Object> timeTable = new LinkedHashMap<String, Object>()
        {{
            put("stopId", String.class);
            put("lineId", String.class);
            put("maxTime", Long.class);
            put("minTime", Long.class);
            put("sum", Long.class);
            put("cont", Integer.class);
            put("avg", Long.class);
        }};

        th.createUserPojo(timeTable, tool, pack, "TimeTablePojo");
    }
}
