package com.nanda.abstraction.general;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;


public class TypeHelper {

    /*Initialize Type*/
    public String getSQLTypes(HashMap<String, Object> tuplesAndTypes) {
        Object[] values = tuplesAndTypes.values().toArray();
        String[] valuesConverted = new String[values.length];

        Integer i = 0;
        for (Object value : values) {
            if (value.equals(BigInteger.class))
                valuesConverted[i] = "Types.BIGINT";
            else if (value.equals(Boolean.class) || value.equals(boolean.class))
                valuesConverted[i] = "Types.BOOLEAN";
            else if (value.equals(Byte.class) || value.equals(byte.class))
                valuesConverted[i] = "Types.DECIMAL";
            else if (value.equals(Character.class) || value.equals(char.class))
                valuesConverted[i] = "Types.CHAR";
            else if (value.equals(Date.class))
                valuesConverted[i] = "Types.DATE";
            else if (value.equals(Double.class) || value.equals(double.class))
                valuesConverted[i] = "Types.DOUBLE";
            else if (value.equals(Float.class) || value.equals(float.class))
                valuesConverted[i] = "Types.FLOAT";
            else if (value.equals(Integer.class) || value.equals(int.class))
                valuesConverted[i] = "Types.INTEGER";
            else if (value.equals(Long.class) || value.equals(long.class))
                valuesConverted[i] = "Types.BIGINT";
            else if (value.equals(Short.class) || value.equals(short.class))
                valuesConverted[i] = "Types.SMALLINT";
            else if (value.equals(String.class))
                valuesConverted[i] = "Types.VARCHAR";
            else
                valuesConverted[i] = "Types.VARCHAR";
            i++;
        }

        String result = Arrays.toString(valuesConverted);
        result = result.substring(1, result.length() - 1);
        return result;
    }

    public String getBasicTypes(HashMap<String, Object> tuplesAndTypes) {
        Object[] values = tuplesAndTypes.values().toArray();
        String[] valuesConverted = new String[values.length];

        Integer i = 0;
        for (Object value : values) {
            if (value.equals(BigInteger.class))
                valuesConverted[i] = "BasicTypeInfo.BIG_INT_TYPE_INFO";
            else if (value.equals(Character.class))
                valuesConverted[i] = "BasicTypeInfo.CHAR_TYPE_INFO";
            else if (value.equals(Integer.class))
                valuesConverted[i] = "BasicTypeInfo.INT_TYPE_INFO";
            else if (value.equals(BigDecimal.class))
                valuesConverted[i] = "BasicTypeInfo.BIG_DEC_TYPE_INFO";
            else
                valuesConverted[i] = String.format("BasicTypeInfo.%s_TYPE_INFO", value.toString().toUpperCase().split("CLASS JAVA.LANG.")[1]);

            i++;
        }

        String result = Arrays.toString(valuesConverted);
        result = result.substring(1, result.length() - 1);
        return result;
    }

    public void createPojo(HashMap<String, Object> tuplesAndTypes, String endPath, String pack, String className) {
        BufferedWriter writer;

        try {
            writer = new BufferedWriter(new FileWriter(endPath, false));
            writer.write("package " + pack + ";\n\n");

            writer = new BufferedWriter(new FileWriter(endPath, true));
            writer.write("public class " + className + " {\n\n");

            String key;
            Object value;
            for (Map.Entry<String, Object> item : tuplesAndTypes.entrySet()) {
                key = item.getKey().toLowerCase();
                value = item.getValue().toString().split("class java.lang.")[1];
                writer.write("\tprivate " + value + " " + key + ";\n");
            }

            writer.write("\n");

            for (Map.Entry<String, Object> item : tuplesAndTypes.entrySet()) {
                key = item.getKey().toLowerCase();
                value = item.getValue().toString().split("class java.lang.")[1];

                String keyCap = key.substring(0, 1).toUpperCase() + key.substring(1);
                writer.write(String.format("\tpublic %s get%s() { return %s; }\n", value, keyCap, key));

                writer.write(String.format("\tpublic void set%s(%s %s) { this.%s = %s; }\n\n"
                        , keyCap, value, key, key, key));
            }

            writer.write("\n}");

            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    public void createUserPojo(LinkedHashMap<String, Object> tuplesAndTypes, String tool, String pack, String className) {

        String apexPath = "", flinkPath = "", pojoPath = "";
        tool = tool.toLowerCase();

        if (tool.equals("apex") || tool.equals("both")) {
            apexPath = String.format("../generated-code/apex/%s/src/main/java/com/nanda/%s/%s.java", pack, pack, className);
            writeToFile(tuplesAndTypes, apexPath, pack, className);
        }
        if (tool.equals("flink") || tool.equals("both")) {
            flinkPath = String.format("../generated-code/flink/%s/src/main/java/com/nanda/%s/%s.java", pack, pack, className);
            writeToFile(tuplesAndTypes, flinkPath, pack, className);
        }

        pojoPath = String.format("../abstraction/src/main/java/com/nanda/abstraction/pojo/%s.java", className);
        writeToFile(tuplesAndTypes, pojoPath, "abstraction.pojo", className);


    }

    private void writeToFile(LinkedHashMap<String, Object> tuplesAndTypes, String endPath, String pack, String className) {
        BufferedWriter writer;

        try {
            writer = new BufferedWriter(new FileWriter(endPath, false));
            writer.write("package " + pack + ";\n\n");

            writer = new BufferedWriter(new FileWriter(endPath, true));
            writer.write("package com.nanda." + pack + ";\n\n");
            writer.write("public class " + className + " {\n\n");

            String key, all_values_keys = "", all_values = "";
            Object value;
            HashMap<String, Object> treatedValues = new HashMap<String, Object>();

            /* ****************
            Declare variables
            *******************/
            for (Map.Entry<String, Object> item : tuplesAndTypes.entrySet()) {
                key = item.getKey();
                value = item.getValue().toString().split("class java.lang.")[1];
                treatedValues.put(key, value);
                writer.write("\tprivate " + value + " " + key + ";\n");
                all_values_keys += value + " " + key + ", ";
                all_values += key + " + \",\" + ";
            }

            if (all_values_keys.length() > 2)
                all_values_keys = all_values_keys.substring(0, all_values_keys.length() - 2);

            if (all_values.length() > 9)
                all_values = all_values.substring(0, all_values.length() - 9) + ";";

            /* ****************
            Create constructors*
            ********************/
            writer.write("\n\tpublic " + className + "() {};\n\n");

            writer.write(String.format("\n\tpublic %s(%s) {\n\n", className, all_values_keys));
            for (Map.Entry<String, Object> item : treatedValues.entrySet()) {
                key = item.getKey();
                writer.write(String.format("\t\tthis.%s = %s;\n", key, key));
            }
            writer.write("\t}\n");


            /* ************************
            Create getters and setters*
            ***************************/
            for (Map.Entry<String, Object> item : treatedValues.entrySet()) {
                key = item.getKey();
                String keyCap = key.substring(0, 1).toUpperCase() + key.substring(1);
                writer.write(String.format("\tpublic %s get%s() { return %s; }\n", item.getValue(), keyCap, key));
                writer.write(String.format("\tpublic void set%s(%s %s) { this.%s = %s; }\n\n"
                        , keyCap, item.getValue(), key, key, key));
            }

            /* ************************
            Create method toString()*
            ***************************/
            writer.write("\tpublic String toString() {\n");
            writer.write("\t\treturn " + all_values + "\n\t}\n");

            writer.write("\n}");

            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
