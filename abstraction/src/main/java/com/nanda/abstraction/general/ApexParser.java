package com.nanda.abstraction.general;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.visitor.ModifierVisitor;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import com.github.javaparser.symbolsolver.javaparsermodel.JavaParserFacade;
import com.github.javaparser.symbolsolver.model.resolution.TypeSolver;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ApexParser {

    private static final Pattern API_PACKAGE = Pattern.compile("com.nanda.abstraction.general");
    private static final Pattern PROJ_SETTINGS = Pattern.compile("ProjectSettings");
    private static final Pattern TIME_CLASS = Pattern.compile("Time");
    private static final Pattern DATA_CLASS = Pattern.compile("Data");
    private static final Pattern IO_DATA_DTRANS = Pattern.compile("IO|Data|DataTransformation");

    private TreeMap<Integer, String> apiStatements = new TreeMap<>();
    private TreeMap<Integer, String> fileContentA = new TreeMap<>();
    private static List<String> importsApex;
    private String fileName;
    private String streamOrBatch = "Stream"; //default

    private String mapper = "Both"; //default
    private static List<String> toApex = Arrays.asList("apex", "Apex", "both", "Both");
    private static List<String> udfs = Arrays.asList("map", "flatMap", "reduce", "filter", "folder", "print");

    Auxiliar aux = new Auxiliar();


    ApexParser(TreeMap<Integer, String> apiStatements, TreeMap<Integer, String> fileContentA,
               List<String> importsApex, String fileName, String streamOrBatch) {
        this.apiStatements = apiStatements;
        this.fileContentA = fileContentA;
        this.importsApex = importsApex;
        this.fileName = fileName;
        this.streamOrBatch = streamOrBatch;
    }


    protected void doMapping(CompilationUnit comp, TypeSolver typeSolver) {
        /*comp.accept(new VarDeclVisitor(), JavaParserFacade.get(typeSolver));
        comp.accept(new MethodCallVisitor(), JavaParserFacade.get(typeSolver));*/
        comp.accept(new MethodChangerVisitor(), null);
        comp.accept(new ClassChangerVisitor(), null);

        writeParsedA();
    }

    public static String mapApexTypes(String currentParam, String toReplace) {

        if (currentParam.contains("KV")) {
            toReplace = toReplace.replace("KV", KV.apexKV());
            if (importsApex != null && !importsApex.contains("import com.datatorrent.lib.util.KeyValPair;"))
                importsApex.add("import com.datatorrent.lib.util.KeyValPair;");
        }

        if (currentParam.contains("Data"))
            toReplace = toReplace.replace("Data", Data.apexData());
        /*else if (currentParam.contains("FileIn"))
            toReplace = toReplace.replace("FileIn", FileIn.apexFileInput());
        else if (currentParam.contains("FileOut"))
            toReplace = toReplace.replace("FileOut", FileOut.apexFileOutput());*/
        else if (currentParam.contains("KafkaIn"))
            toReplace = toReplace.replace("KafkaIn", KafkaIn.apexKafkaInput());
        else if (currentParam.contains("KafkaOut"))
            toReplace = toReplace.replace("KafkaOut", KafkaOut.apexKafkaOutput());

        return toReplace;
    }

    /*** Here can be done the logic for mapping UDFs signature*/
    private class ClassChangerVisitor extends VoidVisitorAdapter<Void> {
        @Override
        public void visit(ClassOrInterfaceDeclaration n, Void arg) {
            super.visit(n, arg);
            boolean found = false;
            boolean ignore = false;
            int beginLine = n.getBegin().get().line;
            int endLine = n.getEnd().get().line;

            String func = n.getImplementedTypes().toString();
            String funcA = func;
            String before = "";

            if (! func.isEmpty()) {
                if (func.contains("MapFunction")) {
                    before = "MapFunction";
                    funcA = "Function.MapFunction";
                    importsApex.add("import org.apache.apex.malhar.lib.function.Function;");
                }
                else if (func.contains("FlatMapFunction")) {
                    before = "FlatMapFunction";
                    funcA = "Function.FlatMapFunction";
                    importsApex.add("import org.apache.apex.malhar.lib.function.Function;");
                }
                else if (func.contains("ReduceFunction")) {
                    //importsApex.add("import org.apache.apex.malhar.lib.window.accumulation;");


                    String type = func.substring(func.indexOf("<") + 1, func.lastIndexOf(">"));
                    type = mapApexTypes(type, type);
                    String keyType = type.substring(type.indexOf("<") + 1, type.lastIndexOf(",")).trim();
                    String valueType = type.substring(type.indexOf(",") + 1, type.lastIndexOf(">")).trim();

                    String fileWriteContent = String
                            .format("\tpublic static class ToKeyVal%s implements Function.ToKeyValue<%s, %s, %s> {",
                            aux.getReduceKeyValSuffix(), type, keyType, valueType);
                    aux.incReduceKeyValSuffix();

                    fileWriteContent += "\n\t\t@Override\n\t\t";
                    fileWriteContent += String.format("public Tuple<%s> f(%s input) {", type, type);
                    fileWriteContent += String.format("\n\t\t\treturn new Tuple.PlainTuple<%s>(input);\n", type);
                    fileWriteContent += "\t\t}\n\t}\n";

                    String endPath = "../abstraction-python-scripts/apex/out/";
                    aux.writeGeneral(endPath, "udf.java", fileWriteContent);


                    String contentA = "", part = "", sequence = "";
                    String seqIn = "", seqOut = "";
                    //Boolean newVarDecl = false;
                    Integer indexSize = 0;
                    for (int i = beginLine; i <= endLine; i++) {
                        /*While loop is not needed because we only want to get the
                        value and it does not need to be recursively*/
                        //while (contentA.contains("KV")) {
                        contentA = fileContentA.get(i);
                        if (contentA.contains("KV") | contentA.contains("KeyValPair")) {
                            if (contentA.contains("KV")) {
                                sequence = "KV";
                                indexSize = 3; //KV + < or (
                            } else {
                                sequence = "KeyValPair";
                                indexSize = 11;
                            }

                            if (contentA.contains("KV<>(") || contentA.contains("KeyValPair<>(")) {
                                sequence += "<>(";
                                seqOut = ")";
                                indexSize += 2; //inclusion of >(
                                //newVarDecl = true;
                                contentA = contentA.replace("new ","");
                            }
                            else if (contentA.contains("KV<") || contentA.contains("KeyValPair<")) {
                                sequence += "<";
                                seqOut = ">";
                            }

                            before = contentA
                                    .substring(contentA.indexOf(sequence) + indexSize,
                                            contentA.indexOf(seqOut));
                            if (before.contains(","))
                                part = before.split(",")[1].trim();

                            /*if (newVarDecl)
                                part = "(" + part + ")";*/

                            before = sequence + before + seqOut;
                            contentA = contentA.replace(before, part);
                            fileContentA.put(i, contentA);
                        }
                    }

                    before = "implements ReduceFunction";
                    funcA = "extends ReduceFn";
                    importsApex.add("import org.apache.apex.malhar.lib.window.accumulation.ReduceFn;");
                    importsApex.add("import org.apache.apex.malhar.lib.window.Tuple;");
                }
                else if (func.contains("FilterFunction")) {
                    before = "FilterFunction";
                    funcA = "Function.FilterFunction";
                    importsApex.add("import org.apache.apex.malhar.lib.function.Function;");
                }
                else if (func.contains("FoldFunction")) {
                    before = "FoldFunction";
                    funcA = "Function.FoldFunction";
                    //Window accumulation (stream)
                    importsApex.add("import org.apache.apex.malhar.lib.window.accumulation;");
                }
                else
                    ignore = true;

                if (!ignore) {
                    //comp.addImport("import org.apache.apex.malhar.lib.function.Function;");
                    for (int i = beginLine; i <= endLine && !found; i++) {
                        String contentA = fileContentA.get(i);
                        if (contentA.contains(before)) {
                            contentA = contentA.replace(before, funcA);

                            /*Mapping parameters type for Apex. Split is done to separate each type
                             * present implements interface. For ex: MapFunction<String, Integer>*/
                            String[] parameters = func.split(",");
                            for (String param : parameters)
                                contentA = mapApexTypes(param, contentA);

                            fileContentA.put(beginLine, contentA);
                            found = true;
                        }
                    }
                }
            }
        }
    }

    private class MethodChangerVisitor extends ModifierVisitor<JavaParserFacade> {
        @Override
        public Node visit(MethodDeclaration n, JavaParserFacade arg) {
            super.visit(n, arg);
            System.out.println(n.getDeclarationAsString());
            int beginLine = n.getBegin().get().line;
            int endLine = n.getEnd().get().line;
            boolean found = false;
            String searchedMethod = n.getName().toString();

            /*Get the parameters together with its types*/
            NodeList parameters = n.getParameters();

            int line = 0;
            /*If mapper is Apex or both*/
            for (line = beginLine; line <= endLine && !found; line++)
                if (fileContentA.get(line).contains(searchedMethod))
                    found = true;
            line--;

            String toReplace = fileContentA.get(line);
            switch(searchedMethod) {
                case("map"):
                    toReplace = fileContentA.get(line).replace("map", "f");
                    break;
                case("flatMap"):
                    toReplace = fileContentA.get(line).replace("flatMap", "f");
                    break;
                case("filter"):
                    toReplace = fileContentA.get(line).replace("filter", "f");
                    break;
                case("main"):
                    String populate = "@Override\npublic void populateDAG(DAG dag, Configuration conf) ";
                    if (toReplace.contains("{"))
                        populate += "{";
                    toReplace = fileContentA.get(line).replace(toReplace, populate);
                    break;
            }

            /*Mapping parameters type for Apex*/
            for (int i = 0; i < parameters.size(); i++) {
                String current = parameters.get(i).toString();
                System.out.println("TO REPLACE: " + toReplace);
                toReplace = mapApexTypes(current, toReplace);
            }

            fileContentA.put(line, toReplace);

            return n;
        }
    }


    private void writeParsedA() {
        String line = "";
        BufferedWriter writer;
        try {
            /*It writes an output file for Apex*/
            String endPath = "../abstraction-python-scripts/apex/out/" + fileName + "ParsedA.java";
            writer = new BufferedWriter(new FileWriter(endPath));

            /*Package line*/
            writer.write("package com.nanda.$packageName;\n\n");

            importsApex.add("import com.datatorrent.api.StreamingApplication;");
            importsApex.add("import com.datatorrent.api.annotation.ApplicationAnnotation;");
            importsApex.add("import org.apache.hadoop.conf.Configuration;");
            importsApex.add("import com.datatorrent.api.DAG;");
            importsApex.add("import org.apache.apex.malhar.stream.api.ApexStream;");
            importsApex.add("import org.apache.apex.malhar.stream.api.impl.StreamFactory;");

            for (String item : importsApex)
                writer.write(item + "\n");

            Pattern publicClass = Pattern.compile("public\\s+class\\s+" + fileName);
            Matcher publicClassMatch;
            Pattern abstractionImport = Pattern.compile("com.nanda.abstraction.general");
            Pattern pojoImport = Pattern.compile("com.nanda.abstraction.pojo");
            Pattern populateDAG = Pattern.compile("public\\s+void\\s+populateDAG");
            Matcher abstractionMatch, pojoMatch, populateDAGMatch;
            for (int i = 2; i <= fileContentA.size(); i++) {
                line = fileContentA.get(i);
                publicClassMatch = publicClass.matcher(line);
                abstractionMatch = abstractionImport.matcher(line);
                pojoMatch = pojoImport.matcher(line);
                populateDAGMatch = populateDAG.matcher(line);
                if (publicClassMatch.find()) {
                    //line = line.replace(fileName, fileName + "ParsedA implements StreamingApplication");
                    line = line.replace(fileName, fileName + " implements StreamingApplication");
                    line = "@ApplicationAnnotation(name=\"" + fileName + "\")\n" + line;
                } else if (populateDAGMatch.find()) {
                    line += "\n\n\t\t/*$placeholderDeclarations*/\n";
                }

                if (!abstractionMatch.find() && !pojoMatch.find()) {
                    line = mapApexTypes(line, line);
                    writer.write(line + "\n");
                }

            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}