package com.nanda.abstraction.general;

/******************************************************
This is useful because shows the API user can declare
a File, Kafka or RabbitMQ and pass this to the Dataflow
without error (once these objects are also IO).*/

public class Dataflow {

    /*private IO io;
    DataTransformation dt;*/
    ProjectSettings project;

    Dataflow(ProjectSettings project) {
        this.project = project;

    }

    public static void main(String[] args) throws Exception {

        System.out.println("Inside Dataflow");
        /***The is visible to the other components (as Kafka, Map, Reduce, etc)*/
        ProjectSettings project = new ProjectSettings("MyClass2", "both");
        /*project.create("novinho2", args);*/
        project.setPackageName("novinho2");
        project.start("stream");
        Dataflow dataflow = new Dataflow(project);

        /*Kafka k = new Kafka("localhost:9092");
        Data d = k.read("group", "localhost:9092", "my-topic", "localhost:8081");
        k.write("localhost:9092", "write-topic", d);*/

        FileIO<String> f = new FileIO<>(FileIO.FileFormat.Text);
        Data<String> d = f.read("/tmp/", "newFile.java");
        //f.write("/tmp/", "output.txt", "1024", "3", d);
        //d.map("WordCountCrazy");
        //project.finish();
    }
}
