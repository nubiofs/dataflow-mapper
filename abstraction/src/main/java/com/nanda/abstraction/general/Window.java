package com.nanda.abstraction.general;

import com.nanda.abstraction.general.apex.WindowA;
import com.nanda.abstraction.general.flink.WindowF;

public class Window {

    private WindowA windowA;
    private WindowF windowF;

    private Auxiliar aux = new Auxiliar();
    protected String mp;
    protected Boolean keyed;
    protected String windowName = "";

    /*User constructor*/
    public Window(Boolean keyed) {
        this.mp = ProjectSettings.mapper.toLowerCase();
        this.keyed = keyed;
    }

    /*Constructors used for the mapping*/
    protected Window(Boolean keyed, String windowName) {
        this.keyed = keyed;
        this.windowName = windowName;
        System.out.println(String.format("Inside Window constr: %s, %s", this.keyed, this.windowName));
    }

    protected Window(Boolean keyed, String windowName, String type) {
        this.mp = ProjectSettings.mapper.toLowerCase();
        /*This part sets content for Window*/
        this.keyed = keyed;
        this.windowName = windowName;

        /*This part uses the other Window constructor to set content to WindowA and WindowF*/
        this.windowA = new WindowA(keyed, windowName);
        this.windowF = new WindowF(keyed, windowName);
        System.out.println(String.format("Inside Window constr2: %s, %s", this.keyed, this.windowName));
    }

    public void setWindowName(String windowName) { this.windowName = windowName; }

    public String getWindowName() { return this.windowName; }

    public void timeWindow(Time time) {}

    /*This method handles with tumbling time-based window*/
    protected void timeWindow(Time time, String caller) {
        if (mp.equals("flink") || mp.equals("both"))
            windowF.timeWindow(time, this.keyed, caller);
        if (mp.equals("apex") || mp.equals("both"))
            windowA.timeWindow(time, caller);
    }

    public void timeWindow(Time time, Time slide) {}

    /*It handles sliding time-based window*/
    protected void timeWindow(Time time, Time slide, String caller) {
        if (mp.equals("flink") || mp.equals("both"))
            windowF.timeWindow(time, slide, this.keyed, caller);
        if (mp.equals("apex") || mp.equals("both"))
            windowA.timeWindow(time, slide, caller);
    }

    public void sessionWindow(Time time) {}

    protected void sessionWindow(Time time, String caller) {
        System.out.println(String.format("Inside sessionWindow: %s, %s", this.keyed, caller));
        if (mp.equals("flink") || mp.equals("both"))
            windowF.sessionWindow(time, keyed, caller);
        if (mp.equals("apex") || mp.equals("both"))
            windowA.sessionWindow(time, caller);
    }

    public void countWindow(long size) { }

    /*Tumbling count-based window*/
    protected void countWindow(long size, String caller) {
        if (mp.equals("flink") || mp.equals("both"))
            windowF.countWindow(size, this.keyed, caller);
        /*if (mp.equals("apex") || mp.equals("both")) {
            //Not implemented due to Apex support problems
        }*/
    }

    public void countWindow(long size, long slide) { }

    /*Sliding count-based window*/
    protected void countWindow(long size, long slide, String caller) {
        if (mp.equals("flink") || mp.equals("both"))
            windowF.countWindow(size, slide, this.keyed, caller);
        /*if (mp.equals("apex") || mp.equals("both")) {
            //Not implemented due to Apex support problems with this type of windows
        }*/
    }


}
