package com.nanda.abstraction.general;

import com.github.javaparser.JavaParser;
import com.github.javaparser.StreamProvider;
import com.github.javaparser.ast.*;
import com.github.javaparser.ast.body.*;
import com.github.javaparser.ast.expr.*;
import com.github.javaparser.ast.visitor.ModifierVisitor;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import com.github.javaparser.printer.lexicalpreservation.LexicalPreservingPrinter;
import com.github.javaparser.symbolsolver.JavaSymbolSolver;
import com.github.javaparser.symbolsolver.javaparsermodel.JavaParserFacade;
import com.github.javaparser.symbolsolver.model.resolution.TypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.CombinedTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.JavaParserTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.ReflectionTypeSolver;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Parser {
    //private static final String FILE_PATH = "src/main/java/com/nanda/abstraction/general/Teste.java";
    private static final Pattern API_PACKAGE = Pattern.compile("com.nanda.abstraction.general");
    private static final Pattern PROJ_SETTINGS = Pattern.compile("ProjectSettings");
    private static final Pattern TIME_CLASS = Pattern.compile("Time");
    private static final Pattern DATA_CLASS = Pattern.compile("Data");
    private static final Pattern IO_DATA_DTRANS = Pattern.compile("IO|Data|DataTransformation");

    private static TreeMap<Integer, String> apiStatements = new TreeMap<>();
    private static TreeMap<Integer, String> fileContentF = new TreeMap<>();
    private static TreeMap<Integer, String> fileContentA = new TreeMap<>();
    private static String mapper = "Both"; //default
    private static String streamOrBatch = "Stream"; //default
    private static List<String> toFlink = Arrays.asList("flink", "Flink", "both", "Both");
    private static List<String> toApex = Arrays.asList("apex", "Apex", "both", "Both");
    private static List<String> udfs = Arrays.asList("map", "flatMap", "reduce", "filter", "folder", "print");

    private static List<String> importsFlink = new ArrayList<>();
    private static List<String> importsApex = new ArrayList<>();

    public static void main(String[] args) throws Exception {

        CompilationUnit comp;
        String WHOLE_FILE_PATH, FILE_PATH, FILE_NAME;

        if (args.length != 1) {
            System.out.println("Usage: java Parser.java pathTofile.java");
            System.exit(0);
        }

        WHOLE_FILE_PATH = args[0];
        System.out.println(WHOLE_FILE_PATH);
        FILE_NAME = WHOLE_FILE_PATH.replaceAll(".*/", "");
        FILE_PATH = WHOLE_FILE_PATH.substring(0, WHOLE_FILE_PATH.length() - FILE_NAME.length());
        FILE_NAME = FILE_NAME.replace(".java", "");

        System.out.println(FILE_NAME);
        System.out.println(FILE_PATH);


        TypeSolver typeSolver = new CombinedTypeSolver(
                new ReflectionTypeSolver(),
                new JavaParserTypeSolver(new File("src/main/java/")));

        JavaSymbolSolver symbolSolver = new JavaSymbolSolver(typeSolver);
        JavaParser.getStaticConfiguration().setSymbolResolver(symbolSolver);
        comp = LexicalPreservingPrinter.setup(JavaParser.parse(new FileInputStream(WHOLE_FILE_PATH)));


        /*Read from the used given file, mounting the compilation unit (syntax tree)*/
        BufferedReader reader;
        int cont = 1;
        try {
            reader = new BufferedReader(new FileReader(WHOLE_FILE_PATH));
            String line = reader.readLine();
            while (line != null) {
                //This includes the case: a method of the current class is called in the body
                //of the function, like ClassName.my_method();

                fileContentF.put(cont, line);
                fileContentA.put(cont, line);
                line = reader.readLine();
                cont++;
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        /*Get mapper before doing the rest*/
        comp.findAll(VariableDeclarationExpr.class)
                .forEach(n -> {
                    if (PROJ_SETTINGS.matcher(n.toString()).find())
                        mapper = n.toString().split(",[\\s\\t\n]*")[1];
                });

        mapper = mapper.substring(1, mapper.length() - 2);

        System.out.println(mapper);


        /*Get arguments type of a method
        comp.findAll(MethodCallExpr.class).forEach(mce ->
                System.out.println(mce.resolveInvokedMethod().getQualifiedSignature()));*/

        comp.accept(new VarDeclVisitor(), JavaParserFacade.get(typeSolver));
        comp.accept(new MethodCallVisitor(), JavaParserFacade.get(typeSolver));
        comp.accept(new MethodChangerVisitor(), null);
        comp.accept(new ClassChangerVisitor(), null);

        // visit and change the methods names and parameters
        //VoidVisitorAdapter<Void> metChanger = new MethodChangerVisitor();
        //metChanger.visit(cu, null);


        //File with pure java content + $number instead of API commands
        //Files.write(new File("Modified.java").toPath(), comp.toString(), StandardCharsets.UTF_8);

        /*It writes to a file the commands from our notation*/
        BufferedWriter writer;
        try {
            writer = new BufferedWriter(new FileWriter(FILE_PATH + FILE_NAME + "APIParsed.java"));
            writer.write("package " + comp.getPackageDeclaration().get().getName().asString() + ";\n");
            for (ImportDeclaration item : comp.getImports())
                writer.write("import " + item.getName().asString() + ";\n");
            ClassOrInterfaceDeclaration myClass = new ClassOrInterfaceDeclaration();
            writer.write("\n public class " + FILE_NAME + "APIParsed {\n");
            writer.write("\n\t public static void main(String args[]) {\n");

            System.out.println(apiStatements);
            for (Integer key : apiStatements.keySet())
                writer.write("\n" + apiStatements.get(key));
            writer.write("\n\t}\n}");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


        String line = "";
        try {
            /*It writes an output file for Flink*/
            if (toFlink.contains(mapper)) {
                String endPath = "../abstraction-python-scripts/flink/out/" + FILE_NAME + "ParsedF.java";
                writer = new BufferedWriter(new FileWriter(endPath));

                /*Package line*/
                writer.write(fileContentF.get(1) + "\n\n");

                if (! importsFlink.isEmpty())
                    for (String item : importsFlink)
                        writer.write(item + "\n");

                for (int i = 2; i <= fileContentF.size(); i++) {
                    line = fileContentF.get(i);
                    if (line.contains(FILE_NAME))
                        line = line.replace(FILE_NAME, FILE_NAME + "ParsedF");
                    writer.write(line + "\n");
                }
                writer.close();
            }

            /*It writes an output file for Apex*/
            if (toApex.contains(mapper)) {
                String endPath = "../abstraction-python-scripts/apex/out/" + FILE_NAME + "ParsedA.java";
                writer = new BufferedWriter(new FileWriter(endPath));

                /*Package line*/
                writer.write(fileContentA.get(1) + "\n\n");

                importsApex.add("import com.datatorrent.api.StreamingApplication;");
                importsApex.add("import com.datatorrent.api.annotation.ApplicationAnnotation;");
                importsApex.add("import org.apache.hadoop.conf.Configuration;");
                importsApex.add("import com.datatorrent.api.DAG;");

                for (String item : importsApex)
                    writer.write(item + "\n");

                for (int i = 2; i <= fileContentA.size(); i++) {
                    line = fileContentA.get(i);
                    Pattern publicClass = Pattern.compile("public\\s+class\\s+" + FILE_NAME);
                    Matcher publicClassMatch = publicClass.matcher(line);
                    if (publicClassMatch.find()) {
                        line = line.replace(FILE_NAME, FILE_NAME + "ParsedA implements StreamingApplication");
                        line = "@ApplicationAnnotation(name=\"" + FILE_NAME + "\")\n" + line;
                    }
                    writer.write(line + "\n");
                }
                writer.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        /*After changes the content of comp can be printed to a file*/
        //LexicalPreservingPrinter.print(comp);

    }


    static class MethodCallVisitor extends ModifierVisitor<JavaParserFacade> {
        @Override
        public Node visit(MethodCallExpr n, JavaParserFacade javaParserFacade) {
            super.visit(n, javaParserFacade);
            System.out.println(n);

            /*Here is retrieved the type of the object instances that call methods.
            * For example: f.read(args);*/

            Matcher m = API_PACKAGE.matcher(javaParserFacade.getType(n.getScope().orElse(null)).describe());
            if (m.find()) { //if it is in out API
                int beginLine = n.getBegin().get().line;
                int endLine = n.getEnd().get().line;
                String contentF = fileContentF.get(beginLine);
                String contentA = fileContentA.get(beginLine);
                String target = n.toString();
                String placeholderF = n.toString();
                String placeholderA = n.toString();

                MethodCallExpr copy = n.clone();

                /*If this is our API method call, add to apiStatements and replace fileContent with a
                 * placeholder*/

                /*If it is the first method and it was not called by VarDeclVisitor*/
                if (apiStatements.get(beginLine) == null) {

                    /*If the method called is project settings, Time or it is one of common udfs,
                    does not add the caller as argument*/
                    Matcher proj = PROJ_SETTINGS.matcher(javaParserFacade.getType(n.getScope().orElse(null)).describe());
                    Matcher time = TIME_CLASS.matcher(javaParserFacade.getType(n.getScope().orElse(null)).describe());
                    boolean timeFound = time.find();
                    /*In this case, remove the whole line since it will not generate any code*/
                    if (proj.find()) {
                        /*Target for Project methods are the same for Flink and Apex*/
                        target = contentF;
                        /*In Flink case, it is necessary to create the environment, that is why a
                         * placeholder is necessary*/
                        if (n.getNameAsString().equals("start") && toFlink.contains(mapper)) {
                            placeholderF = "\t\t/*$placeholderMethodCall*/";
                            placeholderA = "";
                            streamOrBatch = n.getArgument(0).toString();
                            System.out.println(streamOrBatch);
                        }
                        /*In Apex case, Project methods do not generate any code. In Flink case, only
                        * project start generates code*/
                        else
                            placeholderF = placeholderA = "";
                    }

                    /*If it is a udf of the list and the first argument is a string, replace it
                    * with "new ClassName()" structure */
                    else if (udfs.contains(n.getNameAsString())) {

                        if (n.getArgument(0).isStringLiteralExpr()) {
                            String oldArg = n.getArgument(0).toString();
                            String firstArg = "new " + oldArg.replace("\"", "") + "()";
                            placeholderF = copy.toString().replace(oldArg, firstArg);
                            placeholderA = placeholderF;
                            n.getRange().ifPresent(r -> apiStatements.put(beginLine, "\t\t"));
                        }
                    }
                    /*If it is not the above (nor project neither udf) and it does not use Time
                    class, add the caller as argument and add a placeholder to the Parsed file*/
                    else if (! timeFound){
                        String newArg = n.getScope().get().toString();
                        copy.addArgument('\"' + newArg + '\"');
                        //placeholder = "\t\t" + newArg + "/*$placeholderMethodCall*/";
                        placeholderF = "/*$placeholderMethodCall*/";
                        placeholderA = "/*$placeholderMethodCall*/";
                    }


                    if (!udfs.contains(n.getNameAsString()) && !timeFound)
                        n.getRange().ifPresent(r -> apiStatements.put(beginLine, "\t\t" + copy.toString() + ";"));

                    contentF = contentF.replace(target, placeholderF);
                    fileContentF.put(beginLine, contentF);
                    contentA = contentA.replace(target, placeholderA);
                    fileContentA.put(beginLine, contentA);

                    System.out.println("File Content METHODS0 " + fileContentF.get(beginLine));
                }

                /*If this is our API method call and there is method chaining, add it to apiStatements
                 * and replace fileContent with nothing, since there is already a placeholder there*/
                else  {
                    /*Split on caller object is not a good idea, since if occurs a var attribution in which
                    the variable is substring of the caller, it will fail. So it is a better option
                    to split by the method*/
                    String caller = n.toString().split("\\.")[0];
                    Boolean isFirstMethod = false;
                    String methods[] = n.toString().split("\\)\\.");

                    /*If last method is actually the first and contains the caller, remove the caller*/
                    methods[methods.length - 1] = methods[methods.length - 1].replace(caller + ".", "");

                    String lastMethod = methods[methods.length - 1];
                    String lastMethodOld = lastMethod;
                    String lastMethodName = lastMethod.split("\\(")[0];

                    /*It means that a caller need to be added, since it is actually the first method and
                    * it is from class Data. There is a mix of VariableDeclarationExpr with MethodCallExpr*/
                    if (contentF.equals("\t\t/*$placeholderVariableDeclarationExpr*/")) {
                        Matcher dataMatcher = DATA_CLASS.matcher(javaParserFacade.getType(n.getScope().get()).describe());
                        if (dataMatcher.find()) {
                            lastMethod = " " + caller + "." + lastMethod;
                            isFirstMethod = true;
                        }
                    }


                    if (udfs.contains(lastMethodName)) {
                        if (n.getArguments().size() > 0 && n.getArgument(0).isStringLiteralExpr()) {
                            String oldArg = n.getArgument(0).toString();
                            String firstArg = "new " + oldArg.replace("\"", "") + "()";
                            lastMethod = lastMethod.replace(oldArg, firstArg);
                        }


                        /*This solves cases like System.out.println(d.map("Example"));*/
                        if (contentF.contains(lastMethodName)) {
                            fileContentF.put(beginLine, contentF.replace(lastMethodOld, lastMethod));
                        }
                        else {
                            if (contentF.endsWith(";")) contentF = contentF.replace(";", "");

                            /*If caller is present, does not add '.'*/
                            if (isFirstMethod) fileContentF.put(beginLine, contentF + lastMethod + ";");
                            else fileContentF.put(beginLine, contentF + "." + lastMethod + ";");
                        }


                        if (contentA.contains(lastMethodName)) {
                            fileContentA.put(beginLine, contentA.replace(lastMethodOld, lastMethod));
                        }
                        else {
                            if (contentA.endsWith(";")) contentA = contentA.replace(";", "");

                            /*If caller is present, does not add '.'*/
                            if (isFirstMethod) fileContentA.put(beginLine, contentA + lastMethod + ";");
                            else fileContentA.put(beginLine, contentA + "." + lastMethod + ";");
                        }


                        System.out.println("File Content METHODS1 " + fileContentF.get(beginLine));
                    }

                    else if (methods.length >= 1) {
                        String curr;
                        System.out.println("Last method: " + lastMethod);
                        Pattern noArgs = Pattern.compile("\\([ \t\n]*\\)");
                        Matcher mNoArgs = noArgs.matcher(lastMethod);
                        Integer idxParenthesis = lastMethod.lastIndexOf(")");

                        String addCaller = ",\"" + caller + "\");";

                        /*If method does not have arguments, else (has arguments)*/
                        if (mNoArgs.find())
                            addCaller = "\"" + caller + "\");";

                        curr = "." + lastMethod.substring(0, idxParenthesis) + addCaller;

                        String pattern = "^\\s*$";
                        Pattern whitespace = Pattern.compile(pattern);
                        Matcher mwhitespace = whitespace.matcher(apiStatements.get(beginLine));

                        /*If line is empty or with whitespace*/
                        if (mwhitespace.find())
                            n.getRange().ifPresent(r -> apiStatements.put(beginLine,
                                    "\t\t" + caller + curr));

                        else
                            n.getRange()
                                .ifPresent(r -> apiStatements.put(beginLine,
                                        apiStatements.get(beginLine).replace(";", curr)));

                        fileContentF.put(beginLine, contentF.replace("." + lastMethodOld, "/*$placeholderMethodCall*/"));
                        fileContentA.put(beginLine, contentA.replace("." + lastMethodOld, "/*$placeholderMethodCall*/"));

                    }
                }

                //For other lines
                for (int i = beginLine + 1; i <= endLine; i++) {
                    if (apiStatements.get(i) == null) {
                        apiStatements.put(i, "");
                        fileContentF.put(i, "");
                        fileContentA.put(i, "");
                    }
                }
            }
            return n;
        }

    }


    static String[] getReceiverAndType(Node n, Expression item) {
        String[] returnValues = new String[2];
        String temp = n.toString().split("=")[0].trim();
        String[] tokens = temp.split("\\s+");
        String receiver = tokens[tokens.length-1].trim();

        returnValues[0] = receiver;
        if (temp.contains("<") && temp.contains(">")) {
            String dataType = temp.substring(temp.indexOf("<") + 1, temp.lastIndexOf(">"));
            returnValues[1] = dataType;
        }
        return returnValues;
    }

    /*It is being considered only the 4 scenarios below: varDeclaration is
     "int var = 0" or "Data<String> d = new Data<>();" or Data<String> d = d2.print();
     or Data<String> d = d2.flatMap().reduce().print();*/
    static class VarDeclVisitor extends ModifierVisitor<JavaParserFacade> {
        @Override
        public Node visit(VariableDeclarationExpr n, JavaParserFacade arg) {

            super.visit(n, arg);

            Matcher m = API_PACKAGE.matcher(arg.getType(n).describe());
            if (m.find()) {
                int beginLine = n.getBegin().get().line;
                int endLine = n.getEnd().get().line;
                String placeholder = "\t\t/*$placeholderVariableDeclarationExpr*/";

                Node nodeCopy = n.clone();

                /*Add the caller to the first method (the others will receive it in MethodCallVisitor).
                * When there is a method chaining, we get a list with the partial calls
                * [d.flatMap().reduce(), d.flatMap(), d]
                * The necessary part is 'd', the caller, which is not a MethodCall*/

                List<String> test = new LinkedList<>();

                nodeCopy.findAll(MethodCallExpr.class)
                        .forEach(item -> {

                            String instanceName = item.getScope().get().toString();
                            MethodCallExpr methodSet = new MethodCallExpr(instanceName + ".setDataInfo");
                            String caller = "\"" + instanceName + "\"";

                            if (! item.getScope().get().isMethodCallExpr()) {
                                /*if (!udfs.contains(item.getNameAsString()))
                                    item.addArgument(caller);*/

                                String[] returnedValues = getReceiverAndType(n, item);
                                for (String i : returnedValues)
                                    methodSet.addArgument("\"" + i + "\"");

                                String firstPart = nodeCopy.toString().split(instanceName + "\\.")[0];
                                String content = firstPart + methodSet.toString();
                                //String content = item.toString().replaceFirst(instanceName, firstPart + methodSet.toString());

                                test.add(content);

                            }

                        });

                nodeCopy.findAll(ObjectCreationExpr.class)
                    .forEach(item -> {
                        Matcher io_data_dtrans_matcher = IO_DATA_DTRANS
                                .matcher(item.getType().toString());

                        System.out.println(">>>>" + item.getType().getParentNode().get());
                        System.out.println(arg.getType(n).describe());

                        Boolean is_io_data_dtrans = io_data_dtrans_matcher.find();

                        if (! item.getType().toString().equals("ProjectSettings")) {
                            String[] returnedValues = getReceiverAndType(n, item);
                            for (String i : returnedValues)
                                item.addArgument("\"" + i + "\"");
                        }

                        /*FileContent line will be empty if constructor is not Data*/
                        Matcher data_matcher = DATA_CLASS.matcher(item.getType().toString());
                        if (!data_matcher.find()) {
                            System.out.println("I'M HERE " + nodeCopy.toString().split("\\)[\n \t]*\\.")[0]);
                            test.add(nodeCopy.toString().split("\\)[\n \t]*\\.")[0]);
                            test.add("");
                        }

                    });

                System.out.println("TEST IS " + test);

                /*First method has to be handled here, after all possible changes in nodeCopy*/
                String firstMethod = nodeCopy.toString().split("\\)[\n \t]*\\.")[0];


                if (test.size() >= 1)
                    firstMethod = test.get(0);
                if (test.size() == 2)
                    placeholder = test.get(1);


                if (! firstMethod.endsWith(")"))
                    firstMethod += ")";
                if (! firstMethod.endsWith(";"))
                    firstMethod += ";";

                String methodToAdd = "\t\t" + firstMethod;
                System.out.println("METHOD TO ADD " + methodToAdd);

                n.getRange().ifPresent(r -> apiStatements.put(beginLine, methodToAdd));
                fileContentF.put(beginLine, placeholder);
                fileContentA.put(beginLine, placeholder);


                for (int i = beginLine + 1; i <= endLine; i++) {
                    apiStatements.put(i, "");
                    fileContentF.put(i, "");
                    fileContentA.put(i, "");
                }
            }

            return n;
        }
    }


    /**
     * Here can be done the logic for mapping UDFs signature
     */
    private static class ClassChangerVisitor extends VoidVisitorAdapter<Void> {
        @Override
        public void visit(ClassOrInterfaceDeclaration n, Void arg) {
            super.visit(n, arg);
            boolean found = false;
            boolean ignore = false;
            int beginLine = n.getBegin().get().line;
            int endLine = n.getEnd().get().line;

            String func = n.getImplementedTypes().toString();
            String funcF = func;
            String funcA = func;
            String before = "";

            if (! func.isEmpty()) {
                if (func.contains("MapFunction")) {
                    before = "MapFunction";
                    funcF = before;
                    funcA = "Function.MapFunction";
                    importsFlink.add("import org.apache.flink.api.common.functions.MapFunction;");
                    importsApex.add("import org.apache.apex.malhar.lib.function.Function;");
                    //importsApex.add("import org.apache.apex.malhar.stream.api.ApexStream;");
                }
                else if (func.contains("FlatMapFunction")) {
                    before = "FlatMapFunction";
                    funcF = before;
                    funcA = "Function.FlatMapFunction";
                    importsFlink.add("import org.apache.flink.api.common.functions.FlatMapFunction;");
                    importsApex.add("import org.apache.apex.malhar.lib.function.Function;");
                    //importsApex.add("import org.apache.apex.malhar.stream.api.ApexStream;");
                }
                else if (func.contains("ReduceFunction")) {
                    before = "ReduceFunction";
                    funcF = before;
                    funcA = "Function.ReduceFunction";
                    importsFlink.add("import org.apache.flink.api.common.functions.ReduceFunction;");
                    importsApex.add("import org.apache.apex.malhar.lib.window.accumulation;");
                    //importsApex.add("import org.apache.apex.malhar.stream.api.ApexStream;");
                }
                else if (func.contains("FilterFunction")) {
                    before = "FilterFunction";
                    funcF = before;
                    funcA = "Function.FilterFunction";
                    importsFlink.add("import org.apache.flink.api.common.functions.FilterFunction;");
                    importsApex.add("import org.apache.apex.malhar.lib.function.Function;");
                    //importsApex.add("import org.apache.apex.malhar.stream.api.ApexStream;");
                }
                else if (func.contains("FoldFunction")) {
                    before = "FoldFunction";
                    funcF = before;
                    funcA = "Function.FoldFunction";
                    importsFlink.add("import org.apache.flink.api.common.functions.FoldFunction;");
                    //Window accumulation (stream)
                    importsApex.add("import org.apache.apex.malhar.lib.window.accumulation;");
                    //importsApex.add("import org.apache.apex.malhar.stream.api.ApexStream;");
                }
                /*else if (func.contains("main")) {
                    System.out.println("");
                }*/
                else {
                    ignore = true;
                }

                if (!ignore && toFlink.contains(mapper)) {
                    for (int i = beginLine; i <= endLine && !found; i++) {
                        String contentF = fileContentF.get(i);
                        if (contentF.contains(before)) {
                            contentF = contentF.replace(before, funcF);

                            /*Mapping parameters type for Flink. Split is done to separate each
                            * type present implements interface. For ex: MapFunction<String, Integer>*/
                            String[] parameters = func.split(",");
                            for (String param : parameters)
                                contentF = mapFlinkTypes(param, contentF);

                            fileContentF.put(beginLine, contentF);
                            found = true;
                        }
                    }
                }

                found = false;

                if (!ignore && toApex.contains(mapper)) {
                    //comp.addImport("import org.apache.apex.malhar.lib.function.Function;");
                    for (int i = beginLine; i <= endLine && !found; i++) {
                        String contentA = fileContentA.get(i);
                        if (contentA.contains(before)) {
                            contentA = contentA.replace(before, funcA);

                            /*Mapping parameters type for Apex. Split is done to separate each type
                            * present implements interface. For ex: MapFunction<String, Integer>*/
                            String[] parameters = func.split(",");
                            for (String param : parameters)
                                contentA = mapApexTypes(param, contentA);

                            fileContentA.put(beginLine, contentA);
                            found = true;
                        }
                    }
                }
            }


            /*if (n.getClass().getName().contains("main")) {
            }*/
        }
    }

    private static class MethodChangerVisitor extends ModifierVisitor<JavaParserFacade> {
        @Override
        public Node visit(MethodDeclaration n, JavaParserFacade arg) {
            super.visit(n, arg);
            System.out.println(n.getDeclarationAsString());
            int beginLine = n.getBegin().get().line;
            int endLine = n.getEnd().get().line;
            boolean found = false;
            String searchedMethod = n.getName().toString();

            /*Get the parameters together with its types*/
            NodeList parameters = n.getParameters();
            System.out.println("ARGSSSSS: " + parameters.get(0));


            int line = 0;
            /*If mapper is Flink or both*/
            if (toFlink.contains(mapper)) {
                for (line = beginLine; line <= endLine && !found; line++)
                    if (fileContentF.get(line).contains(searchedMethod)) {
                        found = true;
                    }
                line--;

                String toReplace = fileContentF.get(line);
                switch(searchedMethod) {
                    case("main"):
                        if (! toReplace.contains("throws Exception"))
                            toReplace = toReplace.replace("args)", "args) throws Exception");
                        break;
                }

                /*Mapping parameters type for Flink*/
                for (int i = 0; i < parameters.size(); i++) {
                    String current = parameters.get(i).toString();
                    toReplace = mapFlinkTypes(current, toReplace);
                }

                fileContentF.put(line, toReplace);
            }

            found = false;
            /*If mapper is Apex or both*/
            if (toApex.contains(mapper)) {
                for (line = beginLine; line <= endLine && !found; line++)
                    if (fileContentA.get(line).contains(searchedMethod)) {
                        found = true;
                    }
                line--;

                String toReplace = fileContentA.get(line);
                switch(searchedMethod) {
                    case("map"):
                        toReplace = fileContentA.get(line).replace("map", "f");
                        break;
                    case("flatMap"):
                        toReplace = fileContentA.get(line).replace("flatMap", "f");
                        break;
                    case("main"):
                        String populate = "@Override\npublic void populateDAG(DAG dag, Configuration conf) ";
                        if (toReplace.contains("{"))
                            populate += "{";
                        toReplace = fileContentA.get(line).replace(toReplace, populate);
                        break;
                }

                /*Mapping parameters type for Apex*/
                for (int i = 0; i < parameters.size(); i++) {
                    String current = parameters.get(i).toString();
                    toReplace = mapApexTypes(current, toReplace);
                }

                fileContentA.put(line, toReplace);
            }

            // add a new parameter to the method
            //n.addParameter("String", );
            return n;
        }
    }


    protected static String mapFlinkTypes(String current, String toReplace) {

        if (current.contains("KV"))
            toReplace = toReplace.replace("KV", KV.flinkKV());

        if (current.contains("Data"))
            toReplace = toReplace.replace("Data", Data.flinkData(streamOrBatch));
        else if (current.contains("FileIn"))
            toReplace = toReplace.replace("FileIn", FileIn.flinkFileInput(streamOrBatch));
        else if (current.contains("FileOut"))
            toReplace = toReplace.replace("FileOut", FileOut.flinkFileOutput(streamOrBatch));
        else if (current.contains("KafkaIn"))
            toReplace = toReplace.replace("KafkaIn", KafkaIn.flinkKafkaInput(streamOrBatch));
        else if (current.contains("KafkaOut"))
            toReplace = toReplace.replace("KafkaOut", KafkaOut.flinkKafkaOutput(streamOrBatch));

        return toReplace;
    }

    protected static String mapApexTypes(String current, String toReplace) {

        if (current.contains("KV"))
            toReplace = toReplace.replace("KV", KV.apexKV());

        if (current.contains("Data"))
            toReplace = toReplace.replace("Data", Data.apexData());
        /*else if (current.contains("FileIn"))
            toReplace = toReplace.replace("FileIn", FileIn.apexFileInput());
        else if (current.contains("FileOut"))
            toReplace = toReplace.replace("FileOut", FileOut.apexFileOutput());*/
        else if (current.contains("KafkaIn"))
            toReplace = toReplace.replace("KafkaIn", KafkaIn.apexKafkaInput());
        else if (current.contains("KafkaOut"))
            toReplace = toReplace.replace("KafkaOut", KafkaOut.apexKafkaOutput());

        return toReplace;
    }


    /*private static class MethodNameCollector extends VoidVisitorAdapter<List<String>> {
        @Override
        public void visit(MethodDeclaration md, List<String> collector) {
            super.visit(md, collector);
            collector.add(md.getNameAsString());
        }
    }

    */
}


