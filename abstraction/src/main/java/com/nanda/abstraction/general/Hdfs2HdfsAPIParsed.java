package com.nanda.abstraction.general;
import com.nanda.abstraction.general.interfaces.MapFunction;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

 public class Hdfs2HdfsAPIParsed {

	 public static void main(String args[]) {

		ProjectSettings project = new ProjectSettings("Flink", "Hdfs2Hdfs");
		project.setPackageName("experient");
		project.start("batch");
		FileIO<String> f = new FileIO<>(FileIO.FileFormat.Hdfs, "f", "String");
		Data<String> d = f.setDataInfo("d", "String", new Data<>()).read("localhost:8020/tmp/201904/", "","f");


		f.write(TP.build("localhost:8020/tmp/output/"), TP.build("test.txt"), TP.build(1024), d, "f");
		project.finish();
	}
}
