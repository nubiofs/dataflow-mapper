package com.nanda.abstraction.general.interfaces;

import java.io.Serializable;

public interface ReduceFunction<I> extends Serializable {

    public I reduce(I input1, I input2);
}
