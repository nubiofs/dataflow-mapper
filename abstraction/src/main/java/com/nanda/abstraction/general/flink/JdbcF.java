package com.nanda.abstraction.general.flink;

import com.nanda.abstraction.general.*;

import java.util.HashMap;

public class JdbcF<T> extends Jdbc<T> {

    private Auxiliar aux = new Auxiliar();
    private String flinkEndPath = "../abstraction-python-scripts/flink/out/";

    public JdbcF(String ioType, String jdbcVarName, String jdbcVarType) {
        super(ioType, jdbcVarName, jdbcVarType);
    }

    public Data<T> read(String tableName, HashMap<String, Object> tupleAndTypes, String key, String query, Integer partitionCount, String caller) {
        String action = "readF";
        TypeHelper typeHelper = new TypeHelper();

        String imports =
                "import org.apache.flink.api.common.typeinfo.BasicTypeInfo;\n" +
                "import org.apache.flink.api.java.io.jdbc.JDBCInputFormat;\n" +
                "import org.apache.flink.api.java.typeutils.RowTypeInfo;\n" +
                "import org.apache.flink.types.Row;\n";


        String jdbcReadContent = "\n\n//placeholder\n";

        Integer currSuffix = aux.getBuilderInSuffix();

        jdbcReadContent += String
                .format("final RowTypeInfo rowTypeInfo%s = " +
                                "new RowTypeInfo(new TypeInformation<?>[] { %s });\n",
                        currSuffix, typeHelper.getBasicTypes(tupleAndTypes));

        jdbcReadContent += String
                .format("JDBCInputFormat.JDBCInputFormatBuilder inputBuilder%s = " +
                                "JDBCInputFormat\n.buildJDBCInputFormat().setDrivername(\"%s\")" +
                                "\n.setDBUrl(\"%s\")\n.setQuery(\"%s\").setRowTypeInfo(rowTypeInfo%s);\n"
                        , currSuffix, this.databaseDriver, this.databaseUrl, query, currSuffix);


        //String flinkDataType = Parser.mapFlinkTypes(this.dataType, this.dataType);
        if ( !this.dataType.equals("") && !this.dataName.equals("")) {
            if (ProjectSettings.processingChoice.toLowerCase().equals("stream"))
                jdbcReadContent += String.format("DataStream<Row> %s = environment" +
                        ".createInput(inputBuilder%s.finish());\n", this.dataName, currSuffix);
            else
                jdbcReadContent += String.format("DataSet<Row> %s = environment" +
                        ".createInput(inputBuilder%s.finish());\n", this.dataName, currSuffix);
        }

        aux.writeContent(flinkEndPath, imports, jdbcReadContent);

        return new Data<T>(ProjectSettings.processingChoice);
    }

    /****
     This method is not intended to be called by the user
     ***/
    public void write(String tableName, HashMap<String, Object> tupleAndTypes, String query, String caller) {
        String action = "writeF";
        TypeHelper typeHelper = new TypeHelper();

        String imports = "import org.apache.flink.api.common.typeinfo.TypeInformation;\n" +
                "import org.apache.flink.api.java.io.jdbc.JDBCOutputFormat;\n";

        String jdbcWriteContent = "\n\n//placeholder\n";

        Integer currSuffix = aux.getBuilderOutSuffix();

        jdbcWriteContent += String
                .format("JDBCOutputFormat.JDBCOutputFormatBuilder outputBuilder%s =" +
                                "JDBCOutputFormat\n.buildJDBCOutputFormat().setDrivername(\"%s\")" +
                                "\n.setDBUrl(\"%s\")\n.setQuery(\"%s\")\n.setSqlTypes(new int[] {%s});\n"
                        , currSuffix, this.databaseDriver, this.databaseUrl, query, typeHelper.getSQLTypes(tupleAndTypes));

        jdbcWriteContent += String.format("%s.output(outputBuilder%s.finish());\n\n"
                , this.dataName, currSuffix);

        aux.writeContent(flinkEndPath, imports, jdbcWriteContent);
    }

}
