package com.nanda.abstraction.general.flink;

import com.nanda.abstraction.general.*;

public class FileF<T> extends FileIO<T> {

    private Auxiliar aux = new Auxiliar();
    private String flinkEndPath = "../abstraction-python-scripts/flink/out/";

    public FileF(String ioType, String fileVarName, String fileVarType) {
        super(ioType, fileVarName, fileVarType);
    }

    public Data<T> readTextFile(String inputDirName, String fileName, String caller) {
        String imports = "import org.apache.flink.core.fs.FileSystem;\n";

        String fileReadContent = "\n//placeholder\n";
        String flinkDataType = FlinkParser.mapFlinkTypes(this.dataType, this.dataType);
        if ( !this.dataType.equals("") && !this.dataName.equals("")) {
            if (ProjectSettings.processingChoice.toLowerCase().equals("stream"))
                fileReadContent += String.format("DataStream<%s> %s = ", flinkDataType, this.dataName);
            else
                fileReadContent += String.format("DataSet<%s> %s = ", flinkDataType, this.dataName);
        }
        fileReadContent += String.format("env.readTextFile(\"%s\");\n\n", inputDirName + fileName);

        aux.writeContent(flinkEndPath, imports, fileReadContent);

        return new Data<T>(ProjectSettings.processingChoice);
    }

    public Data<T> readHdfsFile(String inputDirName, String fileName, String caller) {
        return readTextFile("hdfs://" + inputDirName, fileName, caller);
    }


    public void writeTextFile(String outputDirName, String fileName, Integer maxLength, Data d, String caller) {
        String imports = "import org.apache.flink.core.fs.FileSystem;\n";

        String fileWriteContent = "\n//placeholder\n";
        fileWriteContent += String.format("%s.writeAsText(\"%s\", " +
                        "FileSystem.WriteMode.OVERWRITE).setParallelism(1);\n\n",
                d.getDataName(), outputDirName + fileName);

        aux.writeContent(flinkEndPath, imports, fileWriteContent);
    }

    public void writeHdfsFile(String outputDirName, String fileName, Integer maxLength, Data d, String caller) {
        writeTextFile("hdfs://" + outputDirName, fileName, maxLength, d, caller);
    }

}