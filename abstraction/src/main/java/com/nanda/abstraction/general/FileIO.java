package com.nanda.abstraction.general;

import com.nanda.abstraction.general.apex.FileA;
import com.nanda.abstraction.general.flink.FileF;

public class FileIO<T> extends IO {

    public enum FileFormat {
        Text, Hdfs, Csv;
    }

    protected FileFormat format;
    protected String fileVarName = "";
    protected String fileVarType = "";
    protected String dataName = "";
    protected String dataType = "";
    protected String tabs = "\t\t";

    private FileA fileA;
    private FileF fileF;

    private Auxiliar aux = new Auxiliar();

    /*User constructor*/
    public FileIO(FileFormat format) {
        super("file");
        this.format = format;
    }

    /*Constructors used for the mapping*/
    public FileIO(String ioType, String fileVarName, String fileVarType) {
        super(ioType);
        this.fileVarName = fileVarName;
        this.fileVarType = fileVarType;
    }

    public FileIO(FileFormat format, String fileVarName, String fileVarType) {
        super("file");
        /*This part sets content for FileIO*/
        this.format = format;
        this.fileVarName = fileVarName;
        this.fileVarType = fileVarType;

        /*This part uses the other FileIO constructor to set content to FileA and FileF*/
        this.fileA = new FileA("file", fileVarName, ApexParser.mapApexTypes(fileVarType, fileVarType));
        this.fileF = new FileF("file", fileVarName, FlinkParser.mapFlinkTypes(fileVarType, fileVarType));
        System.out.println("File Constructor");
        System.out.println(String.format("Inside File constr2: %s, %s", this.fileVarName, this.fileVarType));
    }

    /*Return this to allow chaining*/
    protected <O> FileIO<T> setDataInfo(String dataName, String dataType, Data<O> d) {
        this.dataName = dataName;
        this.dataType = dataType;

        fileA.dataName = dataName;
        fileA.dataType = dataType;

        fileF.dataName = dataName;
        fileF.dataType = dataType;

        /*ItemReceiver receiver = new ItemReceiver();
        receiver.setDataReceiver(dataName, dataType);*/

        System.out.println("FFFFFFFFFFFFFFFFFFFFF " + dataName + " >>>>>> " + dataType);
        return this;
    }

    /***User function***/
    public Data<T> read(String inputDirName, String fileName) {
        return new Data<>("batch");
    }

    /***Mapping function (converter)***/
    protected Data<T> read(String inputDirName, String fileName, String caller) {
        String mp = ProjectSettings.mapper.toLowerCase();

        if (this.format.equals(FileFormat.Hdfs))
            aux.callScript(new ProcessBuilder("python3","../abstraction-python-scripts/pom-files/add_pom_dep.py", ProjectSettings.packageName, ProjectSettings.mapper, "hdfs"));

        if (mp.equals("apex") || mp.equals("both")) {
            if (this.format.equals(FileFormat.Text)) {
                fileA.readTextFile(inputDirName, fileName, caller);
                aux.incFileInSuffix();
            }
            else if (this.format.equals(FileFormat.Hdfs)) {
                fileA.readHdfsFile(inputDirName, fileName, caller);
                aux.incHdfsInSuffix();
            }
        }

        if (mp.equals("flink") || mp.equals("both")) {
            if (this.format.equals(FileFormat.Text))
                fileF.readTextFile(inputDirName, fileName, caller);
            else if (this.format.equals(FileFormat.Hdfs))
                fileF.readHdfsFile(inputDirName, fileName, caller);
        }

        Data<T> d = new Data<>(ProjectSettings.processingChoice);
        d.setDataName(fileA.dataName);
        d.setDataType(fileA.dataType);
        return d;
    }


    /***User function***/
    public void write(String outputDirName, String fileName, Integer maxLength, Data d) { }

    /****
    This method is not intended to be called by the user
    ***/
    protected void write(String outputDirName, String fileName, Integer maxLength, Data d, String caller) {
        String mp = ProjectSettings.mapper.toLowerCase();

        if (this.format.equals(FileFormat.Hdfs))
            aux.callScript(new ProcessBuilder("python3","../abstraction-python-scripts/pom-files/add_pom_dep.py", ProjectSettings.packageName, ProjectSettings.mapper, "hdfs"));

        if (mp.equals("apex") || mp.equals("both")) {
            if (this.format.equals(FileFormat.Text)) {
                fileA.writeTextFile(outputDirName, fileName, maxLength, d, caller);
                aux.incFileOutSuffix();
            }
            else if (this.format.equals(FileFormat.Hdfs)) {
                fileA.writeHdfsFile(outputDirName, fileName, maxLength, d, caller);
                aux.incHdfsOutSuffix();
            }
        }

        if (mp.equals("flink") || mp.equals("both")) {
            if (this.format.equals(FileFormat.Text))
                fileF.writeTextFile(outputDirName, fileName, maxLength, d, caller);
            else if (this.format.equals(FileFormat.Hdfs))
                fileF.writeHdfsFile(outputDirName, fileName, maxLength, d, caller);
        }
    }
}