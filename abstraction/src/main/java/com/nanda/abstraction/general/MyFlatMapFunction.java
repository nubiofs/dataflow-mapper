package com.nanda.abstraction.general;

public interface MyFlatMapFunction<I, O> {

    public O flatMap(I input);
}

