package com.nanda.abstraction.general.apex;

import com.nanda.abstraction.general.*;


public class KafkaA<T> extends Kafka<T> {

    private Auxiliar aux = new Auxiliar();
    private String apexEndPath = "../abstraction-python-scripts/apex/out/";
    private String tabs = "\t\t";
    private String kafkaInputVarName = "";
    private String kafkaOutputVarName = "";

    public KafkaA(String ioType, String kafkaVarName, String kafkaVarType) {
        super(ioType, kafkaVarName, kafkaVarType);
    }

    /*Getters and Setters*/
    public String getKafkaOutputVarName() { return kafkaOutputVarName; }

    public String getKafkaInputVarName() { return kafkaInputVarName; }

    public void setKafkaOutputVarName(String kafkaOutputVarName) {
        this.kafkaOutputVarName = kafkaOutputVarName;
    }

    public void setKafkaInputVarName(String kafkaInputVarName) {
        this.kafkaInputVarName = kafkaInputVarName;
    }

    public Data<T> read(String groupid, String bootstrap, String topic, String zookeeper, Boolean latest, String caller) {
        String action = "kafkaR";

        String imports =
                "import com.datatorrent.contrib.kafka.KafkaSinglePortStringInputOperator;\n" +
                "import static org.apache.apex.malhar.stream.api.Option.Options.name;\n";

        String kafkaReadContent = "\n//placeholder\n";
        /*if (this.kafkaInputVarName.equals("")) {
            setKafkaInputVarName(kafkaVarName);
            kafkaReadContent += String
                    .format("KafkaSinglePortByteArrayInputOperator %s = new KafkaSinglePortByteArrayInputOperator());\n",
                            this.kafkaVarName);
        }*/

        kafkaReadContent += String
                .format("KafkaSinglePortStringInputOperator %s = new KafkaSinglePortStringInputOperator();\n",
                        this.kafkaVarName);

        String tagName = "kafkaIn" + aux.getKafkaInSuffix();
        String apexDataType = ApexParser.mapApexTypes(this.dataType, this.dataType);
        if ( !dataType.equals("") && !dataName.equals(""))
            kafkaReadContent += String.format("%sApexStream<%s> %s = ", tabs, apexDataType, dataName);
        kafkaReadContent += String.format("StreamFactory.fromInput(%s, %s.outputPort, name(\"%s\"));\n\n",
                this.kafkaVarName, this.kafkaVarName, tagName);

        System.out.println("Before add_properties");
        String offset = "LATEST";
        if (!latest) offset = "EARLIEST";
        aux.callScript(new ProcessBuilder("python3","../abstraction-python-scripts/properties-files/add_properties_apex.py",
                action, tagName, topic, zookeeper, offset));

        /*Write to imports.java and content.java*/
        aux.writeContent(apexEndPath, imports, kafkaReadContent);

        /*Write to importsTest.java and contentTest.java*/
        String importsTest = "import info.batey.kafka.unit.KafkaUnit;\n" +
                "import info.batey.kafka.unit.KafkaUnitRule;\n";

        kafkaReadContent = "";
        if (aux.getKafkaInSuffix() == 1 && aux.getKafkaOutSuffix() == 1)
            kafkaReadContent = "//placeholderKafka\n@Rule\n" +
                "\tpublic KafkaUnitRule kafkaUnitRule = new KafkaUnitRule(zkPort, brokerPort);\n";

        kafkaReadContent += String.format("//placeholderKafka\n" +
                "\tKafkaUnit ku%s = kafkaUnitRule.getKafkaUnit();\n" +
                "\t\tku%s.createTopic(\"%s\");\n\n" +
                "\t\t/*placeholderKafka*/\n", aux.getKafkaInSuffix(), aux.getKafkaInSuffix(), topic);

        aux.writeGeneral(apexEndPath, "importsTest.java", importsTest);
        aux.writeGeneral(apexEndPath, "contentTest.java", kafkaReadContent);


        return new Data<T>(ProjectSettings.processingChoice);
    }

    public void write(String bootstrap, String topic, Data data, String caller) {
        String action = "kafkaW";

        //aux.callScript(new ProcessBuilder("python3", "../abstraction-python-scripts/pom-files/add_pom_dep.py", ProjectSettings.packageName, ProjectSettings.mapper, "kafka"));

        String outputOperatorName = this.kafkaVarName + "Out";
        String imports = "import com.datatorrent.contrib.kafka.KafkaSinglePortOutputOperator;\n" +
                         "import static org.apache.apex.malhar.stream.api.Option.Options.name;\n";

        String kafkaWriteContent = "\n//placeholder\n";
        /*if (this.kafkaOutputVarName.equals("")) {
            setKafkaOutputVarName(outputOperatorName);
            kafkaWriteContent +=
                    String.format("KafkaSinglePortOutputOperator<String,String> %s = new KafkaSinglePortOutputOperator<>();\n",
                            outputOperatorName);
        }*/

        kafkaWriteContent +=
                String.format("KafkaSinglePortOutputOperator<%s, String> %s = new KafkaSinglePortOutputOperator<>();\n",
                        this.kafkaVarType, outputOperatorName);

        String tagName = "kafkaOut" + aux.getKafkaOutSuffix();
        kafkaWriteContent += String.format("%s%s.endWith(%s, %s.inputPort, name(\"%s\"));\n\n",
                tabs, data.getDataName(), outputOperatorName, outputOperatorName, tagName);

        System.out.println("Before add_properties");
        aux.callScript(new ProcessBuilder("python3", "../abstraction-python-scripts/properties-files/add_properties_apex.py",
                action, tagName, topic));

        /*Write to imports.java and content.java*/
        aux.writeContent(apexEndPath, imports, kafkaWriteContent);


        /*Write to importsTest.java and contentTest.java*/
        String importsTest = "import info.batey.kafka.unit.KafkaUnit;\n" +
                "import info.batey.kafka.unit.KafkaUnitRule;\n";

        kafkaWriteContent = "";
        if (aux.getKafkaInSuffix() == 1 && aux.getKafkaOutSuffix() == 1)
            kafkaWriteContent = "//placeholderKafka\n@Rule\n" +
                    "\tpublic KafkaUnitRule kafkaUnitRule = new KafkaUnitRule(zkPort, brokerPort);\n";

        kafkaWriteContent += String.format("//placeholderKafka\n" +
                "\tKafkaUnit kuOut%s = kafkaUnitRule.getKafkaUnit();\n" +
                "\t\tkuOut%s.createTopic(\"%s\");\n\n" +
                "\t\t/*placeholderKafka*/\n", aux.getKafkaOutSuffix(), aux.getKafkaOutSuffix(), topic);

        aux.writeGeneral(apexEndPath, "importsTest.java", importsTest);
        aux.writeGeneral(apexEndPath, "contentTest.java", kafkaWriteContent);
    }

}
