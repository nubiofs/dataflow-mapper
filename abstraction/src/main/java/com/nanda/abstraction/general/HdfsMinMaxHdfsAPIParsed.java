package com.nanda.abstraction.general;
import com.nanda.abstraction.general.Data;
import com.nanda.abstraction.general.FileIO;
import com.nanda.abstraction.general.KV;
import com.nanda.abstraction.general.ProjectSettings;
import com.nanda.abstraction.general.interfaces.MapFunction;

 public class HdfsMinMaxHdfsAPIParsed {

	 public static void main(String args[]) throws Exception {

		ProjectSettings project = new ProjectSettings("Both", "HdfsMinMaxHdfs");
		project.setPackageName("experiment");
		project.start("batch");
		FileIO<String> f = new FileIO<>(FileIO.FileFormat.Hdfs, "f", "String");
		Data<String> d = f.setDataInfo("d", "String", new Data<String>()).read("localhost:8020/tmp/201906/prev/", "201906_apex.txt","f");
		Data<KV<String, Double>> d2 = d.setDataInfo("d2", "KV<String, Double>", new Data<KV<String, Double>>());
		FileIO<KV<String, Double>> f2 = new FileIO<>(FileIO.FileFormat.Hdfs, "f2", "KV<String, Double>");
		d2.minByKey("d2");
		f2.write("localhost:8020/tmp/output/201906/prev/", "min_apex.txt", 52428800, d2, "f2");
		d2.maxByKey("d2");
		f2.write("localhost:8020/tmp/output/201906/prev/", "max_apex.txt", 52428800, d2, "f2");
		project.finish();
	}
}