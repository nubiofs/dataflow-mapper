package com.nanda.abstraction.general;

import java.util.HashMap;

/***This class handles with type parameters passed to methods from the abstraction API.
It will be useful to know that we need to keep it inside APIParsed and
also in mapped files. This avoids problems with declarations of variables (they
were not declared before inside API Parsed, since common Java types are pure Java)
This type needs to be treated inside parser, adapting VarDeclVisitor and MethodCall*/
public class TP {

    /*It contains Java variable content*/
    static Object content;
    static String hashName;
    /*It contains Java HashMap content*/
    public static HashMap<String, Object> hash;

    static TP tp;

    /***Constructors***/
    /*public TP(HashMap<String, Object> i) { this.hash = new HashMap<>(); }

    protected TP(HashMap<String, Object> i, String caller, String type) {
        this.hash = new HashMap<>();
        ItemReceiver receiver = new ItemReceiver();
        receiver.newTPReceiver(hash.toString(), caller, type);
    }*/

    public static TP create() {
        TP.hash = new HashMap<>();
        return tp;
    }

    public static TP setDataInfo(String caller, String type) {
        TP.hash = new HashMap<>();
        TP.hashName = caller;
        ItemReceiver receiver = new ItemReceiver();
        receiver.createTPReceiver(caller);
        return tp;
    }

    /***Setters***/
    public static Integer build(Integer i) {
        content = i;
        return (Integer) content;
    }

    public static Float build(Float i) {
        content = i;
        return (Float) content;
    }

    public static Double build(Double i) {
        content = i;
        return (Double) content;
    }

    public static Long build(Long i) {
        content = i;
        return (Long) content;
    }

    public static Boolean build(Boolean i) {
        content = i;
        return (Boolean) content;
    }

    public static String build(String i) {
        content = i;
        return (String) content;
    }

    public static int build(int i) {
        content = i;
        return (int) content;
    }

    public static float build(float i) {
        content = i;
        return (float) content;
    }

    public static double build(double i) {
        content = i;
        return (double) content;
    }

    public static long build(long i) {
        content = i;
        return (long) content;
    }

    public static boolean build(boolean i) {
        content = i;
        return (boolean) content;
    }

    public static byte build(byte i) {
        content = i;
        return (byte) content;
    }

    public static void put(String key, Object value) {
        hash.put(key, value);
        ItemReceiver receiver = new ItemReceiver();
        String valueStr = value.toString();
        Integer lastDotIdx = valueStr.lastIndexOf(".");
        valueStr = valueStr.substring(lastDotIdx + 1, valueStr.length());
        System.out.println("Treated value: " + valueStr);
        receiver.putTPReceiver(key, valueStr + ".class", TP.hashName);
    }

}
