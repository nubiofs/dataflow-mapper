package com.nanda.abstraction.general.interfaces;

import java.io.Serializable;

@FunctionalInterface
public interface MapFunction<I, O> extends Serializable {

    /**
     * Mapping method.
     * Takes an element from the input data and transforms it into exactly one element.
     *
     * @param value The input value.
     * @return The transformed value
     *
     */

    O map(I value) throws Exception;
}