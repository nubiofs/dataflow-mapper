package com.nanda.abstraction.general;
import com.nanda.abstraction.general.interfaces.FilterFunction;
import com.nanda.abstraction.general.interfaces.MapFunction;
import com.nanda.abstraction.general.interfaces.ReduceFunction;
import com.nanda.abstraction.pojo.BusPosLastStopPojo;
import com.nanda.abstraction.pojo.BusPosPojo;
import com.nanda.abstraction.pojo.StopPojo;
import com.nanda.abstraction.pojo.TimeTablePojo;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

 public class StudyCase2APIParsed {

	 public static void main(String args[]) throws Exception {

		ProjectSettings project = new ProjectSettings("Both", "StudyCase2");
		project.setPackageName("experiment2");
		project.start("stream");
		Kafka<String> kafkaInput = new Kafka<>("kafkaInput", "String");
		Data<String> kafkaContent = kafkaInput.setDataInfo("kafkaContent", "String", new Data<String>()).read(TP.build("group"), TP.build("localhost:9092"), TP.build("example"), TP.build("localhost:2181"), TP.build(true),"kafkaInput");

		Data<BusPosPojo> busPosInput = kafkaContent.setDataInfo("busPosInput", "BusPosPojo", new Data<BusPosPojo>());

		Data<String> formattedBusPos = busPosInput.setDataInfo("formattedBusPos", "String", new Data<String>());
		Kafka<BusPosPojo> kafkaOutput = new Kafka<>("kafkaOutput", "BusPosPojo");
		kafkaOutput.write(TP.build("localhost:9092"), TP.build("filtered"), busPosInput,"kafkaOutput");
		FileIO<String> f = new FileIO<>(FileIO.FileFormat.Text, "f", "String");
		f.write(TP.build("/tmp/"), TP.build("output.txt"), TP.build(52428800), formattedBusPos,"f");
		Window w = new Window(true, "w", "null");
		busPosInput.timeWindow(w, Time.seconds(10),"busPosInput");



		KeyValue.getKey();
		 KeyValue.getValue();
		 KeyValue.getValue();
		 project.finish();

	}
}