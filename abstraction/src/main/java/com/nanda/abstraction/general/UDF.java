package com.nanda.abstraction.general;

import com.nanda.abstraction.general.interfaces.FilterFunction;
import com.nanda.abstraction.general.interfaces.FlatMapFunction;
import com.nanda.abstraction.general.interfaces.MapFunction;
import com.nanda.abstraction.udfs.WordCountMap;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.util.Collector;

public class UDF extends DataTransformation {

    private String nameClass;
    private Class javaClass;
    private String instance;

    /*public UDF(ProjectSettings projSettings) {
        super(projSettings);
    }*/
    public UDF() {}

    /*Setters and getters*/
    public void setNameClass(String nameClass) { this.nameClass = nameClass; }

    public String getNameClass() { return this.nameClass; }

    public void setJavaClass(Class javaClass) { this.javaClass = javaClass; }

    public Class getJavaClass() { return this.javaClass; }

    private void selectorSameFile(Object nameClass, String instance, UDFTransformType ttype) {
        Auxiliar aux = new Auxiliar();
        String transfType = udfTransformationTypeToString(ttype);
        String mtype = ProjectSettings.mapper;
        aux.callScript(new ProcessBuilder("python3", "../abstraction-python-scripts/DataTransformation/udf_transformations.py", mtype, transfType, nameClass.toString(), instance));
    }

    private void selectorSepFile(String nameClass, UDFTransformType ttype, String path) {

        /***Inside udftransformations file I'm doing some verifications.
         * Checking first if the file exists, because if it doesn't,
         * the python script would crash silently before this verification being
         * done.***/

        Auxiliar aux = new Auxiliar();
        String transfType = udfTransformationTypeToString(ttype);
        String mtype = ProjectSettings.mapper;
        aux.callScript(new ProcessBuilder("python3", "../abstraction-python-scripts/DataTransformation/udfTransformations.py", mtype, transfType, nameClass, "test", path));
    }


    /*************
     * UDFs
    To chain methods is necessary to return the class object
    This can be done for map, flatMap, reduce functions
     *************/

    /***********
        Map
    ************/
    //Given function generates code for both frameworks
    public void map(MapFunction nameClass, String instance) {
        System.out.println("Map Function");
        selectorSameFile(nameClass, instance, UDFTransformType.Map);
    }


    /************
        FlatMap
    *************/
    public void flatMap(FlatMapFunction nameClass, String instance) {
        System.out.println("FlatMap Function");
        selectorSameFile(nameClass, instance, UDFTransformType.FlatMap);
    }


    /************
        Reduce
    *************/
    public void reduce(Object nameClass, String instance) {
        System.out.println("Reduce Function");
        selectorSameFile(nameClass, instance, UDFTransformType.Reduce);
    }

    /*public void reduce(String nameClass, String pathToFile) {
        System.out.println("Reduce Function");
        selectorSepFile(nameClass, UDFTransformType.Reduce, pathToFile);
    }*/

    /************
         Fold
    *************/
    public void fold(Object nameClass, String instance) {
        System.out.println("Fold Function");
        selectorSameFile(nameClass, instance, UDFTransformType.Fold);
    }

    /*public void fold(String nameClass, String pathToFile) {
        System.out.println("Fold Function");
        selectorSepFile(nameClass, UDFTransformType.Fold, pathToFile);
    }*/


    /************
         Filter
    *************/
    public void filter(FilterFunction nameClass, String instance) {
        System.out.println("Filter Function");
        selectorSameFile(nameClass, instance, UDFTransformType.Filter);
    }

    /*public void filter(String nameClass, String pathToFile) {
        System.out.println("Filter Function");
        selectorSepFile(nameClass, UDFTransformType.Filter, pathToFile);
    }*/



    public static void main(String[] args) throws Exception {
        ProjectSettings project = new ProjectSettings("MyMap", "both");
        //project.setPackageName("novinho2");
        //project.start();

        //UDF udf = new UDF(project);
        UDF udf = new UDF();

        //udf.map("WordCountMap", MapperType.Flink).sum().print();
        udf.selectorSameFile("WordCount", "udf", UDFTransformType.Map);
        //project.finish();
    }


    public static class WordCount implements MapFunction<String, KV<String,Integer>> {
        @Override
        public KV<String, Integer> map(String value) {
            String[] tokens = value.toLowerCase().split("\\W+");
            Integer cont = 0;
            for (String token : tokens) {
                if (token.length() > 0) {
                    cont++;
                }
            }
            return new KV<>(value, cont);
        }
    }


    /*public static class WordCount2 implements FlatMapFunction<String, Tuple2<String, Integer>> {
        @Override
        public void flatMap(String value, Collector<Tuple2<String, Integer>> out) {
            String[] tokens = value.toLowerCase().split("\\W+");
            for (String token : tokens) {
                if (token.length() > 0) {
                    out.collect(new Tuple2<>(token, 1));
                }
            }
        }
    }*/

}
