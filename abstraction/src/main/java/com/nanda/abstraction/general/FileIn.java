package com.nanda.abstraction.general;

public class FileIn {


    /*Environment contains methods that handles file reading*/
    public static String flinkFileInput(String streamOrBatch) {
        streamOrBatch = streamOrBatch.toLowerCase();
        switch (streamOrBatch) {
            case("batch"):
                return "ExecutionEnvironment";
            default:
                return "StreamExecutionEnvironment";
        }
    }

    public static String apexFileInput(String stream) { return "File$TypeInput"; }
}
