package com.nanda.abstraction.general;
import com.nanda.abstraction.general.interfaces.FlatMapFunction;
import com.nanda.abstraction.general.interfaces.MapFunction;
import java.util.ArrayList;

 public class MinMaxAPIParsed {

	 public static void main(String args[]) {

		ProjectSettings project = new ProjectSettings("Both", "MinMax");
		project.setPackageName("experiment");
		project.start("stream");
		FileIO<String> f = new FileIO<>(FileIO.FileFormat.Hdfs, "f", "String");
		Data<String> d = f.setDataInfo("d", "String", new Data<String>()).read("localhost:8020/tmp/201904/prev/", "201904_flink.txt","f");
		Data<KV<String, Double>> d2 = d.setDataInfo("d2", "KV<String, Double>", new Data<KV<String, Double>>());
		d2.minByKey("d2");
		d2.maxByKey("d2");
		f.write("localhost:8020/tmp/output/201904/prev/", "min_flink.txt", 52428800, d2, "f");
		project.finish();
	}
}
