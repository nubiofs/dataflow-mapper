package com.nanda.abstraction.general;

import akka.dispatch.Mapper;
import com.nanda.abstraction.general.apex.DataTransformationA;
import com.nanda.abstraction.general.flink.DataTransformationF;
import com.sun.javafx.collections.MappingChange;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class DataTransformation {

    DataTransformationA dtTransfA;
    DataTransformationF dtTransfF;

    /*private ProjectSettings projSettings;

    DataTransformation(ProjectSettings projSettings) {
        this.projSettings = projSettings;
    }
*/
    private String tabs = "\t\t";

    public DataTransformation() {}

    public enum UDFTransformType {
        Map, FlatMap, Reduce, Fold, Filter, Sort
    }

    public enum TransformType {
        Max, Min, Sum
    }

    public enum MapperType {
        Flink, Apex, Both
    }

    public void getMapper() {
        System.out.println(ProjectSettings.mapper);
    }

    private void selector(String mapper, TransformType ttype) {
        Auxiliar aux = new Auxiliar();
        //String mapper = mapperTypeToString(mtype);
        String transformation = transformationTypeToString(ttype);
        aux.callScript(new ProcessBuilder("python3 ../abstraction-python-scripts/DataTransformation/transformations.py", mapper, transformation));

    }


    private String transformationTypeToString(TransformType ttype) {
        if (ttype.equals(TransformType.Max))
            return "max";
        else if (ttype.equals(TransformType.Min))
            return "min";
        return "sum";
    }

    public String udfTransformationTypeToString(UDFTransformType ttype) {
        if (ttype.equals(UDFTransformType.Map))
            return "map";
        else if (ttype.equals(UDFTransformType.FlatMap))
            return "flatMap";
        else if (ttype.equals(UDFTransformType.Reduce))
            return "reduce";
        else if (ttype.equals(UDFTransformType.Fold))
            return "fold";
        else if (ttype.equals(UDFTransformType.Filter))
            return "filter";
        return "sort";
    }


    public <T> void maxByKey() {}

    /*It is necessary to know which is the mapper, the data and
     if it is batch or stream (in Flink case)*/
    public void maxByKey(String caller, String dataType) {
        /*selector(ProjectSettings.mapper, TransformType.Max);*/

        Auxiliar aux = new Auxiliar();
        String mp = ProjectSettings.mapper.toLowerCase();

        if (mp.equals("apex") || mp.equals("both")) {
            String imports = "import com.datatorrent.lib.math.MaxKeyVal;\n";
            String fileWriteContent = "\n//placeholder\n";
            String declarationWriteContent = "\n\t";

            String maxOperatorName = "maxOperator" + Auxiliar.getMaxSuffix();
            Auxiliar.incMaxSuffix();
            if (dataType.contains("KV"))
                dataType = dataType.substring(dataType.indexOf("<") + 1, dataType.indexOf(">"));

            declarationWriteContent += String.format("MaxKeyVal<%s> %s = new MaxKeyVal<>();\n",
                    dataType, maxOperatorName);
            /*fileWriteContent += String.format("MaxKeyVal<%s> %s = new MaxKeyVal<>();\n",
                    dataType, maxOperatorName);

            fileWriteContent += String.format("%s%s.addOperator(%s, %s.data, %s.max);\n",
                    tabs, caller, maxOperatorName, maxOperatorName, maxOperatorName);*/

            if (ProjectSettings.processingChoice.toLowerCase().equals("stream"))
                fileWriteContent += String.format(".addOperator(%s, %s.data, %s.max);\n",
                    maxOperatorName, maxOperatorName, maxOperatorName);
            else
                fileWriteContent += String.format(".addOperator(%s, %s.data, %s.max).window(new WindowOption.GlobalWindow());\n",
                        maxOperatorName, maxOperatorName, maxOperatorName);

            /*Write to imports.java and content.java*/
            String endPath = "../abstraction-python-scripts/apex/out/";
            aux.writeContent(endPath, imports, fileWriteContent);

            /*Write to declarations.java the MaxKeyVal declaration*/
            aux.writeGeneral(endPath, "declarations.java", declarationWriteContent);
        }

        if (mp.equals("flink") || mp.equals("both")) {
            String imports = "";
            String fileWriteContent = "\n//placeholder\n";
            if (ProjectSettings.processingChoice.toLowerCase().equals("stream"))
                fileWriteContent += ".maxBy(1);\n";
                //fileWriteContent += String.format("%s.keyBy(0).maxBy(1);\n", caller);
            else
                fileWriteContent += String.format("%s.groupBy(0).maxBy(1);\n", caller);

            String endPath = "../abstraction-python-scripts/flink/out/";
            aux.writeContent(endPath, imports, fileWriteContent);
        }

    }

    public void minByKey() {}

    public <T> void minByKey(String caller, String dataType) {
        /*selector(ProjectSettings.mapper, TransformType.Min);*/

        Auxiliar aux = new Auxiliar();
        String mp = ProjectSettings.mapper.toLowerCase();

        if (mp.equals("apex") || mp.equals("both")) {
            String imports = "import com.datatorrent.lib.math.MinKeyVal;\n";
            String fileWriteContent = "\n//placeholder\n";
            String declarationWriteContent = "\n\t";

            String minOperatorName = "minOperator" + Auxiliar.getMinSuffix();
            Auxiliar.incMinSuffix();
            if (dataType.contains("KV"))
                dataType = dataType.substring(dataType.indexOf("<") + 1, dataType.indexOf(">"));

            declarationWriteContent += String.format("MinKeyVal<%s> %s = new MinKeyVal<>();\n",
                    dataType, minOperatorName);

            /*fileWriteContent += String.format("MinKeyVal<%s> %s = new MinKeyVal<>();\n",
                    dataType, minOperatorName);

            fileWriteContent += String.format("%s%s.addOperator(%s, %s.data, %s.min);\n",
                    tabs, caller, minOperatorName, minOperatorName, minOperatorName);*/

            if (ProjectSettings.processingChoice.toLowerCase().equals("stream"))
                fileWriteContent += String.format(".addOperator(%s, %s.data, %s.min);\n",
                        minOperatorName, minOperatorName, minOperatorName);
            else
                fileWriteContent += String.format(".addOperator(%s, %s.data, %s.min).window(new WindowOption.GlobalWindow());\n",
                        minOperatorName, minOperatorName, minOperatorName);


            /*Write to imports.java and content.java*/
            String endPath = "../abstraction-python-scripts/apex/out/";
            aux.writeContent(endPath, imports, fileWriteContent);

            /*Write to declarations.java the MinKeyVal declaration*/
            aux.writeGeneral(endPath, "declarations.java", declarationWriteContent);
        }

        if (mp.equals("flink") || mp.equals("both")) {
            String imports = "";
            String fileWriteContent = "\n//placeholder\n";
            if (ProjectSettings.processingChoice.toLowerCase().equals("stream"))
                fileWriteContent += ".minBy(1);\n";
                //fileWriteContent += String.format("%s.keyBy(0).minBy(1);\n", caller);
            else
                fileWriteContent += String.format("%s.groupBy(0).minBy(1);\n", caller);

            String endPath = "../abstraction-python-scripts/flink/out/";
            aux.writeContent(endPath, imports, fileWriteContent);
        }

    }


    public void sum() {}

    public <T> void sum(String caller, String dataType) {
        /*selector(ProjectSettings.mapper, TransformType.Sum);*/

        Auxiliar aux = new Auxiliar();
        String mp = ProjectSettings.mapper.toLowerCase();

        if (mp.equals("apex") || mp.equals("both")) {
            String imports = "import com.datatorrent.lib.math.SumKeyVal;\n";
            String fileWriteContent = "\n//placeholder\n";
            String declarationWriteContent = "\n\t";

            String sumOperatorName = "sumOperator" + Auxiliar.getSumSuffix();
            Auxiliar.incSumSuffix();
            if (dataType.contains("KV"))
                dataType = dataType.substring(dataType.indexOf("<") + 1, dataType.indexOf(">"));

            declarationWriteContent += String.format("SumKeyVal<%s> %s = new SumKeyVal<>();\n",
                    dataType, sumOperatorName);


            if (ProjectSettings.processingChoice.toLowerCase().equals("stream"))
                fileWriteContent += String.format(".addOperator(%s, %s.data, %s.sum);\n",
                        sumOperatorName, sumOperatorName, sumOperatorName);
            else
                fileWriteContent += String.format(".addOperator(%s, %s.data, %s.sum).window(new WindowOption.GlobalWindow());\n",
                        sumOperatorName, sumOperatorName, sumOperatorName);

            /*Write to imports.java and content.java*/
            String endPath = "../abstraction-python-scripts/apex/out/";
            aux.writeContent(endPath, imports, fileWriteContent);

            /*Write to declarations.java the SumKeyVal declaration*/
            aux.writeGeneral(endPath, "declarations.java", declarationWriteContent);
        }

        if (mp.equals("flink") || mp.equals("both")) {
            String imports = "";
            String fileWriteContent = "\n//placeholder\n";
            /*TODO: think about the case where keyBy/groupBy field 1 instead of 0 */
            if (ProjectSettings.processingChoice.toLowerCase().equals("stream"))
                fileWriteContent += ".sum(1);\n";
                //fileWriteContent += String.format("%s.keyBy(0).sum(1);\n\n", caller);
            else
                fileWriteContent += String.format("%s.groupBy(0).sum(1);\n", caller);


            String endPath = "../abstraction-python-scripts/flink/out/";
            aux.writeContent(endPath, imports, fileWriteContent);
        }

    }

    public <T> DataTransformation print() {
        return this;
    }

}
