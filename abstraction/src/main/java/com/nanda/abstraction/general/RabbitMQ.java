package com.nanda.abstraction.general;

public class RabbitMQ<T> extends IO<T> {

    String topic;
    String channel;
    String url;

    RabbitMQ(String topic, String channel, String url) {
        super("RabbitMQ");
        this.topic = topic;
        this.channel = channel;
        this.url = url;
        System.out.println("Sou o RabbitMQ");
    }

    /*Data<T> read(){
        Data<String> d;
        return d;
    }

    void write(Data<T> d){

    }*/

}