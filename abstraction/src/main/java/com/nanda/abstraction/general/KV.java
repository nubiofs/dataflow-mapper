package com.nanda.abstraction.general;

import javax.annotation.Nullable;
import java.io.Serializable;
import java.util.Objects;

public class KV<K, V> implements Serializable {

    final private @Nullable K key;
    final private @Nullable V value;
    private String flinkType = "Tuple2"; //Tuples are mutable types
    private String apexType = "KeyValPair"; //Key and Value are to be treated as immutable objects
    /*Even though Tuple2 and KeyValPair are not equal, they have similarities*/

    public KV(@Nullable K key, @Nullable V value) {
        this.key = key;
        this.value = value;
    }

    /*Getters*/
    public @Nullable K getKey() { return key; }

    public V getValue() { return value; }

    public static <K, V> KV<K, V> of(@Nullable K key, @Nullable V value) {
        return new KV<>(key, value);
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) { return true; }
        if (!(other instanceof KV)) { return false; }
        KV<?, ?> otherKv = (KV<?, ?>) other;
        // Compare two arrays
        return Objects.deepEquals(this.key, otherKv.key)
                && Objects.deepEquals(this.value, otherKv.value);
    }

    public static String flinkKV() { return "Tuple2"; }

    public static String apexKV() { return "KeyValPair"; }
}