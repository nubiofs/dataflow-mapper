package com.nanda.abstraction.general.apex;

import com.nanda.abstraction.general.Auxiliar;
import com.nanda.abstraction.general.DataTransformation;
import com.nanda.abstraction.general.ProjectSettings;

public class DataTransformationA extends DataTransformation {


    public void maxByKey(String caller, String dataType) {

        Auxiliar aux = new Auxiliar();

        String imports = "import com.datatorrent.lib.math.MaxKeyVal;\n";
        String fileWriteContent = "\n//placeholder\n";
        String declarationWriteContent = "\n\t";

        String maxOperatorName = "maxOperator" + Auxiliar.getMaxSuffix();

        if (dataType.contains("KV"))
            dataType = dataType.substring(dataType.indexOf("<") + 1, dataType.indexOf(">"));

        declarationWriteContent += String.format("MaxKeyVal<%s> %s = new MaxKeyVal<>();\n",
                dataType, maxOperatorName);

        if (ProjectSettings.processingChoice.toLowerCase().equals("stream"))
            fileWriteContent += String.format(".addOperator(%s, %s.data, %s.max);\n",
                    maxOperatorName, maxOperatorName, maxOperatorName);
        else
            fileWriteContent += String.format(".addOperator(%s, %s.data, %s.max).window(new WindowOption.GlobalWindow());\n",
                    maxOperatorName, maxOperatorName, maxOperatorName);

        /*Write to imports.java and content.java*/
        String endPath = "../abstraction-python-scripts/apex/out/";
        aux.writeContent(endPath, imports, fileWriteContent);

        /*Write to declarations.java the MaxKeyVal declaration*/
        aux.writeGeneral(endPath, "declarations.java", declarationWriteContent);

    }


    public <T> void minByKey(String caller, String dataType) {

        Auxiliar aux = new Auxiliar();
        String mp = ProjectSettings.mapper.toLowerCase();

        String imports = "import com.datatorrent.lib.math.MinKeyVal;\n";
        String fileWriteContent = "\n//placeholder\n";
        String declarationWriteContent = "\n\t";

        String minOperatorName = "minOperator" + Auxiliar.getMinSuffix();

        if (dataType.contains("KV"))
            dataType = dataType.substring(dataType.indexOf("<") + 1, dataType.indexOf(">"));

        declarationWriteContent += String.format("MinKeyVal<%s> %s = new MinKeyVal<>();\n",
                dataType, minOperatorName);

        if (ProjectSettings.processingChoice.toLowerCase().equals("stream"))
            fileWriteContent += String.format(".addOperator(%s, %s.data, %s.min);\n",
                    minOperatorName, minOperatorName, minOperatorName);
        else
            fileWriteContent += String.format(".addOperator(%s, %s.data, %s.min).window(new WindowOption.GlobalWindow());\n",
                    minOperatorName, minOperatorName, minOperatorName);


        /*Write to imports.java and content.java*/
        String endPath = "../abstraction-python-scripts/apex/out/";
        aux.writeContent(endPath, imports, fileWriteContent);

        /*Write to declarations.java the MinKeyVal declaration*/
        aux.writeGeneral(endPath, "declarations.java", declarationWriteContent);

    }


    public <T> void sum(String caller, String dataType) {

        Auxiliar aux = new Auxiliar();

        String imports = "import com.datatorrent.lib.math.SumKeyVal;\n";
        String fileWriteContent = "\n//placeholder\n";
        String declarationWriteContent = "\n\t";

        String sumOperatorName = "sumOperator" + Auxiliar.getSumSuffix();

        if (dataType.contains("KV"))
            dataType = dataType.substring(dataType.indexOf("<") + 1, dataType.indexOf(">"));

        declarationWriteContent += String.format("SumKeyVal<%s> %s = new SumKeyVal<>();\n",
                dataType, sumOperatorName);

        if (ProjectSettings.processingChoice.toLowerCase().equals("stream"))
            fileWriteContent += String.format(".addOperator(%s, %s.data, %s.sum);\n",
                    sumOperatorName, sumOperatorName, sumOperatorName);
        else
            fileWriteContent += String.format(".addOperator(%s, %s.data, %s.sum).window(new WindowOption.GlobalWindow());\n",
                    sumOperatorName, sumOperatorName, sumOperatorName);

        /*Write to imports.java and content.java*/
        String endPath = "../abstraction-python-scripts/apex/out/";
        aux.writeContent(endPath, imports, fileWriteContent);

        /*Write to declarations.java the SumKeyVal declaration*/
        aux.writeGeneral(endPath, "declarations.java", declarationWriteContent);

    }

}
