package com.nanda.abstraction.general;

public class FileOut {


    /*DataSet/DataStream contains methods that handles file writing*/
    public static String flinkFileOutput(String streamOrBatch) {
        streamOrBatch = streamOrBatch.toLowerCase();
        switch (streamOrBatch) {
            case("batch"):
                return "DataSet";
            default:
                return "DataStream";
        }
    }

    /*XXXXXXXXXXXX contains methods that handles file writing*/
    public static String apexFileOutput() { return "File$TypeOutput"; }

}
