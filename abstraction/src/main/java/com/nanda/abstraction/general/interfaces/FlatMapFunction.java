package com.nanda.abstraction.general.interfaces;

public interface FlatMapFunction<I, O> {

    public void flatMap(I input, O output);
}
