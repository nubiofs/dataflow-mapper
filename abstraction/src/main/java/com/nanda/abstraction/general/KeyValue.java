package com.nanda.abstraction.general;

import java.io.Serializable;

public class KeyValue implements Serializable {


    public static void getKey() {
        Auxiliar aux = new Auxiliar();
        String mp = ProjectSettings.mapper.toLowerCase();
        String fileWriteContent;
        String endPath = "";

        if (mp.equals("flink") || mp.equals("both")) {
            fileWriteContent = "\n//placeholder\n.f0";
            endPath = "../abstraction-python-scripts/flink/out/";
            aux.writeContent(endPath, "", fileWriteContent);
        }

        if (mp.equals("apex") || mp.equals("both")) {
            fileWriteContent = "\n//placeholder";
            endPath = "../abstraction-python-scripts/apex/out/";
            aux.writeContent(endPath, "", fileWriteContent);
        }
    }

    public static void getValue() {
        Auxiliar aux = new Auxiliar();
        String mp = ProjectSettings.mapper.toLowerCase();
        String fileWriteContent = "";
        String endPath = "";

        if (mp.equals("flink") || mp.equals("both")) {
            fileWriteContent = "\n//placeholder\n.f1";
            endPath = "../abstraction-python-scripts/flink/out/";
            aux.writeContent(endPath, "", fileWriteContent);
        }

        if (mp.equals("apex") || mp.equals("both")) {
            fileWriteContent = "\n//placeholder";
            endPath = "../abstraction-python-scripts/apex/out/";
            aux.writeContent(endPath, "", fileWriteContent);
        }
    }
}