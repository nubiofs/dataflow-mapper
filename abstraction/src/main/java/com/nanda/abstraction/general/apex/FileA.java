package com.nanda.abstraction.general.apex;

import com.nanda.abstraction.general.*;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FileA<T> extends FileIO<T> {

    private Auxiliar aux = new Auxiliar();
    private String apexEndPath = "../abstraction-python-scripts/apex/out/";

    public FileA(String ioType, String fileVarName, String fileVarType) {
        super(ioType, fileVarName, fileVarType);
    }

    public Data<T> readTextFile(String inputDirName, String fileName, String caller) {
        String action = "readA";

        if (inputDirName.endsWith("/"))
            inputDirName += fileName;
        else
            inputDirName += "/" + fileName;

        String tagName = caller + aux.getFileInSuffix();
        aux.callScript(new ProcessBuilder("python3", "../abstraction-python-scripts/properties-files/add_properties_apex.py", "fileTextR", tagName, inputDirName));

        String apexVarType = ApexParser.mapApexTypes(this.fileVarType, this.fileVarType);
        if (apexVarType.contains("KeyValPair")) {
            String content = apexVarType.substring(apexVarType.indexOf("<") + 1, apexVarType.indexOf(">"));
            String types[] = content.split(",");
            aux.callScript(new ProcessBuilder("python3", "../abstraction-python-scripts/IO/add_file_connector.py", ProjectSettings.packageName, action, types[0].trim(), types[1].trim()));
            apexVarType = types[0].trim() + types[1].trim();
        } else
            aux.callScript(new ProcessBuilder("python3", "../abstraction-python-scripts/IO/add_file_connector.py", ProjectSettings.packageName, action, this.fileVarType));

        String imports = "import static org.apache.apex.malhar.stream.api.Option.Options.name;\n";
        imports += "import org.apache.apex.malhar.stream.api.ApexStream;\n";
        imports += "import org.apache.apex.malhar.stream.api.impl.StreamFactory;\n";

        String fileReadContent = "\n//placeholder\n";
        fileReadContent += "File" + apexVarType + "Input " + this.fileVarName + " = new File" + apexVarType + "Input();\n";

        String apexDataType = ApexParser.mapApexTypes(this.dataType, this.dataType);
        if (!this.dataType.equals("") && !this.dataName.equals(""))
            fileReadContent += String.format("%sApexStream<%s> %s = ", tabs, apexDataType, this.dataName);
        fileReadContent += String.format("StreamFactory.fromInput(%s, %s.output, name(\"%s\"));\n\n",
                this.fileVarName, this.fileVarName, tagName);

        /*Write to imports.java and content.java*/
        aux.writeContent(apexEndPath, imports, fileReadContent);

        return new Data<T>(ProjectSettings.processingChoice);
    }


    public Data<T> readHdfsFile(String inputDirName, String fileName, String caller) {

        /*Looking for pattern server:port*/
        String pattern = "[\\S]+\\:[0-9]+";
        Pattern serverPort = Pattern.compile(pattern);
        Matcher mServerPort = serverPort.matcher(inputDirName);

        /*If line is empty or with whitespace*/
        if (mServerPort.find())
            inputDirName = mServerPort.replaceFirst("");

        if (inputDirName.endsWith("/"))
            inputDirName += fileName;
        else
            inputDirName += "/" + fileName;


        String tagName = caller + aux.getHdfsInSuffix();
        aux.callScript(new ProcessBuilder("python3", "../abstraction-python-scripts/properties-files/add_properties_apex.py",
                "fileHdfsR", tagName, inputDirName, fileName));

        String imports = "import static org.apache.apex.malhar.stream.api.Option.Options.name;\n";
        imports += "import org.apache.apex.malhar.lib.fs.LineByLineFileInputOperator;\n";
        imports += "import org.apache.apex.malhar.stream.api.ApexStream;\n";
        imports += "import org.apache.apex.malhar.stream.api.impl.StreamFactory;\n";

        String hdfsReadContent = "\n//placeholder\n";
        hdfsReadContent += "LineByLineFileInputOperator " + this.fileVarName + " = new LineByLineFileInputOperator();\n";

        String apexDataType = ApexParser.mapApexTypes(this.dataType, this.dataType);
        if (!this.dataType.equals("") && !this.dataName.equals(""))
            hdfsReadContent += String.format("%sApexStream<%s> %s = ", tabs, apexDataType, this.dataName);
        hdfsReadContent += String.format("StreamFactory.fromInput(%s, %s.output, name(\"%s\"));\n\n",
                this.fileVarName, this.fileVarName, tagName);

        /*Write to imports.java and content.java*/
        aux.writeContent(apexEndPath, imports, hdfsReadContent);

        return new Data<T>(ProjectSettings.processingChoice);
    }

    /****
     This method is not intended to be called by the user
     ***/
    public void writeTextFile(String outputDirName, String fileName, Integer maxLength, Data d, String caller) {

        String action = "writeA";

        String outputOperatorName = this.fileVarName + "Out" + aux.getFileOutSuffix();
        //String tagName = caller + aux.getFileOutSuffix();

        if (outputDirName.endsWith("/"))
            outputDirName += fileName;
        else
            outputDirName += "/" + fileName;

        aux.callScript(new ProcessBuilder("python3", "../abstraction-python-scripts/properties-files/add_properties_apex.py",
                "fileTextW", outputOperatorName, outputDirName, fileName, maxLength + ""));

        String apexVarType = ApexParser.mapApexTypes(this.fileVarType, this.fileVarType);
        if (apexVarType.contains("KeyValPair")) {
            String content = apexVarType.substring(apexVarType.indexOf("<") + 1, apexVarType.indexOf(">"));
            String types[] = content.split(",");
            aux.callScript(new ProcessBuilder("python3", "../abstraction-python-scripts/IO/add_file_connector.py", ProjectSettings.packageName, action, types[0].trim(), types[1].trim()));
            apexVarType = types[0].trim() + types[1].trim();
        } else
            aux.callScript(new ProcessBuilder("python3", "../abstraction-python-scripts/IO/add_file_connector.py", ProjectSettings.packageName, action, this.fileVarType));


        String imports = "import static org.apache.apex.malhar.stream.api.Option.Options.name;\n";

        String fileWriteContent = "\n//placeholder\n";
        fileWriteContent += "File" + apexVarType + "Output " + outputOperatorName
                + " = new File" + apexVarType + "Output();\n";
        //fileWriteContent += String.format("%s%s.setFilePath(\"%s\");\n", tabs, outputOperatorName, outputDirName);
        //fileWriteContent += String.format("%s%s.setBaseName(\"%s\");\n", tabs, outputOperatorName, fileName);
        /*fileWriteContent += String.format("%s%s.addOperator(%s, %s.input, %s.output, name(\"%s\"));\n\n",
                tabs, d.getDataName(), outputOperatorName, outputOperatorName, outputOperatorName, outputOperatorName);*/

        fileWriteContent += String.format("%s%s.endWith(%s, %s.input, name(\"%s\"));\n\n",
                tabs, d.getDataName(), outputOperatorName, outputOperatorName, outputOperatorName);

        /*Write to imports.java and content.java*/
        aux.writeContent(apexEndPath, imports, fileWriteContent);
    }

    public void writeHdfsFile(String outputDirName, String fileName, Integer maxLength, Data d, String caller) {

        String action = "fileHdfsW";
        String outputOperatorName = this.fileVarName + "Out" + aux.getHdfsOutSuffix();

        /*Looking for pattern server:port*/
        String pattern = "[\\S]+\\:[0-9]+";
        Pattern serverPort = Pattern.compile(pattern);
        Matcher mServerPort = serverPort.matcher(outputDirName);

        /*If line is empty or with whitespace*/
        if (mServerPort.find())
            outputDirName = mServerPort.replaceFirst("");

        if (outputDirName.endsWith("/"))
            outputDirName += fileName;
        else
            outputDirName += "/" + fileName;

        System.out.println("Before add_properties");
        aux.callScript(new ProcessBuilder("python3", "../abstraction-python-scripts/properties-files/add_properties_apex.py",
                action, outputOperatorName, outputDirName, fileName, maxLength + ""));

        /*Adding the hdfs connector LineOutputOperator*/
        aux.callScript(new ProcessBuilder("python3", "../abstraction-python-scripts/IO/add_hdfs_connector.py", ProjectSettings.packageName, "writeA"));

        String imports = "import static org.apache.apex.malhar.stream.api.Option.Options.name;\n";

        String hdfsWriteContent = "\n//placeholder\n";
        hdfsWriteContent += "LineOutputOperator<" + this.fileVarType + "> " + outputOperatorName + " = new LineOutputOperator<>();\n";
        //hdfsWriteContent += String.format("%s%s.setFilePath(\"%s\");\n", tabs, outputOperatorName, outputDirName);
        //hdfsWriteContent += String.format("%s%s.setBaseName(\"%s\");\n", tabs, outputOperatorName, fileName);
        /*hdfsWriteContent += String.format("%s%s.addOperator(%s, %s.input, %s.output, name(\"%s\"));\n\n",
            tabs, d.getDataName(), outputOperatorName, outputOperatorName, outputOperatorName, outputOperatorName);*/

        hdfsWriteContent += String.format("%s%s.endWith(%s, %s.input, name(\"%s\"));\n\n",
                tabs, d.getDataName(), outputOperatorName, outputOperatorName, outputOperatorName);


        /*Write to imports.java and content.java*/
        aux.writeContent(apexEndPath, imports, hdfsWriteContent);
    }
}
