package com.nanda.abstraction.general;
import com.nanda.abstraction.general.interfaces.FilterFunction;

 public class Hdfs2KafkaAPIParsed {

	 public static void main(String args[]) throws Exception {

		ProjectSettings project = new ProjectSettings("Both", "Hdfs2Kafka");
		project.setPackageName("experiment");
		project.start("stream");
		FileIO<String> f = new FileIO<>(FileIO.FileFormat.Hdfs, "f", "String");
		Data<String> d = f.setDataInfo("d", "String", new Data<String>()).read("localhost:8020/tmp/", "sample.txt","f");
		Kafka<String> k = new Kafka<>("k", "String");
		k.write(TP.build("localhost:9092"), TP.build("example"), d,"k");
		project.finish();
	}
}
