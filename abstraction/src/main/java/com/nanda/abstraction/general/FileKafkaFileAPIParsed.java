package com.nanda.abstraction.general;
import java.util.HashMap;

 public class FileKafkaFileAPIParsed {

	 public static void main(String args[]) throws Exception {

		ProjectSettings project = new ProjectSettings("Both", "FileKafkaFile");
		project.setPackageName("experiment");
		project.start("stream");
		FileIO<String> f = new FileIO<>(FileIO.FileFormat.Hdfs, "f", "String");
		Data<String> d = f.setDataInfo("d", "String", new Data<String>()).read("localhost:8020/tmp/", "sample.txt","f");
		Kafka<String> k = new Kafka<>("k", "String");
		k.write(TP.build("localhost:9092"), TP.build("example"), d,"k");
		Kafka<String> k2 = new Kafka<>("k2", "String");
		Data<String> d2 = k2.setDataInfo("d2", "String", new Data<String>()).read(TP.build("group"), TP.build("localhost:9092"), TP.build("example"), TP.build("localhost:2181"), TP.build(false),"k2");

		FileIO<String> f2 = new FileIO<>(FileIO.FileFormat.Text, "f2", "String");
		f2.write(TP.build("/tmp/"), TP.build("output.txt"), TP.build(1024), d2,"f2");
		project.finish();
	}
}
