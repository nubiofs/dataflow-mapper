package com.nanda.abstraction.general;

public class KafkaOut {

    /*DataSet/DataStream contains methods that handles Kafka writing (addSink)*/
    public static String flinkKafkaOutput(String streamOrBatch) {
        streamOrBatch = streamOrBatch.toLowerCase();
        switch (streamOrBatch) {
            case("batch"):
                return "DataSet";
            default:
                return "DataStream";
        }
    }

    /*Kafka Operator for output*/
    public static String apexKafkaOutput() { return "KafkaSinglePortOutputOperator"; }
}
