package com.nanda.abstraction.general;

public class MyFile extends IO {

    public enum FileFormat {
        Text, Hdfs, Csv;
    }
    private FileFormat format;

    public MyFile(FileFormat format) {
        super("file");
        this.format = format;
        System.out.println("File Constructor");
    }

    public Data<String> read(String inputDirName, String fileName) {
        Auxiliar aux = new Auxiliar();
        String mp = ProjectSettings.mapper;
        String action = "read";

        if (mp.equals("flink")) action +="F";
        else if (mp.equals("apex")) action +="A";

        if (mp.equals("apex") || mp.equals("both")) {
            if (this.format.equals(FileFormat.Text)) {
                System.out.println("Criar properties e connector");
                aux.callScript(new ProcessBuilder("python3","../abstraction-python-scripts/properties-files/AddProperties.py", "fileText", "read", inputDirName));
                aux.callScript(new ProcessBuilder("python3","../abstraction-python-scripts/IO/AddConnector.py", action, ProjectSettings.packageName));
            }
        }

        //aux.callScript(new ProcessBuilder("python3","../abstraction-python-scripts/pom-files/PomAddDep.py", ProjectSettings.packageName, ProjectSettings.mapper, "file" + this.format));
        String script = "../abstraction-python-scripts/IO/File" + this.format + ".py";
        //Probably it'll be necessary to do if else statements since the args may vary for different formats
        aux.callScript(new ProcessBuilder("python3", script, action, inputDirName, fileName));

        Data<String> d = new Data<String>(""){};
        d.setDataName("textFileInput");
        d.setIsStream(false);
        d.setInputOperator("fileIn");
        d.setTagInputOperator("fileInput");

        return d;
    }


    public void write(String outputDirName, String fileName, String maxLength, String rotationWindows, Data d) {
        Auxiliar aux = new Auxiliar();
        String mp = ProjectSettings.mapper;
        String action = "write";

        if (mp.equals("flink")) action +="F";
        else if (mp.equals("apex")) action +="A";

        if (mp.equals("apex") || mp.equals("both")) {
            if (this.format.equals(FileFormat.Text)) {
                aux.callScript(new ProcessBuilder("python3", "../abstraction-python-scripts/properties-files/AddProperties.py", "fileText", "write", outputDirName, fileName, maxLength, rotationWindows));
                aux.callScript(new ProcessBuilder("python3","../abstraction-python-scripts/IO/AddConnector.py", action, ProjectSettings.packageName));
            }
        }

        String script = "../abstraction-python-scripts/IO/File" + this.format + ".py";
        //Probably it'll be necessary to do if else statements since the args may vary for different formats
        System.out.println("Before getting data");
        String dataSetName = d.getDataName();
        String inputOperator = d.getInputOperator();
        String tagInputOperator = d.getTagInputOperator();

        System.out.println("Data type is: " + d.getGenericType());

        aux.callScript(new ProcessBuilder("python3", script, action, outputDirName, fileName, dataSetName, inputOperator, tagInputOperator));
    }
}
