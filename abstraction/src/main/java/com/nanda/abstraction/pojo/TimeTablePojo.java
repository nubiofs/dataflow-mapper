package com.nanda.abstraction.pojo;

public class TimeTablePojo {

	private String stopId;
	private String lineId;
	private Long maxTime;
	private Long minTime;
	private Long sum;
	private Integer cont;
	private Long avg;

	public TimeTablePojo() {};


	public TimeTablePojo(String stopId, String lineId, Long maxTime, Long minTime, Long sum, Integer cont, Long avg) {

		this.maxTime = maxTime;
		this.avg = avg;
		this.minTime = minTime;
		this.stopId = stopId;
		this.lineId = lineId;
		this.sum = sum;
		this.cont = cont;
	}
	public Long getMaxTime() { return maxTime; }
	public void setMaxTime(Long maxTime) { this.maxTime = maxTime; }

	public Long getAvg() { return avg; }
	public void setAvg(Long avg) { this.avg = avg; }

	public Long getMinTime() { return minTime; }
	public void setMinTime(Long minTime) { this.minTime = minTime; }

	public String getStopId() { return stopId; }
	public void setStopId(String stopId) { this.stopId = stopId; }

	public String getLineId() { return lineId; }
	public void setLineId(String lineId) { this.lineId = lineId; }

	public Long getSum() { return sum; }
	public void setSum(Long sum) { this.sum = sum; }

	public Integer getCont() { return cont; }
	public void setCont(Integer cont) { this.cont = cont; }

	public String toString() {
		return stopId + "," + lineId + "," + maxTime + "," + minTime + "," + sum + "," + cont + "," + avg;
	}

}