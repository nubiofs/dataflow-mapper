package com.nanda.abstraction.pojo;

public class BusPosLastStopPojo {

	private String busId;
	private String lineId;
	private String lastStopId;
	private String lastStopTimestamp;

	public BusPosLastStopPojo() {};


	public BusPosLastStopPojo(String busId, String lineId, String lastStopId, String lastStopTimestamp) {

		this.busId = busId;
		this.lastStopTimestamp = lastStopTimestamp;
		this.lineId = lineId;
		this.lastStopId = lastStopId;
	}
	public String getBusId() { return busId; }
	public void setBusId(String busId) { this.busId = busId; }

	public String getLastStopTimestamp() { return lastStopTimestamp; }
	public void setLastStopTimestamp(String lastStopTimestamp) { this.lastStopTimestamp = lastStopTimestamp; }

	public String getLineId() { return lineId; }
	public void setLineId(String lineId) { this.lineId = lineId; }

	public String getLastStopId() { return lastStopId; }
	public void setLastStopId(String lastStopId) { this.lastStopId = lastStopId; }

	public String toString() {
		return busId + "," + lineId + "," + lastStopId + "," + lastStopTimestamp;
	}

}