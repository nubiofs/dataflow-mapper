package com.nanda.abstraction.templateUDF;

import com.nanda.abstraction.general.ProjectSettings;

public class UserCode {

    public static void main(String[] args) throws Exception {

        /*Replace by the desired class name, mapper and package name*/
        ProjectSettings project = new ProjectSettings("CLASSNAME", "MAPPERNAME");
        /*project.create("PACKAGENAME", args);  OR
        project.setPackageName("PACKAGENAME"); */
        project.start("stream");

        /*PUT YOUR CODE HERE*/
        project.finish();
    }
}