package com.nanda.abstraction.templateUDF;

import org.apache.flink.api.common.functions.MapFunction;

/**Replace I with type of input elements and O with type of output elements
 * Replace MapFlink with your class name*/
public final class MapFlink implements MapFunction<I, O> {

    @Override
    public O map(I value) throws Exception {
        //Put your code here and replace with appropriated output
        return new O();
    }
}