package com.nanda.abstraction.templateUDF;

import org.apache.flink.api.common.functions.ReduceFunction;

/**Replace T with the appropriated type
 * Static is due to main (this function will be called inside main)*/
public final class ReduceFlink implements ReduceFunction<T> {

    @Override
    public T reduce(T value1, T value2) throws Exception {
        //put your code here
        return new T();
    }
}