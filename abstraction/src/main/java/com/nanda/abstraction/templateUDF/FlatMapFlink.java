package com.nanda.abstraction.templateUDF;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.util.Collector;


/**Replace I with type of input elements and O with type of output elements
 * Replace FlatMapFlink with your class name*/
public final class FlatMapFlink implements FlatMapFunction<I, O> {

    @Override
    public void flatMap(I value, Collector<O> out) throws Exception {
        //put your code here
    }
}
